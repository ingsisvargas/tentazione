/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var websocket;
var user_chat;
var user_need;
var ip_other;

/**
 * Permite iniciar la conexión de solicitud al servicio websocket
 * @returns {undefined}
 */
function startSocketConection() {
    var wsUri = "ws://localhost:9000/demo/server.php";
    websocket = new WebSocket(wsUri);

    websocket.onopen = function (ev) {
        var msg = {
            message: "Logeado",
            name: user_chat,
            color: 'RED',
            user_need: user_need
        };
        console.log("Enviando");
        //convert and send data to server
        websocket.send(JSON.stringify(msg));
    };
    /**
     * Un error en la transaccion
     * @param {type} ev
     * @returns {undefined}
     */
    websocket.onerror = function (ev) {
        $('#message_box').append("<div class=\"system_error\">Error Occurred - " + ev.data + "</div>");
    };

    /**
     * Al cerrar la conexion
     * @param {type} ev
     * @returns {undefined}
     */
    websocket.onclose = function (ev) {

    };

//#### Message received from server?
    websocket.onmessage = function (ev) {
        var msg = JSON.parse(ev.data); //PHP sends Json data
        var type = msg.type; //message type
        var umsg = msg.message; //message text
        var uname = msg.name_in; //user name
        var ucolor = msg.color; //color
        var exist = msg.exist;
        var ip_need = msg.ip_other;
        var ip = msg.ip;



        if (type === 'usermsg')
        {

            if (user_chat === uname) {
                console.log("Pase yo");

                if (exist == true) {
                    document.getElementById('makeDial').style.display = 'block';
                    document.getElementById('endDial').style.display = 'block';
                    ip_other = ip_need;
                }
            } else {
                if (user_need === uname) {
                    console.log("LLego el Otro" + user_need);
                    document.getElementById('makeDial').style.display = 'block';
                    document.getElementById('endDial').style.display = 'block';
                    ip_other = ip_need;
                }
            }

        }
        if (type === 'system')
        {
            if (ip_other == ip) {
                console.log("se fue el otro");
                document.getElementById('makeDial').style.display = 'block';
                document.getElementById('endDial').style.display = 'block';
            }
        }


    };

}








