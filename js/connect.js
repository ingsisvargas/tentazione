class Connect {

    constructor(firebase) {
        this.config = {
            apiKey: "AIzaSyCcUBYlomhzVkjZvkRJ2dXehOZjPucVZgk",
            authDomain: "tentazione2-36177.firebaseapp.com",
            databaseURL: "https://tentazione2-36177.firebaseio.com",
            storageBucket: "tentazione2-36177.appspot.com",
            messagingSenderId: "602017026832"
        };
        this.firebase = firebase;
        this.initializeApp();
        this.database = this.firebase.database();

    }
    initializeApp() {
        this.firebase.initializeApp(this.config);
    }
    login(UserChat) {
        this.firebase.auth().signInWithEmailAndPassword(UserChat.email, UserChat.password);
    }
    loginOut(UserChat, method) {
        this.firebase.auth().signOut().then(method);
    }
    ref(router) {
        return this.database.ref(router);
    }
    registerUser(UserChat) {
        this.firebase.auth().createUserWithEmailAndPassword(UserChat.email, UserChat.password);
    }

}