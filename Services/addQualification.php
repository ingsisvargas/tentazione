<?php
session_start();

include_once '../business/MngQualification.php';

/* 
 * Servicio que permite registrar una calificación al servicio 
 * 
 * parametros de entrada
 * puntuacion-> puntación de la calificación
 * descripcion-> descripcion del calificación
 * id_servicio->identificador del servicio
 * STATUS: OK - Error
 * ERROR: Descripción mysql del error, empty si STATUS es OK.
 */

$idUser = $_SESSION['DATA']['idtnt_usuario'];
$type = $_SESSION['DATA']['tipo'];
$score=$_POST['puntuacion'];
$description = $_POST['descripcion'];
$idService = $_POST['id_servicio'];

$mngQualification = new MngQualification();
$data = $mngQualification->addQualification($idUser, $type, $score, $description,$idService);
echo $data;


