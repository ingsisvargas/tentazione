<?php

/* 
 * Servicio que permite modificar la imagen de perfil para un usuario, para esto
 * recibe la url en donde se encuentra alojada la imagen y como respuesta emite
 * un json con los siguientes valores
 * 
 * STATUS: OK - Error
 * ERROR: Descripción mysql del error.
 */

include_once '../business/MngUser.php';

session_start();
$urlNewImage = $_POST['urlNewImage'];
$idUser = $_SESSION['DATA']['idtnt_usuario'];
$mngUser  = new MngUser();

$res = $mngUser->changePerfilImage($urlNewImage,$idUser);
$_SESSION['DATA']['url_foto_perfil']=$urlNewImage;
echo $res;




