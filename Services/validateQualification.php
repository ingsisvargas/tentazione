<?php

session_start();

include_once '../business/MngQualification.php';

/* 
 * Servicio que permite validar si ya se realizo calificación del servicio el usuario
 * 
 * parametros de entrada
 * id_servicio->identificador del servicio
 * STATUS: OK - Error
 * ERROR: Descripción mysql del error, empty si STATUS es OK.
 */

$idUser = $_SESSION['DATA']['idtnt_usuario'];
$type = $_SESSION['DATA']['tipo'];
$idService = $_POST['id_servicio'];
$mngQualification = new MngQualification();
$data = $mngQualification->validateQualification($idUser, $type,$idService);
echo $data;
