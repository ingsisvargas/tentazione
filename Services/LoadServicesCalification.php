<?php

include_once '../business/MngServices.php';

/*
 * Servicio que permite cargar los servicios faltantes por aplicar calificación
 * por parte de un usuario.
 * 
 * Tiene en cuenta el tipo de usuario en session y el id del usuario.
 * 
 * Como resultado se da un ArrayJson -> DATA de la respuesta, del siguiente modo:
 *  cada Json representando un servicio con los siguientes datos.
 *  [idtnt_servicio] -> identificador del servicio
 *  [fecha_servicio] -> fecha del servicio
 *  [estado] -> estado del servicio
 *  [id_cliente] -> id del cliente que solicito
 *  [id_proveedor] -> id del proveedor que atendio
 *  [total] -> valor total del servicio
 *  [hora_inicio] -> hora de inicio del servicio
 *  [hora_fin] -> hora final del servicio
 *  [descripcion] -> descripción del plan a efectuar en la cita
 *  [plan] -> plan a efectuar en la cita
 *  [categoria] -> categoria del servicio, dada por la categoria del proveedor.
 *  [asociado] -> si el tipo de usuario es cliente, sera el nombre del proveedor
 *  si el tipo de usuario es proveedor, sera el nombre del cliente.
 * 
 * 
 * si no hay el array DATA es vacio.
 * 
 *  
 */

session_start();
$idTipo = $_SESSION['DATA']['tipo'];
$idUser = $_SESSION['DATA']['idtnt_usuario'];


$mngServices = new MngServices();

$res;

if ($idTipo == 2) {
    $res = $mngServices->loadServicesCalification($idUser,true);
}

if ($idTipo == 3) {
    $res = $mngServices->loadServicesCalification($idUser,false);
}

echo $res;

