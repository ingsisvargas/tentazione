<?php

session_start();
/**
 * Servicio que permite cargar los servicios asociado a un usuario
 * parametro de entrada 
 * estado -> estado del servicio
 */
include_once '../business/MngServices.php';
$state = $_POST['estado'];
$idUser = $_SESSION['DATA']['idtnt_usuario'];
$type = $_SESSION['DATA']['tipo'];
$mngServices = new MngServices();
$res = $mngServices->getServices($idUser, $type, $state);

echo $res;
