<?php

include_once '../business/MngServices.php';


/*
 * Servicio que permite registrar una solicitud de servicio por parte de un cli-
 * ente, para esto recibe los datos del servicio descritos a continuación
 *
 *  fecha_servicio -> fecha en la que se llevara a cabo el plan Formato [AAAA-MM-DD]
 *  hora_inicio -> hora inicial del servicio
 *  minutos_inicio -> simboliza los minutos de la hora inicio
 *  hora_fin -> hora final del servicio
 *  minutos_fin-> simboliza los minutos de la hora fin.
 *  descripcion -> Que se hara en el plan
 *  plan -> Nombre del plan.
 *  categoria -> Categoria a la que deben pertenecer los postulantes a aspirante ->  A, AA , AAA
 * 
 * 
 *  Como resultado se emite un JSON del siguiente modo
 *  
 *  STATUS -> OK ó Error
 *  ERROR  -> Error mysqli que se genera.
 *
 */

session_start();

$tipo = $_SESSION['DATA']['tipo'];
$idUser = $_SESSION['DATA']['idtnt_usuario'];

if ($tipo == 2) {

    $date = new DateTime($_POST['fecha_servicio']);
    $fecha_servicio = $date->format('Y-m-d');
    $hi = $_POST['hora_inicio'] . ":00";
    $dateHI = new DateTime($hi);
    $hora_inicio = $dateHI->format('H:i:s');
    $hf = $_POST['hora_fin'] . ":00";
    $dateHF = new DateTime($hf);
    $hora_fin = $dateHF->format('H:i:s');

    $descripcion = $_POST['descripcion'];
    $plan = $_POST['plan'];
    $categoria = $_POST['categoria'];





    $mngServices = new MngServices();
    $res = $mngServices->registerService($idUser, $fecha_servicio, $hora_fin, $hora_inicio, $descripcion, $plan, $categoria);
}





echo $res;





