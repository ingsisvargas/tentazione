<?php

include_once '../business/MngCustomer.php';
include_once '../business/MngProvider.php';

/*
 * Servicio que gestiona la carga de información personal de un usuario
 * Del siguiente modo:
 *   nombres
 *   apellidos
 *   fecha_nacimiento
 *   url_foto_perfil 
 *   puntuacion 
 *   sexo
 *   pais 
 *   region
 *   ciudad
 *   IMAGENES -> Array de JSON del siguiente Modo, cada JSON.
 *               - idtnt_recursos  -> id del objeto       
 *               - url_almacenamiento -> url de la imagen
 *               - visibilidad -> visibilidad 1:si y 0:no
 *              
 *   
 * 
 * 
 * 
 * 
 */ 

    session_start();
    $result=$_SESSION['DATA'];
    if ($result['tipo'] == 2) {
        $mngCustomer = new MngCustomer();
        $data = $mngCustomer->LoadPersonalInfo($result['idtnt_usuario']);
        echo $data;
    }
    if ($result['tipo'] == 3) {
        $mngProvider = new MngProvider();
        $data = $mngProvider->LoadPersonalInfo($result['idtnt_usuario']);
        echo $data;
    }

    




