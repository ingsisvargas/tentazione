<?php

include_once '../business/MngCustomer.php';
include_once '../business/MngProvider.php';

/* 
 * Servicio que permite modificar la información de ubicación un usuario, bien sea
 * cliente o proveedor.
 * 
 * Para esto recibe los datos pais, region y ciudad que seran afectados
 * y como respuesta emite un json con los siguientes valores
 * 
 * STATUS: OK - Error
 * ERROR: Descripción mysql del error, empty si STATUS es OK.
 */

session_start();

echo $_POST['pais'];

/*

$idUser = $_SESSION['DATA']['idtntUsuario'];
$tipo = $_SESSION['DATA']['tipo'];
$pais = $_POST['pais'];
$region = $_POST['region'];
$ciudad = $_POST['ciudad'];

$mngCustomer = new MngCustomer();
$res = $mngCustomer->LocationConfiguration($pais, $region, $ciudad, $idUser);
echo $res;

*/