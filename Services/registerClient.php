<?php
include_once '../business/MngUser.php';
include_once '../business/MngCustomer.php';

$nick = trim($_POST['nick']);
$nombre = $_POST['nombre'];
$apellido = $_POST['apellido'];
$numero = $_POST['nro_celular'];
$fecha = $_POST['fecha_nacimiento'];
$fumador = $_POST['fumador'];
$sexo = $_POST['sexo'];
$email = $_POST['email'];
$codigo_referido=$_POST['seleccion_referido'];
$referido=$_POST['referido'];
$contraseña = $_POST['contraseña'];
$rcontraseña = $_POST['rcontraseña'];
$sms = $_POST['seleccion_sms'];
$ciudad = $_POST['ciudad'];
$pais = $_POST['pais'];
$region = $_POST['region'];

$mngUser = new MngUser();
$mngClient = new MngCustomer();
$nicksUsados = $mngUser->validateNickname($nick);

if($nicksUsados['STATUS'] === 'ERROR'){
    if($contraseña == $rcontraseña){

            $datos =array("nick" => $nick,"password" => $contraseña,"tipo" => 2,"aceptado" => 1,
                          "nombres" => $nombre,"apellidos" => $apellido,"celular" => $numero ,"email" => $email,
                          "fecha_nacimiento" => $fecha ,"url_foto_perfil" => "...","fumador" => $fumador,
                          "notificacion_inactividad" =>$sms,"puntuacion" => 0,"ciudad" => $ciudad ,"pais" => $pais,
                            "region" => $region,"codigo_referido" => $referido,"sexo" => $sexo );
            $data = $mngClient->AddCustomer($datos);

            if($data == true ){
                header('Location:../index.php?create=ok');
                return;
            }else{
                header('Location:../templates/registarCliente.php?create=bad');
            }

    }else{
        header('Location:../templates/registarCliente.php?create=bad');
    }
}else{
    header('Location:../templates/registarCliente.php?create=bad');
}


?>
