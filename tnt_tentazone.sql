-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 01-12-2016 a las 04:13:42
-- Versión del servidor: 10.1.19-MariaDB
-- Versión de PHP: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `tnt_tentazone`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tnt_aspirantes`
--

CREATE TABLE `tnt_aspirantes` (
  `idtnt_aspirantes` int(11) NOT NULL,
  `id_proveedor` int(11) NOT NULL,
  `id_servicio` int(11) NOT NULL,
  `asignado` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tnt_aspirantes`
--

INSERT INTO `tnt_aspirantes` (`idtnt_aspirantes`, `id_proveedor`, `id_servicio`, `asignado`) VALUES
(1, 1, 1, 0),
(2, 1, 37, 0),
(3, 1, 38, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tnt_calificacion`
--

CREATE TABLE `tnt_calificacion` (
  `idtnt_calificacion` int(11) NOT NULL,
  `id_cliente` int(11) DEFAULT NULL,
  `id_proveedor` int(11) DEFAULT NULL,
  `puntuacion` int(11) NOT NULL,
  `descripcion` varchar(300) NOT NULL,
  `id_servicio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tnt_ciudad`
--

CREATE TABLE `tnt_ciudad` (
  `idtnt_ciudad` int(11) NOT NULL,
  `paises_Codigo` varchar(2) NOT NULL,
  `ciudad` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tnt_cliente`
--

CREATE TABLE `tnt_cliente` (
  `idtnt_cliente` int(11) NOT NULL,
  `nombres` varchar(45) NOT NULL,
  `apellidos` varchar(45) NOT NULL,
  `celular` varchar(45) NOT NULL,
  `email` varchar(45) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `fumador` tinyint(1) NOT NULL,
  `notificacion_inactividad` varchar(2) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `puntuacion` int(11) NOT NULL,
  `pais` varchar(45) NOT NULL,
  `region` varchar(45) NOT NULL,
  `ciudad` varchar(45) NOT NULL,
  `codigo_referido` varchar(15) NOT NULL,
  `codigo_referir` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tnt_cliente`
--

INSERT INTO `tnt_cliente` (`idtnt_cliente`, `nombres`, `apellidos`, `celular`, `email`, `fecha_nacimiento`, `fumador`, `notificacion_inactividad`, `id_usuario`, `puntuacion`, `pais`, `region`, `ciudad`, `codigo_referido`, `codigo_referir`) VALUES
(12, 'Ronald Antonny', 'Vargas Mora', '3112307303', 'ingsisvargas@gmail.com', '1993-03-01', 1, 'NO', 41, 3, 'Colombia', 'Valle del Cauca', 'Santiago de Cali', '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tnt_detalles`
--

CREATE TABLE `tnt_detalles` (
  `idtnt_detalles` int(11) NOT NULL,
  `descripción` varchar(100) DEFAULT NULL,
  `valor` double DEFAULT NULL,
  `id_servicio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tnt_membresia`
--

CREATE TABLE `tnt_membresia` (
  `idtnt_membresia` int(11) NOT NULL,
  `fecha_inicio` date NOT NULL,
  `fecha_fin` date NOT NULL,
  `id_plan` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tnt_pais`
--

CREATE TABLE `tnt_pais` (
  `idtnt_pais` varchar(2) NOT NULL,
  `pais` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `tnt_pais`
--

INSERT INTO `tnt_pais` (`idtnt_pais`, `pais`) VALUES
('AU', 'Australia'),
('CN', 'China'),
('JP', 'Japan'),
('TH', 'Thailand'),
('IN', 'India'),
('MY', 'Malaysia'),
('KR', 'Kore'),
('HK', 'Hong Kong'),
('TW', 'Taiwan'),
('PH', 'Philippines'),
('VN', 'Vietnam'),
('FR', 'France'),
('EU', 'Europe'),
('DE', 'Germany'),
('SE', 'Sweden'),
('IT', 'Italy'),
('GR', 'Greece'),
('ES', 'Spain'),
('AT', 'Austria'),
('GB', 'United Kingdom'),
('NL', 'Netherlands'),
('BE', 'Belgium'),
('CH', 'Switzerland'),
('AE', 'United Arab Emirates'),
('IL', 'Israel'),
('UA', 'Ukraine'),
('RU', 'Russian Federation'),
('KZ', 'Kazakhstan'),
('PT', 'Portugal'),
('SA', 'Saudi Arabia'),
('DK', 'Denmark'),
('IR', 'Ira'),
('NO', 'Norway'),
('US', 'United States'),
('MX', 'Mexico'),
('CA', 'Canada'),
('A1', 'Anonymous Proxy'),
('SY', 'Syrian Arab Republic'),
('CY', 'Cyprus'),
('CZ', 'Czech Republic'),
('IQ', 'Iraq'),
('TR', 'Turkey'),
('RO', 'Romania'),
('LB', 'Lebanon'),
('HU', 'Hungary'),
('GE', 'Georgia'),
('BR', 'Brazil'),
('AZ', 'Azerbaijan'),
('A2', 'Satellite Provider'),
('PS', 'Palestinian Territory'),
('LT', 'Lithuania'),
('OM', 'Oman'),
('SK', 'Slovakia'),
('RS', 'Serbia'),
('FI', 'Finland'),
('IS', 'Iceland'),
('BG', 'Bulgaria'),
('SI', 'Slovenia'),
('MD', 'Moldov'),
('MK', 'Macedonia'),
('LI', 'Liechtenstein'),
('JE', 'Jersey'),
('PL', 'Poland'),
('HR', 'Croatia'),
('BA', 'Bosnia and Herzegovina'),
('EE', 'Estonia'),
('LV', 'Latvia'),
('JO', 'Jordan'),
('KG', 'Kyrgyzstan'),
('RE', 'Reunion'),
('IE', 'Ireland'),
('LY', 'Libya'),
('LU', 'Luxembourg'),
('AM', 'Armenia'),
('VG', 'Virgin Island'),
('YE', 'Yemen'),
('BY', 'Belarus'),
('GI', 'Gibraltar'),
('MQ', 'Martinique'),
('PA', 'Panama'),
('DO', 'Dominican Republic'),
('GU', 'Guam'),
('PR', 'Puerto Rico'),
('VI', 'Virgin Island'),
('MN', 'Mongolia'),
('NZ', 'New Zealand'),
('SG', 'Singapore'),
('ID', 'Indonesia'),
('NP', 'Nepal'),
('PG', 'Papua New Guinea'),
('PK', 'Pakistan'),
('AP', 'Asia/Pacific Region'),
('BS', 'Bahamas'),
('LC', 'Saint Lucia'),
('AR', 'Argentina'),
('BD', 'Bangladesh'),
('TK', 'Tokelau'),
('KH', 'Cambodia'),
('MO', 'Macau'),
('MV', 'Maldives'),
('AF', 'Afghanistan'),
('NC', 'New Caledonia'),
('FJ', 'Fiji'),
('WF', 'Wallis and Futuna'),
('QA', 'Qatar'),
('AL', 'Albania'),
('BZ', 'Belize'),
('UZ', 'Uzbekistan'),
('KW', 'Kuwait'),
('ME', 'Montenegro'),
('PE', 'Peru'),
('BM', 'Bermuda'),
('CW', 'Curacao'),
('CO', 'Colombia'),
('VE', 'Venezuela'),
('CL', 'Chile'),
('EC', 'Ecuador'),
('ZA', 'South Africa'),
('IM', 'Isle of Man'),
('BO', 'Bolivia'),
('GG', 'Guernsey'),
('MT', 'Malta'),
('TJ', 'Tajikistan'),
('SC', 'Seychelles'),
('BH', 'Bahrain'),
('EG', 'Egypt'),
('ZW', 'Zimbabwe'),
('LR', 'Liberia'),
('KE', 'Kenya'),
('GH', 'Ghana'),
('NG', 'Nigeria'),
('TZ', 'Tanzani'),
('ZM', 'Zambia'),
('MG', 'Madagascar'),
('AO', 'Angola'),
('NA', 'Namibia'),
('CI', 'Cote D''Ivoire'),
('SD', 'Sudan'),
('CM', 'Cameroon'),
('MW', 'Malawi'),
('GA', 'Gabon'),
('ML', 'Mali'),
('BJ', 'Benin'),
('TD', 'Chad'),
('BW', 'Botswana'),
('CV', 'Cape Verde'),
('RW', 'Rwanda'),
('CG', 'Congo'),
('UG', 'Uganda'),
('MZ', 'Mozambique'),
('GM', 'Gambia'),
('LS', 'Lesotho'),
('MU', 'Mauritius'),
('MA', 'Morocco'),
('DZ', 'Algeria'),
('GN', 'Guinea'),
('CD', 'Cong'),
('SZ', 'Swaziland'),
('BF', 'Burkina Faso'),
('SL', 'Sierra Leone'),
('SO', 'Somalia'),
('NE', 'Niger'),
('CF', 'Central African Republic'),
('TG', 'Togo'),
('BI', 'Burundi'),
('GQ', 'Equatorial Guinea'),
('SS', 'South Sudan'),
('SN', 'Senegal'),
('MR', 'Mauritania'),
('DJ', 'Djibouti'),
('KM', 'Comoros'),
('IO', 'British Indian Ocean Territory'),
('TN', 'Tunisia'),
('GL', 'Greenland'),
('VA', 'Holy See (Vatican City State)'),
('CR', 'Costa Rica'),
('KY', 'Cayman Islands'),
('JM', 'Jamaica'),
('GT', 'Guatemala'),
('MH', 'Marshall Islands'),
('AQ', 'Antarctica'),
('BB', 'Barbados'),
('AW', 'Aruba'),
('MC', 'Monaco'),
('AI', 'Anguilla'),
('KN', 'Saint Kitts and Nevis'),
('GD', 'Grenada'),
('PY', 'Paraguay'),
('MS', 'Montserrat'),
('TC', 'Turks and Caicos Islands'),
('AG', 'Antigua and Barbuda'),
('TV', 'Tuvalu'),
('PF', 'French Polynesia'),
('SB', 'Solomon Islands'),
('VU', 'Vanuatu'),
('ER', 'Eritrea'),
('TT', 'Trinidad and Tobago'),
('AD', 'Andorra'),
('HT', 'Haiti'),
('SH', 'Saint Helena'),
('FM', 'Micronesi'),
('SV', 'El Salvador'),
('HN', 'Honduras'),
('UY', 'Uruguay'),
('LK', 'Sri Lanka'),
('EH', 'Western Sahara'),
('CX', 'Christmas Island'),
('WS', 'Samoa'),
('SR', 'Suriname'),
('CK', 'Cook Islands'),
('KI', 'Kiribati'),
('NU', 'Niue'),
('TO', 'Tonga'),
('TF', 'French Southern Territories'),
('YT', 'Mayotte'),
('NF', 'Norfolk Island'),
('BN', 'Brunei Darussalam'),
('TM', 'Turkmenistan'),
('PN', 'Pitcairn Islands'),
('SM', 'San Marino'),
('AX', 'Aland Islands'),
('FO', 'Faroe Islands'),
('SJ', 'Svalbard and Jan Mayen'),
('CC', 'Cocos (Keeling) Islands'),
('NR', 'Nauru'),
('GS', 'South Georgia and the South Sandwich Islands'),
('UM', 'United States Minor Outlying Islands'),
('GW', 'Guinea-Bissau'),
('PW', 'Palau'),
('AS', 'American Samoa'),
('BT', 'Bhutan'),
('GF', 'French Guiana'),
('GP', 'Guadeloupe'),
('MF', 'Saint Martin'),
('VC', 'Saint Vincent and the Grenadines'),
('PM', 'Saint Pierre and Miquelon'),
('BL', 'Saint Barthelemy'),
('DM', 'Dominica'),
('ST', 'Sao Tome and Principe'),
('KP', 'Kore'),
('FK', 'Falkland Islands (Malvinas)'),
('MP', 'Northern Mariana Islands'),
('TL', 'Timor-Leste'),
('BQ', 'Bonair'),
('MM', 'Myanmar'),
('NI', 'Nicaragua'),
('SX', 'Sint Maarten (Dutch part)'),
('GY', 'Guyana'),
('LA', 'Lao People''s Democratic Republic'),
('CU', 'Cuba'),
('ET', 'Ethiopia');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tnt_plan`
--

CREATE TABLE `tnt_plan` (
  `idtnt_plan` int(11) NOT NULL,
  `nombre` varchar(150) NOT NULL,
  `duracion` int(11) NOT NULL,
  `valor` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tnt_plan`
--

INSERT INTO `tnt_plan` (`idtnt_plan`, `nombre`, `duracion`, `valor`) VALUES
(1, 'referido', 15, 20000);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tnt_proveedor`
--

CREATE TABLE `tnt_proveedor` (
  `idtnt_proveedor` int(11) NOT NULL,
  `nombres` varchar(45) NOT NULL,
  `apellidos` varchar(45) NOT NULL,
  `celular` varchar(15) NOT NULL,
  `fecha_nacimiento` date NOT NULL,
  `puntuacion` int(11) NOT NULL,
  `categoria` varchar(3) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `email` varchar(45) NOT NULL,
  `pais` varchar(60) NOT NULL,
  `region` varchar(60) NOT NULL,
  `ciudad` varchar(60) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tnt_proveedor`
--

INSERT INTO `tnt_proveedor` (`idtnt_proveedor`, `nombres`, `apellidos`, `celular`, `fecha_nacimiento`, `puntuacion`, `categoria`, `id_usuario`, `email`, `pais`, `region`, `ciudad`) VALUES
(1, 'Maria Fernanda', 'Prada Castro', '3112322334', '1997-09-11', 3, 'AAA', 42, 'alguien@gmail.com', 'Colombia', 'Valle del Cauca', 'Santiago de Cali');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tnt_recursos`
--

CREATE TABLE `tnt_recursos` (
  `idtnt_recursos` int(11) NOT NULL,
  `url_almacenamiento` varchar(300) NOT NULL,
  `tipo` varchar(2) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `visisbilidad` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tnt_recursos`
--

INSERT INTO `tnt_recursos` (`idtnt_recursos`, `url_almacenamiento`, `tipo`, `id_usuario`, `visisbilidad`) VALUES
(1, 'https://fbcdn-photos-b-a.akamaihd.net/hphotos-ak-xta1/v/t1.0-0/p206x206/14095925_10208834612576403_3701599243642215658_n.jpg?oh=9d7cff20ff489c3daec9af59d92f4886&oe=5886A74F&__gda__=1490156382_92d0ebf3aadea0cc4eb019868a38e195', 'FT', 41, 1),
(2, 'https://scontent-mia1-2.xx.fbcdn.net/v/t1.0-9/10846279_10204445727817027_4669882750118193846_n.jpg?oh=fbad65f606c4258924f80899fb7ad256&oe=58884BBC', 'FT', 41, 1),
(3, 'https://scontent-iad3-1.xx.fbcdn.net/v/t1.0-9/14470543_1831832793715804_6465436184372509020_n.jpg?oh=558968759d8947c3402494ec8403181d&oe=58944CD4', 'FT', 42, 1),
(4, 'https://fbcdn-photos-b-a.akamaihd.net/hphotos-ak-xlt1/v/t1.0-0/p206x206/13626398_1793057977593286_582528667920321121_n.jpg?oh=1c773af6efe262da0381eefc2a299593&oe=58C8C446&__gda__=1485880666_2df7c31053421c63fabc1b8cb81dcee8', 'FT', 42, 0),
(5, 'http://localhost/tentazione/uploads/ronsex/hombre.jpg', 'FT', 41, 1),
(6, 'http://localhost/tentazione/uploads/fertoz/sara.jpg', 'FT', 42, 1),
(7, 'http://localhost/tentazione/uploads/ronsex/', 'FT', 41, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tnt_servicio`
--

CREATE TABLE `tnt_servicio` (
  `idtnt_servicio` int(11) NOT NULL,
  `fecha_servicio` date NOT NULL,
  `estado` varchar(2) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `id_proveedor` int(11) DEFAULT NULL,
  `total` double NOT NULL,
  `hora_inicio` time NOT NULL,
  `hora_fin` time NOT NULL,
  `descripcion` varchar(500) NOT NULL,
  `plan` varchar(100) NOT NULL,
  `categoria` varchar(3) NOT NULL,
  `fecha_registro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tnt_servicio`
--

INSERT INTO `tnt_servicio` (`idtnt_servicio`, `fecha_servicio`, `estado`, `id_cliente`, `id_proveedor`, `total`, `hora_inicio`, `hora_fin`, `descripcion`, `plan`, `categoria`, `fecha_registro`) VALUES
(1, '2016-11-15', 'AT', 12, 1, 0, '15:13:00', '20:12:25', 'Salida casual a cenar.', 'Cena de Mariscos', 'AAA', '2016-11-07 23:55:02'),
(37, '2016-11-18', 'AT', 12, 1, 0, '19:30:00', '20:15:00', 'Una fan de Harry Potter que quiera ver la peli de monstruos magicos, luego un coctail  estaria bien.', 'Ir al cine', 'AAA', '2016-11-12 16:32:12'),
(38, '2016-11-17', 'AB', 12, NULL, 0, '13:23:00', '13:45:00', 'no lo se', 'algo nuevo', 'AAA', '2016-11-12 19:05:59');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tnt_usuario`
--

CREATE TABLE `tnt_usuario` (
  `idtnt_usuario` int(11) NOT NULL,
  `nick` varchar(20) NOT NULL,
  `password` varchar(15) NOT NULL,
  `admin` tinyint(1) NOT NULL,
  `aceptado` tinyint(1) NOT NULL,
  `url_foto_perfil` varchar(300) NOT NULL,
  `codigo_referir` varchar(30) NOT NULL,
  `tipo` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tnt_usuario`
--

INSERT INTO `tnt_usuario` (`idtnt_usuario`, `nick`, `password`, `admin`, `aceptado`, `url_foto_perfil`, `codigo_referir`, `tipo`) VALUES
(41, 'ronsex', '12345R', 0, 1, 'http://localhost/tentazione/uploads/ronsex/hombre.jpg', 'XXOROR', 2),
(42, 'fertoz', '12345F', 0, 1, 'https://scontent-iad3-1.xx.fbcdn.net/v/t1.0-9/14470543_1831832793715804_6465436184372509020_n.jpg?oh=558968759d8947c3402494ec8403181d&oe=58944CD4', 'XXXEERRE', 3);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `tnt_aspirantes`
--
ALTER TABLE `tnt_aspirantes`
  ADD PRIMARY KEY (`idtnt_aspirantes`),
  ADD KEY `fk_tnt_aspirantes_tnt_proveedor1` (`id_proveedor`),
  ADD KEY `fk_tnt_aspirantes_tnt_servicio1` (`id_servicio`);

--
-- Indices de la tabla `tnt_calificacion`
--
ALTER TABLE `tnt_calificacion`
  ADD PRIMARY KEY (`idtnt_calificacion`),
  ADD KEY `fk_cliente_calificacion` (`id_cliente`),
  ADD KEY `fk_proveedor_calificacion` (`id_proveedor`),
  ADD KEY `fk_servicio_calificacion` (`id_servicio`);

--
-- Indices de la tabla `tnt_ciudad`
--
ALTER TABLE `tnt_ciudad`
  ADD PRIMARY KEY (`idtnt_ciudad`),
  ADD KEY `Paises_Codigo` (`paises_Codigo`),
  ADD KEY `ciudad` (`ciudad`);

--
-- Indices de la tabla `tnt_cliente`
--
ALTER TABLE `tnt_cliente`
  ADD PRIMARY KEY (`idtnt_cliente`),
  ADD KEY `fk_tnt_cliente_tnt_usaurio1` (`id_usuario`);

--
-- Indices de la tabla `tnt_detalles`
--
ALTER TABLE `tnt_detalles`
  ADD PRIMARY KEY (`idtnt_detalles`),
  ADD KEY `fk_tnt_detalles_tnt_servicio1` (`id_servicio`);

--
-- Indices de la tabla `tnt_membresia`
--
ALTER TABLE `tnt_membresia`
  ADD PRIMARY KEY (`idtnt_membresia`),
  ADD KEY `fk_tnt_membresia_tnt_plan1` (`id_plan`),
  ADD KEY `fk_tnt_membresia_tnt_cliente1` (`id_cliente`);

--
-- Indices de la tabla `tnt_pais`
--
ALTER TABLE `tnt_pais`
  ADD PRIMARY KEY (`idtnt_pais`);

--
-- Indices de la tabla `tnt_plan`
--
ALTER TABLE `tnt_plan`
  ADD PRIMARY KEY (`idtnt_plan`);

--
-- Indices de la tabla `tnt_proveedor`
--
ALTER TABLE `tnt_proveedor`
  ADD PRIMARY KEY (`idtnt_proveedor`),
  ADD KEY `fk_tnt_proveedor_tnt_usaurio` (`id_usuario`);

--
-- Indices de la tabla `tnt_recursos`
--
ALTER TABLE `tnt_recursos`
  ADD PRIMARY KEY (`idtnt_recursos`),
  ADD KEY `fk_tnt_recursos_tnt_usaurio1` (`id_usuario`);

--
-- Indices de la tabla `tnt_servicio`
--
ALTER TABLE `tnt_servicio`
  ADD PRIMARY KEY (`idtnt_servicio`),
  ADD KEY `fk_cliente` (`id_cliente`);

--
-- Indices de la tabla `tnt_usuario`
--
ALTER TABLE `tnt_usuario`
  ADD PRIMARY KEY (`idtnt_usuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `tnt_aspirantes`
--
ALTER TABLE `tnt_aspirantes`
  MODIFY `idtnt_aspirantes` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `tnt_calificacion`
--
ALTER TABLE `tnt_calificacion`
  MODIFY `idtnt_calificacion` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `tnt_ciudad`
--
ALTER TABLE `tnt_ciudad`
  MODIFY `idtnt_ciudad` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=269413;
--
-- AUTO_INCREMENT de la tabla `tnt_cliente`
--
ALTER TABLE `tnt_cliente`
  MODIFY `idtnt_cliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT de la tabla `tnt_detalles`
--
ALTER TABLE `tnt_detalles`
  MODIFY `idtnt_detalles` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `tnt_membresia`
--
ALTER TABLE `tnt_membresia`
  MODIFY `idtnt_membresia` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `tnt_plan`
--
ALTER TABLE `tnt_plan`
  MODIFY `idtnt_plan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `tnt_proveedor`
--
ALTER TABLE `tnt_proveedor`
  MODIFY `idtnt_proveedor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `tnt_recursos`
--
ALTER TABLE `tnt_recursos`
  MODIFY `idtnt_recursos` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT de la tabla `tnt_servicio`
--
ALTER TABLE `tnt_servicio`
  MODIFY `idtnt_servicio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT de la tabla `tnt_usuario`
--
ALTER TABLE `tnt_usuario`
  MODIFY `idtnt_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `tnt_aspirantes`
--
ALTER TABLE `tnt_aspirantes`
  ADD CONSTRAINT `fk_tnt_aspirantes_tnt_proveedor1` FOREIGN KEY (`id_proveedor`) REFERENCES `tnt_proveedor` (`idtnt_proveedor`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tnt_aspirantes_tnt_servicio1` FOREIGN KEY (`id_servicio`) REFERENCES `tnt_servicio` (`idtnt_servicio`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `tnt_calificacion`
--
ALTER TABLE `tnt_calificacion`
  ADD CONSTRAINT `fk_cliente_calificacion` FOREIGN KEY (`id_cliente`) REFERENCES `tnt_cliente` (`idtnt_cliente`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_proveedor_calificacion` FOREIGN KEY (`id_proveedor`) REFERENCES `tnt_proveedor` (`idtnt_proveedor`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_servicio_calificacion` FOREIGN KEY (`id_servicio`) REFERENCES `tnt_servicio` (`idtnt_servicio`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `tnt_cliente`
--
ALTER TABLE `tnt_cliente`
  ADD CONSTRAINT `fk_tnt_cliente_tnt_usaurio1` FOREIGN KEY (`id_usuario`) REFERENCES `tnt_usuario` (`idtnt_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `tnt_detalles`
--
ALTER TABLE `tnt_detalles`
  ADD CONSTRAINT `fk_tnt_detalles_tnt_servicio1` FOREIGN KEY (`id_servicio`) REFERENCES `tnt_servicio` (`idtnt_servicio`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `tnt_membresia`
--
ALTER TABLE `tnt_membresia`
  ADD CONSTRAINT `fk_tnt_membresia_tnt_cliente1` FOREIGN KEY (`id_cliente`) REFERENCES `tnt_cliente` (`idtnt_cliente`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_tnt_membresia_tnt_plan1` FOREIGN KEY (`id_plan`) REFERENCES `tnt_plan` (`idtnt_plan`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `tnt_proveedor`
--
ALTER TABLE `tnt_proveedor`
  ADD CONSTRAINT `fk_tnt_proveedor_tnt_usaurio` FOREIGN KEY (`id_usuario`) REFERENCES `tnt_usuario` (`idtnt_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `tnt_recursos`
--
ALTER TABLE `tnt_recursos`
  ADD CONSTRAINT `fk_tnt_recursos_tnt_usaurio1` FOREIGN KEY (`id_usuario`) REFERENCES `tnt_usuario` (`idtnt_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `tnt_servicio`
--
ALTER TABLE `tnt_servicio`
  ADD CONSTRAINT `fk_cliente` FOREIGN KEY (`id_cliente`) REFERENCES `tnt_cliente` (`idtnt_cliente`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
