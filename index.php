
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Tentazione</title>


        <!-- Facebook and Twitter integration -->
        <meta property="og:title" content=""/>
        <meta property="og:image" content=""/>
        <meta property="og:url" content=""/>
        <meta property="og:site_name" content=""/>
        <meta property="og:description" content=""/>
        <meta name="twitter:title" content="" />
        <meta name="twitter:image" content="" />
        <meta name="twitter:url" content="" />
        <meta name="twitter:card" content="" />

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel="shortcut icon" href="favicon.ico">

        <!--        <link href="https://fonts.googleapis.com/css?family=Raleway:200,300,400,700" rel="stylesheet">-->

        <!-- Animate.css -->
        <link rel="stylesheet" href="css/animate.css">
        <!-- Icomoon Icon Fonts-->
        <link rel="stylesheet" href="css/icomoon.css">

        <!-- Font Awesome -->
        <link href="css/font-awesome.min.css" rel="stylesheet">


        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!-- Pace -->
        <link href="css/pace.css" rel="stylesheet">

        <!-- Flexslider  -->
        <link rel="stylesheet" href="css/flexslider.css">
        <!-- Owl Carousel  -->
        <link rel="stylesheet" href="css/owl.carousel.min.css">
        <link rel="stylesheet" href="css/owl.theme.default.min.css">
        <!-- Theme style  -->
        <link rel="stylesheet" href="css/style.css">


        <!-- jQuery -->
        <script src="js/jquery-1.10.2.min.js"></script>

        <!-- Bootstrap -->
        <script src="bootstrap/js/bootstrap.min.js"></script>

        <!-- Waypoints -->
        <script src="js/jquery.waypoints.min.js"></script>
        <!-- Owl Carousel -->
        <script src="js/owl.carousel.min.js"></script>
        <!-- Flexslider -->
        <script src="js/jquery.flexslider-min.js"></script>

        <!-- MAIN JS -->
        <script src="js/main.js"></script>

        <!-- Pace -->
        <script src='js/pace.min.js'></script>

        <!-- Popup Overlay -->
        <script src='js/jquery.popupoverlay.min.js'></script>

        <!-- Slimscroll -->
        <script src='js/jquery.slimscroll.min.js'></script>

        <!-- Cookie -->
        <script src='js/jquery.cookie.min.js'></script>

        <!-- Modernizr -->
        <script src='js/modernizr.min.js'></script>

        <!--Fontawaesome-->
        <script src="https://use.fontawesome.com/835980aa1c.js"></script>








    </head>
    <body>
        <?php
        if (isset($_GET['create'])) {
            if ($_GET['create'] == "ok") {
                echo "    <div class=\"alert alert-success\">
                <strong>Felicidades!!</strong> Tu registro se a completado con exito empieza ya a disfrutar de nuestros servicios
                </div>";
            }
        }
        ?>

        <div id="fh5co-page">
            <header id="fh5co-header" role="banner">
                <div class="container">

                    <div class="header-inner">
                        <img src="img/Logo_1.png" ></img>




                    </div>
                </div>
            </header>

            <div class="container">
                <div class="form-group">
                    <div class="dropdown">
                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            Unete ya!!<i class="fa fa-user-o" aria-hidden="true"></i>
                            <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                            <li><a href="/tentazione/templates/registarCliente.php"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Registrate como cliente</a></li>
                            <li><a href="/tentazione/templates/registrarprovedor.php"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Registrate como provedor</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--
            <aside id="fh5co-hero" class="js-fullheight">
                <div class="flexslider js-fullheight">
                    <ul class="slides">
                        <li style="background-image: url(img/ins0.jpg);">
                            <div class="overlay-gradient"></div>
                            <div class="container">
                                <div class="col-md-10 col-md-offset-1 text-center js-fullheight slider-text">
                                    <div class="slider-text-inner">
                                        
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li style="background-image: url(img/ins1.jpg);">
                            <div class="container">
                                <div class="col-md-10 col-md-offset-1 text-center js-fullheight slider-text">
                                    <div class="slider-text-inner">
                                 
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li style="background-image: url(img/ins2.jpg);">
                            <div class="container">
                                <div class="col-md-10 col-md-offset-1 text-center js-fullheight slider-text">
                                    <div class="slider-text-inner">
                                       
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li style="background-image: url(img/ins3.jpg);">
                            <div class="container">
                                <div class="col-md-10 col-md-offset-1 text-center js-fullheight slider-text">
                                    <div class="slider-text-inner">
                                       
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </aside>
            -->
            <aside id="fh5co-hero" class="js-fullheight">
                <div class="flexslider js-fullheight">
                    <ul class="slides">
                        <li style="background-image: url(img/cita.jpg);">
                            <div class="overlay-gradient"></div>
                            <div class="container">
                                <div class="col-md-10 col-md-offset-1 text-center js-fullheight slider-text">
                                    <div class="slider-text-inner">
                                        <form class="navbar-form navbar-right" >
                                                 <h2 >Una cita que cumplira tus fantasias </h2>

                                            <div class="form-group">
                                                <label style="color: white">Usuario:</label>
                                                <input id="user" type="text" placeholder="Username"  >
                                            </div>
                                            <div class="form-group">
                                                <label style="color: white">Contrase&ntilde;a:</label>
                                                <input id="pass" type="password" placeholder="Password">
                                            </div>
                                                 <br>
                                            <div class="form-group">
                                                <label></label>
                                                <a class="btn btn-danger btn-sm  animation-delay5 login-link pull-right" href="#" onclick="javascript:logIn()"><i class="fa fa-sign-in"></i> Ingresar</a>
                                            </div>

                                        </form>
                                   
                                     
                                    </div>
                                </div>
                            </div>
                        </li>
                        <!--<li style="background-image: url(img/slide_2.jpg);">
                                <div class="container">
                                        <div class="col-md-10 col-md-offset-1 text-center js-fullheight slider-text">
                                                <div class="slider-text-inner">
                                                        <h2>Take Your Business To The Next Level</h2>
                                                        <p><a href="#" class="btn btn-danger btn-lg">Get started</a></p>
                                                </div>
                                        </div>
                                </div>
                        </li>
                        <li style="background-image: url(img/slide_3.jpg);">
                                <div class="container">
                                        <div class="col-md-10 col-md-offset-1 text-center js-fullheight slider-text">
                                                <div class="slider-text-inner">
                                                        <h2>We Think Different That Others Can't</h2>
                                                        <p><a href="#" class="btn btn-danger btn-lg">Get started</a></p>
                                                </div>
                                        </div>
                                </div>
                        </li>-->
                    </ul>
                </div>
            </aside>
            <div id="fh5co-services-section">
                
                <div class="container">


                    <!-- Titulo -->
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3 text-center fh5co-heading animate-box">
                            <h2>Instrucciones Tentazione</h2>
                            <p> Habilitadas para pc y celulares </p>
                        </div>
                    </div>


                    <!-- Sección de acompañantes -->

                    <div class="row">
                        <div class="col-md-4 ">
                            <img src="img/app1.png">
                        </div>
                        <div class="col-md-4">
                             <img src="img/app2.png">
                        </div>
                        <div class="col-md-4">
                            <img src="img/app3.png">
                        </div>
                        <div class="col-md-4">
                              <img src="img/app4.png">
                        </div>
                        <div class="col-md-4 ">
                              <img src="img/app5.png">
                        </div>
                       
                    </div>
                </div>
                
                
                <div class="container">


                    <!-- Titulo -->
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3 text-center fh5co-heading animate-box">
                            <h2>Nuestras bellas acompa&ntilde;antes</h2>
                            <p>Le har&aacute;n pasar una cita inolvidable </p>
                        </div>
                    </div>


                    <!-- Sección de acompañantes -->

                    <div class="row">
                        <div class="col-md-4 animate-box">
                            <div class="thumbnail"  style="width:280px; height:320px;">
                                <div id="myCarousel1" class="carousel slide" data-ride="carousel">
                                    <ol id="carousel-indicators1" class="carousel-indicators"></ol>
                                    <div id="carousel-inner1" class="carousel-inner" role="listbox"></div>

                                    <a class="left carousel-control" href="#myCarousel1" role="button" data-slide="prev">
                                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="right carousel-control" href="#myCarousel1" role="button" data-slide="next">
                                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                                <div id="caption1" class="caption">



                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 animate-box">
                            <div class="thumbnail" style="width:280px; height:320px;">
                                <div id="myCarousel2" class="carousel slide" data-ride="carousel">
                                    <ol id="carousel-indicators2" class="carousel-indicators"></ol>
                                    <div id="carousel-inner2" class="carousel-inner" role="listbox"></div>

                                    <!-- Left and right controls -->
                                    <a class="left carousel-control" href="#myCarousel2" role="button" data-slide="prev">
                                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="right carousel-control" href="#myCarousel2" role="button" data-slide="next">
                                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                                <div id="caption2" class="caption">


                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 animate-box">
                            <div class="thumbnail" style="width:280px; height:320px;">
                                <div id="myCarousel3" class="carousel slide" data-ride="carousel">
                                    <ol id="carousel-indicators3" class="carousel-indicators"></ol>
                                    <div id="carousel-inner3" class="carousel-inner" role="listbox"></div>
                                    <!-- Left and right controls -->
                                    <a class="left carousel-control" href="#myCarousel3" role="button" data-slide="prev">
                                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="right carousel-control" href="#myCarousel3" role="button" data-slide="next">
                                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>

                                </div>
                                <div id="caption3" class="caption">

                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 animate-box">
                            <div class="thumbnail" style="width:280px; height:320px;">
                                <div id="myCarousel4" class="carousel slide" data-ride="carousel">
                                    <ol id="carousel-indicators4" class="carousel-indicators"></ol>
                                    <div id="carousel-inner4" class="carousel-inner" role="listbox"></div>
                                    <!-- Left and right controls -->
                                    <a class="left carousel-control" href="#myCarousel4" role="button" data-slide="prev">
                                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="right carousel-control" href="#myCarousel4" role="button" data-slide="next">
                                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                                <div id="caption4" class="caption">


                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 animate-box">
                            <div class="thumbnail" style="width:280px; height:320px;">
                                <div id="myCarousel5" class="carousel slide" data-ride="carousel">
                                    <ol id="carousel-indicators5" class="carousel-indicators"></ol>
                                    <div id="carousel-inner5" class="carousel-inner" role="listbox"></div>
                                    <!-- Left and right controls -->
                                    <a class="left carousel-control" href="#myCarousel5" role="button" data-slide="prev">
                                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="right carousel-control" href="#myCarousel5" role="button" data-slide="next">
                                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                                <div id="caption5" class="caption">

                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 animate-box">
                            <div class="thumbnail" style="width:280px; height:320px;">
                                <div id="myCarousel6" class="carousel slide" data-ride="carousel">
                                    <ol id="carousel-indicators6"class="carousel-indicators"></ol>
                                    <div id="carousel-inner6" class="carousel-inner" role="listbox"></div>
                                    <!-- Left and right controls -->
                                    <a class="left carousel-control" href="#myCarousel6" role="button" data-slide="prev">
                                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="right carousel-control" href="#myCarousel6" role="button" data-slide="next">
                                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>



                                <div id="caption6" class="caption">


                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>


            <footer id="fh5co-footer" role="contentinfo">

                <div class="container">
                    <div class="col-md-2 col-md-push-1 col-sm-12 col-sm-push-0 col-xs-12 col-xs-push-0">
                        <h3>Siguenos</h3>
                        <a href="#"><i class="icon-twitter"></i></a>
                        <a href="#"><i class="icon-facebook"></i></a>
                        <a href="#"><i class="icon-google-plus"></i></a>
                        <a href="#"><i class="icon-instagram"></i></a>

                    </div>


                    <div class="col-md-12 fh5co-copyright text-center">
                        <p>&copy; Esta protegido por los derechos de copyright All Rights Reserved. <span>Tentazione</span></p>	
                    </div>

                </div>
            </footer>
        </div>





        <script type="text/javascript">
            $(document).ready(function () {
                loadProviders();
            });


            function loadProviders() {

                $.post("Services/loadProvidersHome.php",
                        function (data) {
                            var id = 1;
                            for (var j = 0; j < data.DATA.length; j++) {

                                if (data.DATA[j].IMAGENES.length > 0) {

                                    var list_carrusel = "";
                                    var img_carrusel = "";
                                    var activo = false;
                                    var contImg = 0;
                                    for (var i = 0; i < data.DATA[j].IMAGENES.length; i++)
                                    {
                                        console.log(data.DATA[j].IMAGENES[i].visisbilidad);
                                        if (data.DATA[j].IMAGENES[i].visisbilidad === "1") {


                                            if (!activo) {
                                                list_carrusel += "<li data-target=\"#myCarousel" + id + "\" data-slide-to=\"" + contImg + "\" class='active' ></li>";

                                                activo = true;
                                                img_carrusel += "<div class=\"item active\"><img style='width: 100%; height: 250px;' class=\"img-responsive\" src=\"" + data.DATA[j].IMAGENES[i].url_almacenamiento
                                                        + "\"/></div>";
                                            } else {
                                                list_carrusel += "<li data-target=\"#myCarousel" + id + "\" data-slide-to=\"" + contImg + "\" ></li>";

                                                img_carrusel += "<div class=\"item\"><img style='width: 100%; height: 250px;'   class=\"img-responsive\" src=\"" + data.DATA[j].IMAGENES[i].url_almacenamiento
                                                        + "\"/></div>";
                                            }
                                            contImg++;
                                        }
                                    }







                                    $("#carousel-indicators" + id).html(list_carrusel);
                                    $("#carousel-inner" + id).html(img_carrusel);
                                    $("#caption" + id).html("<h3>" + data.DATA[j].nombres + " " + data.DATA[j].apellidos + "</h3>");


                                    id++;
                                }




                            }




                        },
                        "json");
            }


            /* var connect = new Connect(firebase);
             function loginInUserConnected(user) {
             var refConnected = connect.ref().child("connected");
             refConnected.push({"user": user.nickname});
             }*/
            function logIn() {
                var user = $('#user').val();
                if (user == '') {
                    $('#user').focus();
                    return;
                }

                var pass = $('#pass').val();
                if (pass == '') {
                    $('#pass').focus();
                    return;
                }
                var dataIn = {"user": $('#user').val(), "pass": $('#pass').val()};
                $.post("Services/LogIn.php",
                        dataIn,
                        function (data) {

                            if (data.STATUS === 'OK') {
                                //user = new User(data.DATA[0].nick, "jeffersson.sinza@gmail.com", data.DATA[0].password, data.DATA[0].profilePhoto, data.DATA[0].tipo);
                                if (data.DATA[0].tipo == 1) {

                                    window.location = "home.php";
                                }
                                if (data.DATA[0].tipo == 2) {

                                    window.location = "customerHome.php";
                                }

                                if (data.DATA[0].tipo == 3) {

                                    window.location = "providerHome.php";
                                }
                                //loginInUserConnected(user);
                            } else {
                                window.location = "index.php";
                                alert('ERROR... Datos incorrectos');
                            }


                        },
                        "json");
            }

        </script>

    </body>
</html>

