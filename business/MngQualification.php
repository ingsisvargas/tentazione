<?php

include_once '../dao/Qualification_DAO.php';
include_once '../dao/Customer_DAO.php';
include_once '../dao/Provider_DAO.php';
include_once '../dao/Services_DAO.php';

/**
 * Description of MngQualification
 *
 * @author jeffersson
 */
class MngQualification {

    /**
     * 
     * @param  $idUser  identificador de usuario
     * @param  $type    tipo de usuario
     * @param  $score   puntaje
     * @param  $description descripción
     * @param  $idService identificador del servicio
     * @return json  confirmación del resultado
     */
    public function addQualification($idUser, $type, $score, $description, $idService) {
        $qualificationDAO = new Qualification_DAO();
        $qualification = array("id_cliente" => "null", "id_proveedor" => "null", "puntuacion" => $score
            , "descripcion" => $description, "id_servicio" => $idService);

        if ($type == "2") {

            $providerDAO = new Provider_DAO();
            $servicioDAO = new Services_DAO();
            $service = $servicioDAO->LoadService($idService);
            $qualification["id_proveedor"] = $service['DATA']['id_proveedor'];
            $result = $qualificationDAO->addQualification($qualification);
            $promedio = $qualificationDAO->loadPromedio($service['DATA']['id_proveedor'], $type);
            
            $category = "A";
            
            if($promedio <= 2){
               $category = "A"; 
            } 
            if($promedio > 2  && $promedio <=4){
                $category="AA";
            }
            if($promedio > 4 ){
                $category = "AAA";
            }
            
            
            $providerDAO->updateQualification($service['DATA']['id_proveedor'], $promedio,$category);
        } else {
            $customerDAO = new Customer_DAO();
            $servicioDAO = new Services_DAO();
            $service = $servicioDAO->LoadService($idService);
            $qualification["id_cliente"] = $service['DATA']['id_cliente'];
            $result = $qualificationDAO->addQualification($qualification);
            $promedio = $qualificationDAO->loadPromedio($service['DATA']['id_cliente'], $type);
            $customerDAO->updateQualification($service['DATA']['id_cliente'], $promedio);
        }

        return json_encode($result);
    }

    public function validateQualification($idUser, $type, $idService) {
        $qualificationDAO = new Qualification_DAO();
        $customerDAO = new Customer_DAO();
        $providerDAO = new Provider_DAO();
        $qualification = array("id_cliente" => "null", "id_proveedor" => "null", "id_servicio" => $idService);
        if ($type == "2") {
            $qualification["id_cliente"] = $customerDAO->loadCustomerByUser($idUser)['idtnt_cliente'];
        } else {
            $qualification["id_proveedor"] = $providerDAO->LoadProvider($idUser)['idtnt_proveedor'];
        }

        $result = $qualificationDAO->validateQualification($qualification);
        return json_encode($result);
    }

}
