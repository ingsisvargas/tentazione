<?php

include_once '../dao/Customer_DAO.php';
include_once '../dao/MembershipDAO.php';
include_once '../dao/Transference_DAO.php';

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MngTransferences
 *
 * @author roan0
 */
class MngTransferences {

    function __construct() {
        
    }

    public function updateTransference($id_transference, $estado) {
        $transference_DAO = new Transference_DAO();
        $data = $transference_DAO->updateTransference($id_transference,$estado);
        return json_encode($data);
        
    }

    /**
     * Aplica la transferencia de un pago por servicio
     * @param type $idService
     * @param type $type
     */
    public function applyTransferPayService($idService, $type, $valor) {
        $estado = "PENDIENTE";
        $tipo = "SERVICIO";

        session_start();
        $idUser = $_SESSION['DATA']['idtnt_usuario'];
        $customerDAO = new Customer_DAO();
        $cliente = $customerDAO->LoadPersonalInfo($idUser);
        $descripcion = "";
        $descripcionTransferencia = $type;



        $transferenceDAO = new Transference_DAO();

        $idTransference = $transferenceDAO->addTransference($estado, $valor, null, $idService, $tipo);

        $Firma = 'hdWv1c0IK2I84tAB5dc4JLlHI8~' . '611903~' . $idTransference . '~' . $valor . '~USD';
        $signature = md5($Firma);

        $result = array();

        if ($idTransference != null) {
            $result ['STATUS'] = "OK";
            $result['DATA'] = array();
            $result['DATA']['id_transferencia'] = $idTransference;
            $result['DATA']['valor'] = $valor;
            $result['DATA']['nombreFull'] = $cliente['nombres'] . " " . $cliente['apellidos'];
            $result['DATA']['emailFull'] = $cliente['email'];
            $result['DATA']['descripcion'] = $descripcion;
            $result['DATA']['firma'] = $signature;
            $dataReturn = array();

            $dataReturn['idService'] = $idService;
            $dataReturn['idTransference'] = $idService;
            $dataReturn['stateService'] = 'AT';


            $result['DATA']['extra1'] = $descripcionTransferencia . '-' . json_encode($dataReturn);
        } else {
            $result['STATUS'] = 'ERROR';
        }


        return json_encode($result);
    }

    /**
     * 
     * @param type $idService
     * @param type $idPlan
     * @return type
     */
    public function applyTransferMembershipOrChat($idService, $idPlan, $typetransfer, $idProveedor) {
        $estado = "PENDIENTE";
        $valor = 0;
        $tipo = "CHAT";
        $idMembership = null;
        session_start();
        $idUser = $_SESSION['DATA']['idtnt_usuario'];
        $customerDAO = new Customer_DAO();
        $id_cliente = $customerDAO->loadCustomerByUser($idUser)['idtnt_cliente'];
        $cliente = $customerDAO->LoadPersonalInfo($idUser);
        $descripcion = "";
        $descripcionTransferencia = $typetransfer;

        switch ($idPlan) {

            case -1:
                $valor = 5;
                $descripcion = "Pago por chat de servicio";
                $descripcionTransferencia = 'SERTRA';
                break;
            case 2:
                $tipo = "MEMBRESIA";
                $valor = 20;
                $fecha_inicio = date('Y-m-d');
                $fecha_fin = date("Y-m-d", strtotime(date("Y-m-d") . " +30 day"));
                $memberShipDAO = new MembershipDAO();
                $descripcion = "Pago por Memebresia de 1 mes";

                $idMembership = $memberShipDAO->registerMembership($fecha_inicio, $fecha_fin, $id_cliente, $estado, $idPlan);
                $idService = null;
                break;

            case 3:

                $fecha_inicio = date('Y-m-d');
                $tipo = "MEMBRESIA";
                $fecha_fin = date("Y-m-d", strtotime(date("Y-m-d") . " +180 day"));
                $valor = 96;

                $memberShipDAO = new MembershipDAO();
                $descripcion = "Pago por Memebresia de 6 meses";
                $idMembership = $memberShipDAO->registerMembership($fecha_inicio, $fecha_fin, $id_cliente, $estado, $idPlan);
                $idService = null;
                break;


            case 4:
                $tipo = "MEMBRESIA";
                $fecha_inicio = date('Y-m-d');
                $fecha_fin = date("Y-m-d", strtotime(date("Y-m-d") . " +365 day"));
                $valor = 144;

                $descripcion = "Pago por Memebresia de 1 año";
                $memberShipDAO = new MembershipDAO();
                $idMembership = $memberShipDAO->registerMembership($fecha_inicio, $fecha_fin, $id_cliente, $estado, $idPlan);
                $idService = null;
                break;

            default:
        }

        $transferenceDAO = new Transference_DAO();

        $idTransference = $transferenceDAO->addTransference($estado, $valor, $idMembership, $idService, $tipo);

        $Firma = 'hdWv1c0IK2I84tAB5dc4JLlHI8~' . '611903~' . $idTransference . '~' . $valor . '~USD';
        $signature = md5($Firma);


        $result = array();

        if ($idTransference != null) {
            $result ['STATUS'] = "OK";
            $result['DATA'] = array();
            $result['DATA']['id_transferencia'] = $idTransference;
            $result['DATA']['valor'] = $valor;
            $result['DATA']['nombreFull'] = $cliente['nombres'] . " " . $cliente['apellidos'];
            $result['DATA']['emailFull'] = $cliente['email'];
            $result['DATA']['descripcion'] = $descripcion;
            $result['DATA']['firma'] = $signature;


            $dataReturn = array();


            $dataReturn['idMembership'] = $idMembership;
            $dataReturn['idService'] = $idService;
            $dataReturn['idTransference'] = $idService;
            $dataReturn['stateService'] = 'AB';
            $dataReturn['idProvider'] = $idProveedor;

            $result['DATA']['extra1'] = $descripcionTransferencia . '-' . json_encode($dataReturn);
        } else {
            $result['STATUS'] = 'ERROR';
        }


        return json_encode($result);
    }

    //put your code here
}
