<?php

include_once '../dao/Services_DAO.php';
include_once '../dao/Customer_DAO.php';
include_once '../dao/Provider_DAO.php';
include_once '../dao/Candidate_DAO.php';
include_once '../dao/MembershipDAO.php';

/**
 * Description of MngServices
 * Clase destinada a la gestión lógica del modelo de negocio, respecto a Servicios.
 * @author Ronald Vargas
 */
class MngServices {

    function __construct() {
        
    }

    function loadServicePrice($id_service) {
        $servicesDAO = new Services_DAO();
        $result = $servicesDAO->loadDurationMinutes($id_service);
        $tiempo = $result['minutes'];
        $categoria = $result['categoria'];
        $valor = 0;
        $descuento = 0;

        $memberShipDAO = new MembershipDAO();
        $data = $memberShipDAO->validateStatusChatPay($id_service);

        if ($data['DATA'][0]['cantidad'] > 0) {
            $descuento = 5;
        }

        $horas = $tiempo / 60;
        if ($horas % 60 != 0) {
            $horas = $horas + 1;
        }

        if ($categoria == 'A') {
            $valor = $horas * 25;
        } else if ($categoria == 'AA') {
            $valor = $horas * 40;
        } else if ($categoria == 'AAA') {
            $valor = $horas * 65;
        }

        $data = array();

        $valor = round($valor, 0);
        $valor = $valor - $descuento + 10;
        $data['STATUS'] = 'OK';
        $data['VALOR'] = $valor;

        return json_encode($data);
    }

    /**
     * Permite la carga de todos los Servicios registrados en el sistema a la fecha
     * @return $result -> array con el resultado de la operación codificado en JSON
     */
    public function LoadServices() {
        $servicesDAO = new Services_DAO();
        $result = $servicesDAO->LoadServices();
        return json_encode($result);
    }

    public function confirmService($nick, $id_service) {
        $servicesDAO = new Services_DAO();
        $result = $servicesDAO->confirmService($nick, $id_service);
        return json_encode($result);
    }

    /**
     * Permite la carga de un servicio registrado en el sistema a partir de su id
     * @param type $id -> Identificador del servicio a cargar.
     * @return $result -> array con el resultado de la operación codificado en JSON
     */
    public function LoadService($id) {
        $servicesDAO = new Services_DAO();
        $result = $servicesDAO->LoadService($id);
        return json_encode($result);
    }

    /**
     * Permite la carga de un servicio registrado en el sistema a partir de su id
     * @param type $id -> Identificador del servicio a cargar.
     * @return $result -> array con el resultado de la operación codificado en JSON
     */
    public function LoadService2($id) {
        $servicesDAO = new Services_DAO();
        $result = $servicesDAO->LoadService2($id);
        return json_encode($result);
    }

    /**
     * Permite registrar una solicitud de servicio en el sistema.
     * 
     * @param type $idCustomer
     * @param type $fecha_servicio
     * @param type $hora_fin
     * @param type $hora_inicio
     * @param type $descripcion
     * @param type $plan
     * @param type $categoria
     */
    public function registerService($idUser, $fecha_servicio, $hora_fin, $hora_inicio, $descripcion, $plan, $categoria) {

        $customerDAO = new Customer_DAO();

        $res = $customerDAO->loadCustomerByUser($idUser);
        $idCustomer = $res['idtnt_cliente'];

        $servicesDAO = new Services_DAO();
        $result = $servicesDAO->registerService($idCustomer, $fecha_servicio, $hora_fin, $hora_inicio, $descripcion, $plan, $categoria);

        return json_encode($result);
    }

    /**
     * Permite cargar los servicios que aun no han sido calificados para un usuario
     * @param type $idUser
     * @param type $type
     * @return type
     */
    public function loadServicesCalification($idUser, $type) {

        $result;
        $serviceDAO = new Services_DAO();


        if ($type) {
            $customerDAO = new Customer_DAO();
            $customer = $customerDAO->loadCustomerByUser($idUser);
            $result = $serviceDAO->loadServicesCalification($customer['idtnt_cliente'], $type);
        } else {
            $providerDAO = new Provider_DAO();
            $provider = $providerDAO->loadproviderByUser($idUser);
            $result = $serviceDAO->loadServicesCalification($provider['idtnt_proveedor'], $type);
        }

        return json_encode($result);
    }

    /**
     * Obtiene los Servicios de un usuario, proveedor o cliente y a partir de un estado.
     * @param type $idUser
     * @param type $type
     * @param type $state
     * @return type
     */
    public function getServices($idUser, $type, $state) {
        $result;
        $serviceDAO = new Services_DAO();
        $service = array("id_cliente" => "null", "id_proveedor" => "null", "estado" => $state);
        if ($type == "2") {
            $customerDAO = new Customer_DAO();
            $service["id_cliente"] = $customerDAO->loadCustomerByUser($idUser)['idtnt_cliente'];
        } else {
            $providerDAO = new Provider_DAO();
            $service["id_proveedor"] = $providerDAO->loadProviderByUser($idUser)['idtnt_proveedor'];
        }
        $result = $serviceDAO->GetServices($service, $type, $state);
        return json_encode($result);
    }

    /**
     * 
     * @param type $idService
     * @return type
     */
    public function loadCandidates($idService, $estado) {
        $result;
        if ($estado == "AB") {
            $candidateDAO = new Candidate_DAO();
            $result = $candidateDAO->loadCandidates($idService);
            return json_encode($result);
        } else {
            $servicioDAO = new Services_DAO();
            $service = $servicioDAO->LoadService($idService);
            $providerDAO = new Provider_DAO();
            $result = $providerDAO->LoadProvider($service['DATA']['idtnt_servicio']);
            return json_encode($result);
        }
    }

    /**
     * 
     * @param type $idService
     * @return type
     */
    public function loadCandidate($idService, $id_user) {

        $candidateDAO = new Candidate_DAO();
        $providerDAO = new Provider_DAO();
        $id_provider = $providerDAO->loadProviderByUser($id_user)['idtnt_proveedor'];
        $result = $candidateDAO->loadCandidate($idService, $id_provider);
        return json_encode($result);
    }

    /**
     * Permite cargar los servicios disponibles para los que puede aplicar un
     * proveedor.
     * @param type $id_user
     * @return type
     */
    public function loadServicesAvailables($id_user) {
        $serviceDAO = new Services_DAO();
        $providerDAO = new Provider_DAO();

        $id_provider = $providerDAO->loadProviderByUser($id_user)['idtnt_proveedor'];
        $result = $serviceDAO->loadServicesAvailables($id_provider);
        return json_encode($result);
    }

    public function registerCandidate($id_user, $id_service) {
        $providerDAO = new Provider_DAO();
        $candidateDAO = new Candidate_DAO();
        $id_provider = $providerDAO->loadProviderByUser($id_user)['idtnt_proveedor'];
        $result = $candidateDAO->registerCandidate($id_provider, $id_service);
        return json_encode($result);
    }

}
