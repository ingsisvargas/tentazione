<?php

include_once '../dao/Resource_DAO.php';
include_once('../common/uploader.php');

/**
 * Description of mngFile
 *
 * @author jeffersson
 */
class mngResource {

    function __construct() {
        
    }

    /**
     * Función que permite cargar los recursos de un usuario
     * @param type $idUser
     */
    function loadResources($idUser) {
        $resourceDAO = new Resource_DAO();
        $data = $resourceDAO->loadResources($idUser);
        $result = array();
        if (count($data) > 0) {
            $result['DATA'] = $data;
            $result['STATUS'] = 'OK';
        } else {
            $result['DATA'] = $data;
            $result['STATUS'] = 'ERRROR';
        }
        return json_encode($result);
    }

    /**
     * Metodo que se encarga de guardar los archivos en el servidor y registrar
     * el recurso en la base de datos
     * @param  $files   arreglos de archivos
     * @param  $idUser identificador del cliente
     * @param  $type tipo de usuario
     * @param  $nickName apodo del cliente 
     * @return Json con el resultado del proceso
     */
    public function uploadResources($files, $idUser, $nickName) {
        $uploader = new Uploader();
        $uploadDir = 'uploads/' . $nickName . '/';
        $ResourceDAO = new Resource_DAO();
        $data = $uploader->upload($files, array(
            'limit' => 10, //Maximum Limit of files. {null, Number}
            'maxSize' => 10, //Maximum Size of files {null, Number(in MB's)}
            'extensions' => null, //Whitelist for file extension. {null, Array(ex: array('jpg', 'png'))}
            'required' => false, //Minimum one file is required for upload {Boolean}
            'uploadDir' => '../' . $uploadDir, //Upload directory {String}
            'title' => array('name'), //New file name {null, String, Array} *please read documentation in README.md
            'removeFiles' => true, //Enable file exclusion {Boolean(extra for jQuery.filer), String($_POST field name containing json data with file names)}
            'replace' => false, //Replace the file if it already exists  {Boolean}
            'perms' => null, //Uploaded file permisions {null, Number}
            'onCheck' => null, //A callback function name to be called by checking a file for errors (must return an array) | ($file) | Callback
            'onError' => null, //A callback function name to be called if an error occured (must return an array) | ($errors, $file) | Callback
            'onSuccess' => null, //A callback function name to be called if all files were successfully uploaded | ($files, $metas) | Callback
            'onUpload' => null, //A callback function name to be called if all files were successfully uploaded (must return an array) | ($file) | Callback
            'onComplete' => null, //A callback function name to be called when upload is complete | ($file) | Callback
            'onRemove' => null //A callback function name to be called by removing files (must return an array) | ($removed_files) | Callback
        ));
        $filesArray = $data['data'];
        $pathFile = "http://" . $_SERVER['HTTP_HOST'] . '/' . 'tentazione' . '/'  . $uploadDir . $filesArray['metas'][0]['name'];
       
        $resource = array("url_almacenamiento" => $pathFile, "id_usuario" => $idUser,
            "tipo" => 'FT', "visisbilidad" => 1);
        $ResourceDAO->addResources($resource);

        if ($data['isComplete']) {
            
            return json_encode($filesArray['metas'][0]['name']);
        }

        if ($data['hasErrors']) {
            $errors = $data['errors'];
            return json_encode($errors);
        }
    }

    
    
    
    
    
    
    
    
    /**
     * Metodo que se encarga de guardar los archivos en el servidor y registrar
     * el recurso en la base de datos
     * @param  $files   arreglos de archivos
     * @param  $idUser identificador del cliente
     * @param  $type tipo de usuario
     * @param  $nickName apodo del cliente 
     * @return Json con el resultado del proceso
     */
    public function uploadResourcesVideoPublico($files, $idUser, $nickName) {
        $uploader = new Uploader();
        $uploadDir = 'uploads/' . $nickName . '/';
        $ResourceDAO = new Resource_DAO();
        $data = $uploader->upload($files, array(
            'limit' => 10, //Maximum Limit of files. {null, Number}
            'maxSize' => 10, //Maximum Size of files {null, Number(in MB's)}
            'extensions' => null, //Whitelist for file extension. {null, Array(ex: array('jpg', 'png'))}
            'required' => false, //Minimum one file is required for upload {Boolean}
            'uploadDir' => '../' . $uploadDir, //Upload directory {String}
            'title' => array('name'), //New file name {null, String, Array} *please read documentation in README.md
            'removeFiles' => true, //Enable file exclusion {Boolean(extra for jQuery.filer), String($_POST field name containing json data with file names)}
            'replace' => false, //Replace the file if it already exists  {Boolean}
            'perms' => null, //Uploaded file permisions {null, Number}
            'onCheck' => null, //A callback function name to be called by checking a file for errors (must return an array) | ($file) | Callback
            'onError' => null, //A callback function name to be called if an error occured (must return an array) | ($errors, $file) | Callback
            'onSuccess' => null, //A callback function name to be called if all files were successfully uploaded | ($files, $metas) | Callback
            'onUpload' => null, //A callback function name to be called if all files were successfully uploaded (must return an array) | ($file) | Callback
            'onComplete' => null, //A callback function name to be called when upload is complete | ($file) | Callback
            'onRemove' => null //A callback function name to be called by removing files (must return an array) | ($removed_files) | Callback
        ));
        $filesArray = $data['data'];
        $pathFile = "http://" . $_SERVER['HTTP_HOST'] . '/' . 'Tentazione' . '/' . 'tentazione' . '/' . $uploadDir . $filesArray['metas'][0]['name'];

        $resource = array("url_almacenamiento" => $pathFile, "id_usuario" => $idUser,
            "tipo" => 'FT', "visisbilidad" => 1);
        $ResourceDAO->addResourcesVideoPublico($resource);

        if ($data['isComplete']) {

            return json_encode($filesArray['metas'][0]['name']);
        }

        if ($data['hasErrors']) {
            $errors = $data['errors'];
            return json_encode($errors);
        }
    }
    
    
    
    
    
    
    
    /**
     * Metodo que se encarga de guardar los archivos en el servidor y registrar
     * el recurso en la base de datos
     * @param  $files   arreglos de archivos
     * @param  $idUser identificador del cliente
     * @param  $type tipo de usuario
     * @param  $nickName apodo del cliente 
     * @return Json con el resultado del proceso
     */
    public function uploadResourcesVideoPrivado($files, $idUser, $nickName) {
        $uploader = new Uploader();
        $uploadDir = 'uploads/' . $nickName . '/';
        $ResourceDAO = new Resource_DAO();
        $data = $uploader->upload($files, array(
            'limit' => 10, //Maximum Limit of files. {null, Number}
            'maxSize' => 10, //Maximum Size of files {null, Number(in MB's)}
            'extensions' => null, //Whitelist for file extension. {null, Array(ex: array('jpg', 'png'))}
            'required' => false, //Minimum one file is required for upload {Boolean}
            'uploadDir' => '../' . $uploadDir, //Upload directory {String}
            'title' => array('name'), //New file name {null, String, Array} *please read documentation in README.md
            'removeFiles' => true, //Enable file exclusion {Boolean(extra for jQuery.filer), String($_POST field name containing json data with file names)}
            'replace' => false, //Replace the file if it already exists  {Boolean}
            'perms' => null, //Uploaded file permisions {null, Number}
            'onCheck' => null, //A callback function name to be called by checking a file for errors (must return an array) | ($file) | Callback
            'onError' => null, //A callback function name to be called if an error occured (must return an array) | ($errors, $file) | Callback
            'onSuccess' => null, //A callback function name to be called if all files were successfully uploaded | ($files, $metas) | Callback
            'onUpload' => null, //A callback function name to be called if all files were successfully uploaded (must return an array) | ($file) | Callback
            'onComplete' => null, //A callback function name to be called when upload is complete | ($file) | Callback
            'onRemove' => null //A callback function name to be called by removing files (must return an array) | ($removed_files) | Callback
        ));
        $filesArray = $data['data'];
        $pathFile = "http://" . $_SERVER['HTTP_HOST'] . '/' . 'Tentazione' . '/' . 'tentazione' .'/' . $uploadDir . $filesArray['metas'][0]['name'];

        $resource = array("es_video" => 'si', "url_almacenamiento" => $pathFile, "id_usuario" => $idUser,
            "tipo" => 'FT', "visisbilidad" => 0);
        $ResourceDAO->addResourcesVideoPrivado($resource);

        if ($data['isComplete']) {

            return json_encode($filesArray['metas'][0]['name']);
        }

        if ($data['hasErrors']) {
            $errors = $data['errors'];
            return json_encode($errors);
        }
    }
    
    
    
    
    
    
    
    
    
    
    public function removeResource($file) {
        if (file_exists($file)) {
            unlink($file);
        }
    }

    public function changeStateResource($estado, $id_resource) {
        
        $ResourceDAO = new Resource_DAO();
        $result = $ResourceDAO->changeStateResource($estado, $id_resource);
        return json_encode($result);
    }
    
    public function deleteResource($id_resource) {
        $ResourceDAO = new Resource_DAO();
        $result = $ResourceDAO->deleteResource($id_resource);
        return json_encode($result);
    }

}
