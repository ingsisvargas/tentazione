<?php

include_once '../dao/Customer_DAO.php';
include_once '../dao/Resource_DAO.php';

/**
 * Description of MngCustomer
 * Clase destinada a la gestión lógica del modelo de negocio respecto a Clientes.
 * @author Ronald Vargas
 */
class MngCustomer {

    function __construct() {
        
    }

    /**
     * Aplica la acción de registrar un cliente
     * @param type $data -> información para el registro
     * @return $result -> array con el resultado de la operación codificado en JSON
     */
    public function AddCustomer($data) {
        $customerDAO = new Customer_DAO();
        $result = $customerDAO->AddCustomer($data);
        return $result;
    }

    /**
     * Aplica la acción de Actualizar un cliente
     * @param type $data -> información para el registro
     * @return $result -> array con el resultado de la operación codificado en JSON
     */
    public function UpdateCustomer($data) {
        $customerDAO = new Customer_DAO();
        $result = $customerDAO->UpdateCustomer($data);
        return json_encode($result);
    }

    public function UpdateCustomerAdmin($data) {
        $customerDAO = new Customer_DAO();
        $result = $customerDAO->UpdateCustomerAdmin($data);
        return json_encode($result);
    }

    public function AceptCustomer($id) {
        $customerDAO = new Customer_DAO();
        $result = $customerDAO->AceptCustomer($id);
        return json_encode($result);
    }

    /**
     * Aplica la acción de Eliminar un cliente
     * @param type $id -> identificador del cliente a eliminar
     * @return $result -> array con el resultado de la operación codificado en JSON
     */
    public function DeleteCustomer($id) {
        $customerDAO = new Customer_DAO();
        $result = $customerDAO->DeleteCustomer($id);
        return json_encode($result);
    }

    /**
     * Permite la carga de todos los clientes registrados en el sistema
     * @return $result -> array con el resultado de la operación codificado en JSON
     */
    public function LoadCustomers() {
        $customerDAO = new Customer_DAO();
        $result = $customerDAO->LoadCustomers();
        return json_encode($result);
    }

    /**
     * Permite la carga de un cliente registrado en el sistema a partir de su id
     * @param type $id -> Identificador del cliente a cargar.
     * @return $result -> array con el resultado de la operación codificado en JSON
     */
    public function LoadCustomer($id) {
        $customerDAO = new Customer_DAO();
        $result = $customerDAO->LoadCustomer($id);
        return json_encode($result);
    }

    /**
     * Permite cargar la información personal del usuario en sesion.
     */
    public function LoadPersonalInfo($idUser) {
        $customerDAO = new Customer_DAO();
        $arrayPersonal = $customerDAO->LoadPersonalInfo($idUser);
        $resorceDAO = new Resource_DAO();
        $arrayPersonal['IMAGENES'] = $resorceDAO->loadResources($idUser);

        return json_encode($arrayPersonal);
    }

    public function loadPerfil($id_cliente) {
        $customerDAO = new Customer_DAO();
        $arrayPersonal = $customerDAO->loadPerfil($id_cliente);
        $resourceDAO = new Resource_DAO();
        $arrayPersonal['IMAGENES'] = $resourceDAO->loadResources($arrayPersonal['id_usuario']);
        return json_encode($arrayPersonal);
    }

    public function LocationConfiguration($pais, $region, $ciudad, $idUser) {

        $customerDAO = new Customer_DAO();
        $res = $customerDAO->LocationConfiguration($pais, $region, $ciudad, $idUser);
        return json_encode($res);
    }

    public function LoadCustomerByUser($idUser) {
        $customerDAO = new Customer_DAO();
        $res = $customerDAO->loadCustomerByUser($idUser);
        return json_encode($res);
    }

    public function cambiarLugar($data){
        $customerDAO = new Customer_DAO();
        $res = $customerDAO->cambiarLugar($data);
    }


}
