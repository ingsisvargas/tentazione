<?php

include_once '../dao/Provider_DAO.php';
include_once '../dao/Resource_DAO.php';

/**
 * Description of MngProvider
 * Clase destinada a la gestión lógica del modelo de negocio respecto a Proveedores.
 * @author Ronald Vargas
 */
class MngProvider {

    function __construct() {
        
    }

    /**
     * Aplica la acción de registrar un proveedor
     * @param type $data -> información para el registro
     * @return $result -> array con el resultado de la operación codificado en JSON
     */
    public function AddProvider($data) {
        $providerDAO = new Provider_DAO();
        $result = $providerDAO->AddProvider($data);
        return json_encode($result);
    }

    /**
     * Valida el nickName para saber si ya ha sido creado o no
     * @param $nick_name
     * @return mixed
     */
    public function validateNickname($nick_name){
        $providerDAO = new Provider_DAO();
        $result = $providerDAO->validateNickname($nick_name);
        return $result;
    }

    /**
     * Aplica la acción de Actualizar un Proveedor
     * @param type $data -> información para el registro
     * @return $result -> array con el resultado de la operación codificado en JSON
     */
    public function UpdateProvider($data) {
        $providerDAO = new Provider_DAO();
        $result = $providerDAO->UpdateProvider($data);
        return json_encode($result);
    }

    public function UpdateProviderAdmin($data) {
        $providerDAO = new Provider_DAO();
        $result = $providerDAO->UpdateProviderAdmin($data);
        return json_encode($result);
    }

    public function HabilProviderAdmin($data) {
        $providerDAO = new Provider_DAO();
        $result = $providerDAO->HabiProviderAdmin($data);
        return json_encode($result);
    }

    public function AceptProvider($id) {
        $providerDAO = new Provider_DAO();
        $result = $providerDAO->AceptProvider($id);
        return json_encode($result);
    }

    /**
     * Aplica la acción de Eliminar un Proveedor
     * @param type $id -> identificador del cliente a eliminar
     * @return $result -> array con el resultado de la operación codificado en JSON
     */
    public function DeleteProvider($id) {
        $providerDAO = new Provider_DAO();
        $result = $providerDAO->DeleteProvider($id);
        return json_encode($result);
    }

    /**
     * Permite la carga de todos los proveedores registrados en el sistema
     * @return $result -> array con el resultado de la operación codificado en JSON
     */
    public function LoadProviders() {
        $providerDAO = new Provider_DAO();
        $result = $providerDAO->LoadProviders();
        return json_encode($result);
    }

    public function LoadProvidersYear($year) {
        $providerDAO = new Provider_DAO();
        $result = $providerDAO->LoadProvidersYear($year);
        return json_encode($result);
    }

    /**
     * Permite la carga de un proveedor registrado en el sistema a partir de su id
     * @param type $id -> Identificador del proveedor a cargar.
     * @return $result -> array con el resultado de la operación codificado en JSON
     */
    public function LoadProvider($id) {
        $providerDAO = new Provider_DAO();
        $result = $providerDAO->LoadProvider($id);
        return json_encode($result);
    }

    /**
     * Permite cargar la información personal del usuario en sesion.
     */
    public function LoadPersonalInfo($idUser) {

        $providerDAO = new Provider_DAO();
        $arrayPersonal = $providerDAO->LoadPersonalInfo($idUser);
        $resorceDAO = new Resource_DAO();
        $arrayPersonal['IMAGENES'] = $resorceDAO->loadResources($idUser);


        return json_encode($arrayPersonal);
    }

    public function LoadPerfil($user) {
        $providerDAO = new Provider_DAO();
        $arrayPersonal = $providerDAO->LoadPerfil($user);
        $resorceDAO = new Resource_DAO();
        $arrayPersonal['IMAGENES'] = $resorceDAO->loadResources($arrayPersonal['id_usuario']);
        return json_encode($arrayPersonal);
    }

    /**
     * Carga los usuarios ordenados de acorde al mas ranqueado
     * @return type
     */
    public function loadProviderHome() {
        $providerDAO = new Provider_DAO();
        $data = $providerDAO->LoadProviders();
        $dataProviders = $data['DATA'];
        $resourceDAO = new Resource_DAO();
        $cantidadProviders = count($dataProviders);
        
        for ($j = 0; $j < $cantidadProviders; $j++) {
            $dataProviders[$j]['IMAGENES'] = $resourceDAO->loadResources($dataProviders[$j]['id_usuario']);
        }

        $dataReturn = array();
        $id = "";


        for ($i = 0; $i < 6; $i++) {
            $val = rand(0, $cantidadProviders);
            if (strpos($id, $val) != true) {

                if (isset($dataProviders[$i])) {
                    $dataReturn['DATA'][] = $dataProviders[$i];
                    $id.="-" . $val;
                }
            }
        }



        return json_encode($dataReturn);
    }

    public function LocationConfiguration($pais, $region, $ciudad, $idUser) {

        $providerDao = new Provider_DAO();
        $res = $providerDao->LocationConfiguration($pais, $region, $ciudad, $idUser);
        return json_encode($res);
    }

}
