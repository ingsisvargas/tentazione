<?php

include_once '../dao/Calendar_DAO.php';

/**
 * Description of MngServices
 * Clase destinada a la gestión lógica del modelo de negocio, respecto a Servicios.
 * @author Ronald Vargas
 */
class MngCalendar {
    
    function __construct() {
        
    }
    
    /**
     * Permite la carga de todos los Servicios registrados en el sistema a la fecha
     * @return $result -> array con el resultado de la operación codificado en JSON
     */
    public function LoadServices() {
        $calendarDAO = new Calendar_DAO();
        $result = $calendarDAO->LoadServices();
        return json_encode($result);
    }

    
     /**
     * Permite la carga de un servicio registrado en el sistema a partir de su id
      * @param type $id -> Identificador del servicio a cargar.
     * @return $result -> array con el resultado de la operación codificado en JSON
     */
    public function LoadService($id) {
        $calendarDAO = new Calendar_DAO();
        $result = $servicesDAO->LoadService($id);
        return json_encode($result);
    }
    
      /**
     * Permite la carga de un servicio registrado en el sistema a partir de su id
      * @param type $id -> Identificador del servicio a cargar.
     * @return $result -> array con el resultado de la operación codificado en JSON
     */
    public function moveFile($file) {
        $calendarDAO = new Calendar_DAO();
        $result = $servicesDAO->LoadService($id);
        return json_encode($result);
    }

    
}
