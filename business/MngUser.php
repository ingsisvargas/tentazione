<?php

include_once '../dao/User_DAO.php';

/**
 * Description of MngUser
 * Clase destinada a la gestión lógica del modelo de negocio respecto a Usuarios
 * @author Ronald Vargas
 */
class MngUser {

    function __construct() {
        
    }

    /**
     * Permmite cargar las imagenes de perfil de dos usuarios para un chat.
     * @param type $imageUser
     * @param type $user_need
     */
    function loadImagesPerfil($imageUser, $user_need) {
        $userDAO = new User_DAO();
        $result = $userDAO->loadImagesPerfil($user_need);
        $data = array();
        if ($result['STATUS'] == 'OK' && $result['DATA'][0]['url_foto_perfil']!= null) {
            $data ['img_user'] = $imageUser;
            $data['img_user_need'] = $result['DATA'][0]['url_foto_perfil'];
            $data['STATUS'] = 'OK';
        }
        
        return json_encode($data);
    }



    
    /**
     * Permite aplicar la acción de LogIn para un usuario en el Sistema.
     * @param type $nick -> nombre de usuario
     * @param type $pass -> password del usuario.
     * @return $result -> array codificado en formato JSON con la información del
     * usuario logueado.
     */
    public function LogIn($nick, $pass) {
        $userDAO = new User_DAO();
        $result = $userDAO->logIn($nick, $pass);
        if ($result['STATUS'] == 'OK' && $result['DATA'][0]['idtnt_usuario'] != null) {
            session_start();
            $_SESSION['USER'] = $nick;
            $_SESSION['DATA'] = $result['DATA'][0];
        }
        return json_encode($result);
    }

    /**
     * Permite modificar la imagen de perfil de un usuario en especifico
     * @param $urlNewImage
     * @param $idUser
     * @return string
     */
    public function changePerfilImage($urlNewImage, $idUser) {
        $userDAO = new User_DAO();
        $result = $userDAO->updatePerfilImage($urlNewImage, $idUser);
        return json_encode($result);
    }

    /**
     * Valida el nickName para saber si ya ha sido creado o no
     * @param $nick_name
     * @return mixed
     */
    public function validateNickname($nick_name) {
        $userDAO = new User_DAO();
        $resultado = $userDAO->validateNickname($nick_name);
        return $resultado;
    }

    /**
     * Este metodo va y registra en la base de datos el nuevo usuario
     * @param $nick
     * @param $contraseña
     * @return mixed
     */
    public function createUser($nick, $contraseña) {
        $userDAO = new User_DAO();
        $resultado = $userDAO->createUser($nick, $contraseña);
        return json_decode($resultado);
    }

}
