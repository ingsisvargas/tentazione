<?php

include_once '../dao/MembershipDAO.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MngMembership
 *
 * @author roan0
 */
class MngMembership {

    function __construct() {
        
    }

    public function updateMebmership($idMembership) {
        $memberShipDAO = new MembershipDAO();
        $data = $memberShipDAO->updateMembership($idMembership);
        return json_encode($data);
    }

    public function validateStatusChatPay($idService) {
        $memberShipDAO = new MembershipDAO();
        $data = $memberShipDAO->validateStatusChatPay($idService);
        if ($data['STATUS'] == 'OK') {
            if ($data['DATA'][0]['cantidad'] > 0) {
                $result['STATUSPAY'] = 'OK';
            } else {
                $result['STATUSPAY'] = 'NO';
            }
        } else
            $result['STATUSPAY'] = 'NO';

        return json_encode($result);
    }

    public function validateMemberShip($id_user) {
        $membershipDAO = new MembershipDAO();
        $data = $membershipDAO->validateMemberShip($id_user);
        $result = array();
        if ($data['STATUS'] == 'OK') {
            if ($data['DATA'][0]['cantidad'] > 0) {
                $result['MEMBER'] = 'VALIDATE';
            } else {
                $result['MEMBER'] = 'INVALIDATE';
            }
        } else
            $result['MEMBER'] = 'INVALIDATE';

        return json_encode($result);
    }

    //put your code here
}
