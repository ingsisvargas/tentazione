<?php
session_start();

if (!isset($_SESSION['USER'])) {
    ?>
    <script type="text/javascript">
        alert('DEBE INICIAR SESION..');
        window.location = "index.php";
    </script>
    <?php
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8"/>
        <title>Tentazione</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <!--Estilos CSS-->
        <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500">

        <!-- fontawaesome -->
        <script src="https://use.fontawesome.com/835980aa1c.js"></script>

        <!-- Bootstrap core CSS -->
        <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!-- Font Awesome -->
        <link href="css/font-awesome.min.css" rel="stylesheet">

        <!-- Pace -->
        <link href="css/pace.css" rel="stylesheet">
        <link href="css/jquery.dataTables_themeroller.css" rel="stylesheet">
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

        <!-- Color box -->
        <link href="css/colorbox/colorbox.css" rel="stylesheet">

        <!-- Morris -->
        <link href="css/morris.css" rel="stylesheet"/>	

        <!-- Perfect -->
        <link href="css/app.min.css" rel="stylesheet">
        <link href="css/app-skin.css" rel="stylesheet">

        <!-- Le javascript
         ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->

        <!-- Jquery -->
        <script src="js/jquery-1.10.2.min.js"></script>

        <!-- Bootstrap -->
        <script src="bootstrap/js/bootstrap.min.js"></script>

        <!-- Datatable -->
        <script src='js/jquery.dataTables.min.js'></script> 

        <!-- Modernizr -->
        <script src='js/modernizr.min.js'></script>

        <!-- Pace -->
        <script src='js/pace.min.js'></script>

        <!-- Popup Overlay -->
        <script src='js/jquery.popupoverlay.min.js'></script>

        <!-- Slimscroll -->
        <script src='js/jquery.slimscroll.min.js'></script>

        <!-- Cookie -->
        <script src='js/jquery.cookie.min.js'></script>

        <!-- Perfect -->
        <script src="js/app/app.js"></script>
        <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>


        <!-- Styles -->
        <link href="css/jquery.filer.css" rel="stylesheet">
        <link href="css/themes/jquery.filer-dragdropbox-theme.css" rel="stylesheet">

        <!-- Jvascript -->        
        <script src="js/jquery.filer.min.js" type="text/javascript"></script>

        <script src="http://momentjs.com/downloads/moment-with-locales.js"></script>

        <!-- libs to PubNub-->
        <script src="https://cdn.pubnub.com/pubnub.min.js"></script>
        <script src="js/webrtc.js"></script>
        <script src="js/modalEffects.js"></script>

        <!--videCallManagement-->
        <script src='js/VideoCallManagement.js'></script>
        <script src='js/chat/socketManagement.js'></script>

        <!-- Include Date Range Picker -->
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>

        <!-- Incluir el timePicker-->



        <link href="css/timedropper.css" rel="stylesheet">

        <script src="js/timedropper.js"></script>

        <!--Libreria de ubicación google-->
        
         <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCSMxQ62XqiSmhe5vr9XoYKKUMk2IMkOjw&libraries=places&callback=initAutocomplete"
                    async defer></script>

        <script src="https://www.gstatic.com/firebasejs/3.5.2/firebase.js"></script>
        <script src="js/connect.js"></script>
        <script src="js/user.js"></script>
    </head>

    <body class="overflow-hidden">
        <!-- Overlay Div -->
        <div id="overlay" class="transparent"></div>
        <!-- /theme-setting -->
        <div id="wrapper" class="preload">
            <div id="top-nav" class="fixed skin-2">
                <a href="#" class="brand">
                           <img src="img/tentazione2.png" style="height:40px; width:180px;"></img>

                </a><!-- /brand -->					
                <button type="button" class="navbar-toggle pull-left" id="sidebarToggle">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <button type="button" class="navbar-toggle pull-left hide-menu" id="menuToggle">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <ul class="nav-notification clearfix">

                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#" onclick="$('#notificationIcon').html('')">
                            <i class="fa fa-envelope fa-lg"></i>
                            <div id="notificationIcon"></div>                           
                        </a>
                        <ul class="dropdown-menu message dropdown-1" id="notification">     
                        </ul>                  
                    </li>
                    <li class="profile dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <strong>Usuario</strong>
                            <span><i class="fa fa-chevron-down"></i></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li>
                                <a class="clearfix" href="#">
                                    <img src="img/user.jpg" alt="User Avatar">
                                    <div class="detail">
                                        <strong><?php echo $_SESSION['USER'] ?></strong>
                                        <p class="grey">On line</p> 
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li><a tabindex="-1" class="main-link logoutConfirm_open" href="#logoutConfirm"><i class="fa fa-lock fa-lg"></i> Log out</a></li>
                        </ul>
                    </li>
                </ul>
            </div><!-- /top-nav-->

            <aside class="fixed skin-1">
                <div class="sidebar-inner scrollable-sidebar">
                    <div class="size-toggle">
                        <a class="btn btn-sm" id="sizeToggle">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </a>
                        <a class="btn btn-sm pull-right logoutConfirm_open"  href="#logoutConfirm">
                            <i class="fa fa-power-off"></i>
                        </a>
                    </div><!-- /size-toggle -->	
                    <div class="user-block clearfix">

                        <div class="detail">
                            <strong> <?php echo $_SESSION['USER']; ?></strong>

                        </div>
                    </div><!-- /user-block -->

                    <div class="main-menu">
                        <ul>
                            <li >
                                <a href="#" onclick="javascript:loadPerfil()">
                                    <span class="menu-icon">
                                        <i class="glyphicon glyphicon-user"></i> 
                                    </span>
                                    <span class="text">
                                        Perfil
                                    </span>
                                    <span class="menu-hover"></span>
                                </a>
                            </li>
                            <li >
                                <a href="#" onclick="javascript:loadServices()">
                                    <span class="menu-icon">
                                        <i class="glyphicon glyphicon-glass"></i> 
                                    </span>
                                    <span class="text">
                                        Servicios
                                    </span>
                                    <span class="menu-hover"></span>
                                </a>
                            </li>
                            <li >
                                <a href="#" onclick="javascript:loadScore()">
                                    <span class="menu-icon">
                                        <i class="glyphicon glyphicon-ok"></i> 
                                    </span>
                                    <span class="text">
                                        Calificación
                                    </span>

                                    <span class="menu-hover"></span>
                                </a>

                            </li>

                    </div><!-- /main-menu -->
                </div><!-- /sidebar-inner -->
            </aside>

            <div id="main-container">
                <div id='tittleLoad'class="panel-heading">
                    <h3>Mi Perfil</h3>
                </div>

                <div id='load'  class="padding-md clearfix">
                </div>
             </div><!-- /main-container -->
                <!-- Footer
                ================================================== -->
                <footer>
                    <div class="row">
                     
                        <div class="col-sm-6">
                             
                            <span class="footer-brand">
                              
                            </span>
                            <p class="no-margin">
                                &copy; 2016 <strong>tentazione</strong>. ALL Rights Reserved. 
                            </p>
                        </div><!-- /.col -->
                    </div><!-- /.row-->
                </footer>
            
            </div>
                <a href="" id="scroll-to-top" class="hidden-print"><i class="fa fa-chevron-up"></i></a>
                <!-- Logout confirmation -->
                <div class="custom-popup width-100" id="logoutConfirm">
                    <div class="padding-md">
                        <h4 class="m-top-none">¿Desea cerrar sesion?</h4>
                    </div>
                    <div class="text-center">
                        <a class="btn btn-success m-right-sm" href="#" onclick="javascript:logOut()">Salir</a>
                        <a class="btn btn-danger logoutConfirm_close">Cancelar</a>
                    </div>
                </div>
            </div>       
            <script type="text/javascript">
                connect = new Connect(firebase);
                user = new User("<?php echo $_SESSION['DATA']['nick'] ?>", "jeffersson.sinza@gmail.com", "<?php echo $_SESSION['DATA']['password'] ?>", "<?php echo $_SESSION['DATA']['url_foto_perfil'] ?>", "<?php echo $_SESSION['DATA']['tipo'] ?>");

                connect.ref('rooms').orderByKey().on('value', searchNotification);

                function searchNotification(snapshot)
                {
                    snapshot.forEach(function (data) {
                        var room = data.val();
                        var provider = room.provider;
                        var customer = room.customer;
                        console.log(room);
                        var idLi = 'notiService' + data.key;
                        var state = room.stateService;
                        var notification = "<li id =\'" + idLi + "'>" +
                                '<a class="clearfix" onclick="loadChat(\'' + provider.nickname + '\',' + data.key + ',\'' + state + '\')">' +
                                "<img src='img/user.jpg' alt='User Avatar'>" +
                                "<div class='detail'>" +
                                "<strong>" + provider.nickname + "</strong>" +
                                "<p class='no-margin'>Tienes un nuevo mensaje</p>" +
                                "</div>" +
                                "</a>" +
                                "</li>";
                        if (room !== null) {
                            if ($("#" + idLi).length == 0) {
                                if (user.nickname === customer.nickname && room.messageCustomer) {

                                    $('#notification').append(notification);
                                    $('#notificationIcon').append('<span class="notification-label bounceIn animation-delay4" >*</span>');
                                }
                            }
                        }
                    });

                }
                function loadChat(chat_user, aspirante, estado) {
                    $('#notiService' + aspirante).remove();
                    $('#tittleLoad').html('<h3>Servicios</h3>');
                    $("#load").load("templates/chat.php?nick=" + chat_user + "&aspirante=" + aspirante + "&estado=" + estado);
                }

                function loginOutUserConnected() {
                    var refConnected = connect.ref().child("connected");
                    var idUser = refConnected.orderByChild("name").equalTo(user.nickname).key;
                    refConnected.remove(idUser);
                }
                $(document).ready(function () {
                    $("#load").load("templates/perfilCustomer.php");
                    $('#tittleLoad').html('<h3>Mi Perfil</h3>');
                });

                function loadPerfil() {
                    if (phone != null) {
                        phone.hangup();
                        if (phone.mystream != null) {
                            phone.mystream.getVideoTracks()[0].stop();
                        }
                        if (phone.mystream != null) {
                            phone.mystream.getAudioTracks()[0].stop();
                        }
                        phone.pubnub.unsubscribe({
                            channel: "<?php echo $_SESSION['USER']; ?>"
                        });
                        phone = null;
                    }
                    if (websocket != null) {
                        websocket.close();
                        websocket = null;
                    }

                    $("#load").load("templates/perfilCustomer.php");
                    $('#tittleLoad').html('<h3>Mi Perfil</h3>');
                }

                function loadServices() {
                    if (websocket != null) {
                        websocket.close();
                        websocket = null;
                    }
                    if (phone != null) {
                        phone.hangup();
                        if (phone.mystream != null) {
                            phone.mystream.getVideoTracks()[0].stop();
                        }
                        if (phone.mystream != null) {
                            phone.mystream.getAudioTracks()[0].stop();
                        }
                        phone.pubnub.unsubscribe({
                            channel: "<?php echo $_SESSION['USER']; ?>"
                        });
                        phone = null;
                    }
                    $("#load").load("templates/tramitados.php");
                    $('#tittleLoad').html('<h3>Servicios</h3>');

                }

                function loadScore() {
                    if (websocket != null) {
                        websocket.close();
                        websocket = null;
                    }
                    if (phone != null) {
                        phone.hangup();
                        if (phone.mystream != null) {
                            phone.mystream.getVideoTracks()[0].stop();
                        }
                        if (phone.mystream != null) {
                            phone.mystream.getAudioTracks()[0].stop();
                        }
                        phone.pubnub.unsubscribe({
                            channel: "<?php echo $_SESSION['USER']; ?>"
                        });
                        phone = null;
                    }
                    $("#load").load("templates/Calificacion.php");
                    $('#tittleLoad').html('<h3>Calificación</h3>');

                }
                function logOut() {
                    loginOutUserConnected();
                    $.post("Services/logOut.php",
                            function () {
                                window.location = "index.php";
                            });
                }


               
            </script>
    </body>
</html>
