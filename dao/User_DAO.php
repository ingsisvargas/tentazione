<?php

include '../common/Database.php';

/**
 * Description of User_DAO
 * Clase destinada para la gestión de datos de usuarios.
 * 
 * @author Ronald Vargas
 */
class User_DAO {

    function __construct() {
        
    }

    public function loadImagesPerfil($user_need) {
         $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getInstance();
        }
        
        $sqlLoad = "SELECT url_foto_perfil FROM tnt_usuario WHERE nick='$user_need'";
        $result = $instance -> get_data($sqlLoad);
        return $result;
    }

    /**
     * Permite hacer LogIn de un usuario en el sistema.
     * @param type $nick -> nick del usario que ingresa 
     * @param type $pass -> password del usuario que ingresa
     * @return 
     */
    public function logIn($nick, $pass) {
        $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getInstance();
        }

        $sqlLogIn = "SELECT * FROM tnt_usuario WHERE nick='$nick' AND password='$pass'";
        $result = array();
        $res = $instance->get_data($sqlLogIn);
        if ($res['STATUS'] == 'OK' && $res['DATA'][0]['idtnt_usuario'] != null) {
            $result['DATA'] = $res['DATA'];
            $result['STATUS'] = 'OK';
        } else {
            $result['STATUS'] = 'ERROR';
        }
        return $result;
    }

    /**
     * Actualiza la imagen de perfil de un usuario.
     * @param type $urlNewImage nueva url de imagen
     * @param type $idUser id del usuario a modificar.
     * @return type
     */
    public function updatePerfilImage($urlNewImage, $idUser) {
        $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getInstance();
        }

        $sql = "UPDATE tnt_usuario SET url_foto_perfil = '$urlNewImage' WHERE "
                . " idtnt_usuario = $idUser";
        $res = $instance->exec($sql);
        return $res;
    }

    /*
     * Este metodo recibe el nick que desea ser validado en la base de datos
     * hace una consulta de si existe o no el nick en la base de datos
     * retorna true cuando ya existe y false cuando no
     */

    public function validateNickname($nick_name) {
        $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getInstance();
        }
        $sql = "SELECT * FROM tnt_usuario WHERE tnt_usuario.nick = '$nick_name'";
        $res = $instance->get_data($sql);
        if ($res['STATUS'] == 'OK' && $res['DATA'][0]['idtnt_usuario'] != null) {
            $result['STATUS'] = 'OK';
        } else {
            $result['STATUS'] = 'ERROR';
        }
        return $result;

        return $res;
    }

    public function validarCodigoReferido($codigo) {
        $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getInstance();
        }
        $sql = "SELECT * FROM tnt_usuario usu WHERE  usu.nick = $nick_name";
        $res = $instance->exec($sql);
        if ($res == null) {
            return true;
        }
        return $res;
    }

    /*
     * Este metodo crea un usuario con los datos necesarios para el registro
     */

    public function createUser($nick, $contraseña) {
        $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getInstance();
        }
        $sql = "INSERT INTO `tnt_usuario` (`idtnt_usuario`, `nick`, `password`) VALUES (NULL, $nick,$contraseña')";
        $res = $instance->exec($sql);
        return $res;
    }

}
