<?php

include_once '../common/Database.php';

/**
 * Description of Services_DAO
 * Clase destinada para la gestión de datos de Servicio
 *
 * @author Ronald Vargas
 */
class Services_DAO {

    function __construct() {
        
    }

    /**
     * Permite obtener la duracion en minutos de un servicio
     * para gestión de calculos y liquidación.
     * @param type $id_service
     * @return type
     */
    public function loadDurationMinutes($id_service) {
        $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getInstance();
        }

        $sqlLoad = "SELECT  TRUNCATE(TIME_TO_SEC(TIMEDIFF(hora_fin,hora_inicio))/60,0) AS minutes, categoria FROM tnt_servicio WHERE idtnt_servicio = $id_service";

       
        $res = $instance->get_data($sqlLoad);

        return $res['DATA'][0];
    }

    /**
     * Registra el Proveedor que ul cliente confirma como quien atendera el 
     * servicio
     * @param type $nick
     * @param type $id_service
     * @return type
     */
    public function confirmService($nick, $id_service) {

        $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getInstance();
        }

        $sql = "UPDATE tnt_servicio SET estado='AT', "
                . "id_proveedor = "
                . "     (SELECT idtnt_proveedor "
                . "         FROM tnt_proveedor prov, tnt_usuario usr "
                . "         WHERE prov.id_usuario = usr.idtnt_usuario AND usr.nick ='$nick'"
                . "      ) "
                . "WHERE idtnt_servicio = $id_service";

        $res = $instance->exec($sql);

        return $res;
    }

    /**
     * Permite cargar la información de todos los servicios registrados en el sistema hasta la fecha actual.
     * @return $result -> array asociativo con el resultado de la transacción y la data en caso de ser exitosa.
     */
    public function LoadServices() {
        $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getInstance();
        }

        $sqlLoad = "SELECT ser.*,"
                . "  CONCAT(cli.nombres, ' ', cli.apellidos) as cliente,"
                . "  CONCAT(prov.nombres, ' ',prov.apellidos) as proveedor"
                . "  FROM tnt_servicio ser, tnt_proveedor prov, tnt_cliente cli WHERE ser.fecha_servicio <= now()"
                . "  AND cli.idtnt_cliente = ser.id_cliente "
                . "  AND prov.idtnt_proveedor = ser.id_proveedor ORDER BY fecha_servicio DESC";

        $result = array();
        $res = $instance->get_data($sqlLoad);
        if ($res['STATUS'] == 'OK') {
            $result['DATA'] = $res['DATA'];
            $result['STATUS'] = 'OK';
        } else {
            $result['STATUS'] = 'ERROR';
        }
        return $result;
    }

    public function LoadService2($id) {
        $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getInstance();
        }

        $sqlLoad = "SELECT ser.*,"
                . "  CONCAT(cli.nombres, ' ', cli.apellidos) cliente "
                . "  FROM tnt_servicio ser, tnt_cliente cli WHERE ser.idtnt_servicio = $id"
                . "  AND cli.idtnt_cliente = ser.id_cliente ";


        $result = array();
        $res = $instance->get_data($sqlLoad);
        if ($res['STATUS'] == 'OK') {
            $result['DATA'] = $res['DATA'][0];
            /* $sqlLoadDetail = "SELECT * FROM tnt_detalles WHERE id_servicio =" . $id;
              $resultDetail = $instance->get_data($sqlLoadDetail);
              if ($resultDetail['STATUS'] == 'OK') {
              $result['DATA']['DETALLES'] = $resultDetail['DATA'];
              } */
            $result['STATUS'] = 'OK';
        } else {
            $result['STATUS'] = 'ERROR';
        }
        return $result;
    }

    /**
     * Permite cargar la información d eun servicio en especifico a partir de su id, incluyendo los detalles
     * asociados al servicio.
     * @param type $id -> id del servicio a cargar
     * @return $result -> array asociativo que contiene el resultado de la transacción y la data en caso 
     * de ser exitosa.
     */
    public function LoadService($id) {
        $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getInstance();
        }

        $sqlLoad = "SELECT ser.*,"
                . "  CONCAT(cli.nombres, ' ', cli.apellidos) cliente,"
                . "  CONCAT(prov.nombres, ' ',prov.apellidos) proveedor"
                . "  FROM tnt_servicio ser, tnt_proveedor prov, tnt_cliente cli WHERE ser.idtnt_servicio = $id"
                . "  AND cli.idtnt_cliente = ser.id_cliente "
                . "  AND (prov.idtnt_proveedor = ser.id_proveedor OR ser.id_proveedor IS NULL)";
        $result = array();
        $res = $instance->get_data($sqlLoad);
        if ($res['STATUS'] == 'OK') {
            $result['DATA'] = $res['DATA'][0];
            $sqlLoadDetail = "SELECT * FROM tnt_detalles WHERE id_servicio =" . $id;
            $resultDetail = $instance->get_data($sqlLoadDetail);
            if ($resultDetail['STATUS'] == 'OK') {
                $result['DATA']['DETALLES'] = $resultDetail['DATA'];
            }
            $result['STATUS'] = 'OK';
        } else {
            $result['STATUS'] = 'ERROR';
        }
        return $result;
    }

    /**
     * Permite registrar una solicitud de servicio en el sistema.
     * @param type $idCustomer
     * @param type $fecha_servicio
     * @param type $hora_fin
     * @param type $hora_inicio
     * @param type $descripcion
     * @param type $plan
     * @param type $categoria
     */
    public function registerService($idCustomer, $fecha_servicio, $hora_fin, $hora_inicio, $descripcion, $plan, $categoria) {

        $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getInstance();
        }


        $sql = "INSERT INTO  tnt_servicio (fecha_servicio,estado,id_cliente,total,hora_inicio,hora_fin,descripcion,plan,categoria) "
                . "VALUES ('$fecha_servicio','AB',$idCustomer,0,'$hora_inicio','$hora_fin','$descripcion','$plan','$categoria')";



        $res = $instance->exec($sql);
        return $res;
    }

    public function loadServicesCalification($idUser, $type) {

        $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getInstance();
        } $result;
        $sql;
        if ($type) {
            $sql = "SELECT ser.*, "
                    . "CONCAT(prov.nombres, ' ',prov.apellidos) asociado  "
                    . "FROM tnt_servicio ser, tnt_proveedor prov, tnt_cliente cli  "
                    . "WHERE ser.id_proveedor = prov.idtnt_proveedor AND  "
                    . "ser.id_cliente =cli.idtnt_cliente AND "
                    . "cli.idtnt_cliente= $idUser AND "
                    . "ser.id_proveedor  NOT IN "
                    . "(SELECT id_proveedor FROM  tnt_calificacion cal  WHERE  ser.idtnt_servicio = cal.id_servicio AND prov.idtnt_proveedor = cal.id_proveedor)";
        } else {
            $sql = "SELECT ser.*, "
                    . "CONCAT(cli.nombres, ' ', cli.apellidos) asociado  "
                    . "FROM tnt_servicio ser, tnt_proveedor prov, tnt_cliente cli "
                    . "WHERE ser.id_proveedor = prov.idtnt_proveedor  AND "
                    . "ser.id_cliente =cli.idtnt_cliente AND "
                    . "prov.idtnt_proveedor = $idUser  AND "
                    . "ser.id_cliente = cli.idtnt_cliente AND "
                    . "ser.id_cliente  NOT IN "
                    . "(SELECT id_cliente FROM  tnt_calificacion cal WHERE ser.idtnt_servicio = cal.id_servicio AND cli.idtnt_cliente = cal.id_cliente)";
        }
        
       

        $result = $instance->get_data($sql);
        return $result;
    }

    public function GetServices($service, $type, $state) {
        $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getInstance();
        }
        $sql = "select  ser.* ,CONCAT(cli.nombres, ' ', cli.apellidos) as cliente," .
                "CONCAT(prov.nombres, ' ',prov.apellidos) as proveedor" .
                " from tnt_servicio ser " .
                " left JOIN (tnt_proveedor prov) on (prov.idtnt_proveedor = ser.id_proveedor)" .
                " left JOIN (tnt_cliente cli) on (cli.idtnt_cliente = ser.id_cliente)"
                . "where 1=1  ";
        if ($type == 2) {
            $sql = $sql . " and  ser.id_cliente =" . $service['id_cliente'];
        } else {
            $sql = $sql . " and  ser.id_proveedor =" . $service['id_proveedor'];
        }
        if (!empty($state)) {
            $sql = $sql . " and ser.estado = '" . $state . "'";
        }

        $sql = $sql . " ORDER BY ser.fecha_servicio DESC";

        $result = $instance->get_data($sql);

        return $result;
    }

    public function loadServicesAvailables($id_provider) {
        $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getInstance();
        }

        $sql = "SELECT ser.* FROM tnt_servicio ser, tnt_proveedor prov, tnt_cliente cli "
                . "WHERE prov.idtnt_proveedor = $id_provider AND prov.categoria = ser.categoria AND cli.idtnt_cliente = ser.id_cliente "
                . "AND cli.ciudad = prov.ciudad AND cli.region = prov.region AND cli.pais = prov.pais "
                . "AND ser.estado='AB' AND prov.idtnt_proveedor  NOT IN "
                . "(SELECT id_proveedor FROM tnt_aspirantes WHERE id_servicio = ser.idtnt_servicio AND id_proveedor = $id_provider) "
                . "ORDER BY ser.fecha_registro DESC LIMIT 5 ";

        $result = $instance->get_data($sql);
        return $result;
    }

}
