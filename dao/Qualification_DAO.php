<?php

include_once '../common/Database.php';

/**
 * Description of Qualification_DAO
 *
 * @author jeffersson
 */
class Qualification_DAO {

    function __construct() {
        
    }

    public function loadPromedio($idUser, $type) {
        $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getInstance();
        }

        if ($type == 3) {

            $sqlLoad = "SELECT ROUND(AVG(puntuacion)) promedio FROM tnt_calificacion WHERE id_cliente=$idUser";
            $result = $instance->get_data($sqlLoad);
            return $result['DATA'][0]['promedio'];
        } else {

            $sqlLoad = "SELECT  ROUND(AVG(puntuacion)) promedio FROM tnt_calificacion WHERE id_proveedor=$idUser";
            $result = $instance->get_data($sqlLoad);
            return $result['DATA'][0]['promedio'];
        }
    }

    public function addQualification($qualification) {
        $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getInstance();
        }
        $sql = "INSERT INTO tnt_calificacion(id_cliente, id_proveedor,puntuacion,descripcion,id_servicio) "
                . "VALUES (" . $qualification["id_cliente"] . ","
                . $qualification["id_proveedor"] . ","
                . $qualification["puntuacion"] . ","
                . "'" . $qualification["descripcion"] . "',"
                . $qualification["id_servicio"] . ")";

      
        $res = $instance->exec($sql);
        return $res;
    }

    public function validateQualification($qualification) {
        $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getConnection();
        }

        $sql = "SELECT idtnt_calificacion  FROM tnt_calificacion "
                . "WHERE"
                . " id_cliente      = " . $qualification["id_cliente"] . " and "
                . " id_proveedor    = " . $qualification["id_proveedor"] . " and "
                . " id_servicio     = " . $qualification["id_servicio"];
        $res = $instance->get_data($sql);
        if ($res['STATUS'] == 'OK') {
            return $res['DATA'][0];
        }
    }

}
