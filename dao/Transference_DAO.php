<?php

include_once '../common/Database.php';

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Transference_DAO
 *
 * @author roan0
 */
class Transference_DAO {

    function __construct() {
        
    }

    /**
     * Permite actualizar el estado de una transferencia
     * @param type $id_transference
     * @param type $estado
     */
    public function updateTransference($id_transference, $estado) {
        $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getInstance();
        }
        
        $sqlUpdate = "UPDATE tnt_transferencia SET estado ='$estado' WHERE idtnt_transferencia = $id_transference";
        $data = $instance->exec($sqlUpdate);
        return $data;
    }

    /**
     * Registra una transferencia en el sistema, en el momento en que inicia
     * con un estado PENDIENTE
     * @param type $estado
     * @param type $valor
     * @param type $idMembership
     * @param type $idService
     * @param type $tipo
     * @return type
     */
    public function addTransference($estado, $valor, $idMembership, $idService, $tipo) {

        $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getInstance();
        }

        if (isset($idService)) {
            $sqlInsert = "INSERT INTO tnt_transferencia (estado,valor,id_servicio,tipo) "
                    . " VALUES ('$estado',$valor,$idService,'$tipo')";
        } else {

            $sqlInsert = "INSERT INTO tnt_transferencia (estado,valor,id_membresia,tipo) "
                    . " VALUES ('$estado',$valor,$idMembership,'$tipo')";
        }



        $res = $instance->exec($sqlInsert);
        $id_transference = null;
        if ($res['STATUS'] == 'OK') {
            $id_transference = $instance->last_id('tnt_transferencia') - 1;
        }
        return $id_transference;
    }

    //put your code here
}
