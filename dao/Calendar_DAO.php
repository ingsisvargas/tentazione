<?php

include_once '../common/Database.php';


/**
 * Description of Services_DAO
 * Clase destinada para la gestión de datos de Servicio
 *
 * @author Ronald Vargas
 */
class Calendar_DAO {

    function __construct() {
        
    }

    
    /**
     * Permite cargar la información de todos los servicios registrados en el sistema hasta la fecha actual.
     * @return $result -> array asociativo con el resultado de la transacción y la data en caso de ser exitosa.
     */
    public function LoadServices() {
        $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getInstance();
        }

        $sqlLoad = "SELECT * FROM tnt_servicio WHERE fecha_servicio >= now() ORDER BY fecha_servicio DESC";
        $result = array();
        $res = $instance->get_data($sqlLoad);
        if ($res['STATUS'] == 'OK') {
            $result['DATA'] = $res['DATA'];
            $result['STATUS'] = 'OK';
        } else {
            $result['STATUS'] = 'ERROR';
        }
        return $result;
    }

    
    /**
     * Permite cargar la información d eun servicio en especifico a partir de su id, incluyendo los detalles
     * asociados al servicio.
     * @param type $id -> id del servicio a cargar
     * @return $result -> array asociativo que contiene el resultado de la transacción y la data en caso 
     * de ser exitosa.
     */
    public function LoadService($id) {
        $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getInstance();
        }

        $sqlLoad = "SELECT * FROM tnt_servicio WHERE tntid_servicio=".$id;
        $result = array();
        $res = $instance->get_data($sqlLoad);
        if ($res['STATUS'] == 'OK') {
            $result['DATA'] = $res['DATA'];
            $sqlLoadDetail = "SELECT * FROM tnt_detalles WHERE id_servicio =".$id;
            $resultDetail = $instance->get_data($sqlLoadDetail);
            if ($resultDetail['STATUS'] == 'OK') {
                $result['DATA']['DETALLES'] = $resultDetail['DATA'];
            }
            $result['STATUS'] = 'OK';
        } else {
            $result['STATUS'] = 'ERROR';
        }
        return $result;
    }

    
}
