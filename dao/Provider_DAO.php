<?php

include_once '../common/Database.php';

/**
 * Description of Provider_DAO
 * Clase destinada a la gestion de datos de Proveedores
 * 
 * @author Ronald Vargas
 */
class Provider_DAO {

    function __construct() {
        
    }

    public function updateQualification($idUser, $promedio, $category) {
        $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getInstance();
        }
        $sqlUpd = "UPDATE tnt_proveedor SET puntuacion=$promedio,  categoria='$category' WHERE idtnt_proveedor = $idUser";
        
        $instance->exec($sqlUpd);
    }

    /**
     * Inserta un nuevo Proveedor en la BD
     * @param type $data
     * @return boolean
     */
    public function AddProvider($data) {
        $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getInstance();
        }


        $sqlUser = "INSERT INTO tnt_usuario (nick,password,admin,tipo,aceptado) VALUES ('" . $data['nick'] . "','" . $data['password'] . "','" . 0 . "','" . 3 . "','" . 0 . "')";

        $res = $instance->exec($sqlUser);
        if ($res['STATUS'] == 'OK') {

            $last_id = $instance->last_id('tnt_usuario') - 1;

            $sqlCustomer = "INSERT INTO tnt_proveedor"
                    . " (nombres,apellidos,celular,email,fecha_nacimiento,categoria,puntuacion,id_usuario,pais,ciudad,region,sexo)"
                    . " values('" . $data['nombres']
                    . "','" . $data['apellidos']
                    . "','" . $data['celular']
                    . "','" . $data['email']
                    . "','" . $data['fecha_nacimiento']
                    . "','" . $data['categoria']
                    . "','" . $data['puntuacion']
                    . "','" . $last_id
<<<<<<< HEAD
                    . "','" .$data['pais']
                    . "','". $data['ciudad']
                    . "','". $data['region']
                    . "','". "f"
                    . "')";
=======
                    . "','" . $data['pais']
                    . "','" . $data['ciudad']
                    . "','" . $data['region'] . "')";
>>>>>>> adb4ac7b1478f22ae7633316c19f2936ab96809e

            $res = $instance->exec($sqlCustomer);
            print $sqlCustomer;
            if ($res['STATUS'] == 'OK') {
                $this->enviarSMS($data['celular']);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function enviarSMS($numero) {
        $numero = '57' . $numero;
        $nexmo_url = "https://rest.nexmo.com/sms/json?api_key=75834ba6&api_secret=ffd37caf611807dd&from=NEXMO&to=" . $numero . "&text=Bienvenido+a+tentazone";
        $nexmo_json = file_get_contents($nexmo_url);
        $nexmo_array = json_decode($nexmo_json, true);
        return $nexmo_array;
    }

    /**
     * Carga la información de todos los clientes en el sistema.
     * @return result array asociativo multidimensional con la información de 
     * todos los clientes 
     */
    public function LoadProviders() {
        $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getConnection();
        }
        $result = array();
        $sqlLoad = "SELECT pro.*,usr.aceptado FROM tnt_proveedor pro, tnt_usuario usr WHERE pro.id_usuario=usr.idtnt_usuario ORDER BY pro.puntuacion DESC";
        $res = $instance->get_data($sqlLoad);
        if ($res['STATUS'] == 'OK') {
            $result['DATA'] = $res['DATA'];
            $result['STATUS'] = 'OK';
        } else {
            $result['STATUS'] = 'ERROR';
        }
        return $result;
    }

    /**
     * Permite cargar la informacion para las estadisticas de un año para 
     * proveedores.
     * @param type $year
     * @return string
     */
    public function LoadProvidersYear($year) {
        $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getConnection();
        }
        $result = array();
        $sqlLoad = "SELECT pro.*,usr.aceptado FROM tnt_proveedor pro, tnt_usuario usr WHERE pro.id_usuario=usr.idtnt_usuario and pro.fecha_registro like '" . $year . "%'";
        $res = $instance->get_data($sqlLoad);
        if ($res['STATUS'] == 'OK') {
            $result['DATA'] = $res['DATA'];
            $result['STATUS'] = 'OK';
        } else {
            $result['STATUS'] = 'ERROR';
        }
        return $result;
    }

    /**
     * Carga la información de un cliente en el sistema
     * @param type $id
     * @return result array asociativo con la información del cliente consultado
     */
    public function LoadProvider($id) {

        $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getConnection();
        }
        $result = array();
        $sqlLoad = "SELECT prv.*,usr.nick, usr.aceptado, usr.idtnt_usuario,asp.idtnt_aspirantes aspirante FROM tnt_proveedor prv, tnt_usuario usr, tnt_aspirantes asp,tnt_servicio ser"
                . " where prv.idtnt_proveedor=asp.id_proveedor and ser.id_proveedor = asp.id_proveedor and asp.id_servicio=ser.idtnt_servicio and ser.idtnt_servicio=$id AND prv.id_usuario = usr.idtnt_usuario";
        $res = $instance->get_data($sqlLoad);
        if ($res['STATUS'] == 'OK') {
            $result['DATA'] = $res['DATA'];
            $result['STATUS'] = 'OK';
            $idUsuario = $result['DATA'][0]['idtnt_usuario'];
            $sqlLoadResources = "SELECT * FROM tnt_recursos where id_usuario = $idUsuario AND tipo <> 'FT' AND tipo <> 'VD'";
            $resResources = $instance->get_data($sqlLoadResources);
            if ($resResources['STATUS'] == 'OK') {
                $result['DATA']['RECURSOS'] = $resResources['DATA'];
            }
        } else {
            $result['STATUS'] = 'ERROR';
        }
        return $result;
    }

    /**
     * Permite actualizar la información de un cliente en la BD
     * @param type $data datos del cliente para la actualización
     * @return boolean resultado de la operación.
     */
    public function UpdateProvider($data) {

        $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getConnection();
        }
        $sqlUpdate = "UPDATE tnt_proveedor "
                . " SET nombres = '" . $data['nombres'] . "'"
                . " , apellidos = '" . $data['apellidos'] . "'"
                . " , celular = '" . $data['celular'] . "'"
                . " , email = '" . $data['email'] . "'"
                . " , url_foto_perfil = '" . $data['url_foto_perfil'] . "'"
                . " , categoria = '" . $data['categoria'] . "'"
                . " , puntuacion = " . $data['puntuacion']
                . " WHERE idtnt_proveedor =" . $data['id'];

        $res = $instance->exec($sqlUpdate);
        if ($res['STATUS'] == 'OK') {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Permite actualizar la informacion de un proveedro desde el panel 
     * administrativo.
     * @param type $data
     * @return type
     */
    public function UpdateProviderAdmin($data) {

        $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getConnection();
        }
        $sqlUpdate = "UPDATE tnt_proveedor "
                . " SET nombres = '" . $data['nombres'] . "'"
                . " , apellidos = '" . $data['apellidos'] . "'"
                . " , celular = '" . $data['celular'] . "'"
                . " , email = '" . $data['email'] . "'"
                . " , categoria = '" . $data['categoria'] . "'"
                . " , puntuacion = " . $data['puntuacion']
                . " WHERE idtnt_proveedor =" . $data['id'];

        $res = $instance->exec($sqlUpdate);
        return $res;
    }

    /**
     * Permite habilitar o deshabilitar un proveedor desde el panel 
     * administrativo
     * @param type $data
     * @return type
     */
    public function HabiProviderAdmin($data) {

        $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getConnection();
        }
        $sqlUpdate = "UPDATE tnt_usuario SET  aceptado = " . 1 . " WHERE idtnt_usuario =" . $data['id'] . "";

        $res = $instance->exec($sqlUpdate);
        return $res;
    }

    public function AceptProvider($id) {
        $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getConnection();
        }

        $sql = "UPDATE tnt_usuario usr, tnt_proveedor prov SET usr.aceptado=1 WHERE usr.idtnt_usuario = prov.id_usuario AND prov.idtnt_proveedor=$id";

        $res = $instance->exec($sql);

        return $res;
    }

    /**
     * Permite eliminar la información de un cliente en la BD
     * @param type $id id del cliente a eliminar
     * @return boolean resultado de la operación
     */
    public function DeleteProvider($id) {

        $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getConnection();
        }
        $sqlDelete = "DELETE FROM tnt_proveedor "
                . " WHERE idtnt_proveedor = $id";
        $res = $instance->exec($sqlDelete);
        return $res;
    }

    /**
     * Permite cargar la informacion personal de un proveedor a partir de su id de usuario
     * @param type $idUser
     * @return type
     */
    public function LoadPersonalInfo($idUser) {
        $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getConnection();
        }

        $sqlLoad = "SELECT nombres,apellidos,fecha_nacimiento, puntuacion, pais, region, ciudad "
                . "FROM tnt_proveedor where id_usuario =$idUser";

        $res = $instance->get_data($sqlLoad);
        return $res['DATA'][0];
    }

    public function LoadPerfil($user) {
        $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getConnection();
        }

        $sqlLoad = "SELECT prov.*, user.url_foto_perfil FROM tnt_proveedor prov, tnt_usuario user"
                . " WHERE prov.idtnt_proveedor = $user AND prov.id_usuario = user.idtnt_usuario";
        $res = $instance->get_data($sqlLoad);
        return $res['DATA'][0];
    }

    /**
     * Configura la ubicación de un proveedor
     * @param type $pais
     * @param type $region
     * @param type $ciudad
     * @param type $idUser
     * @return type
     */
    public function LocationConfiguration($pais, $region, $ciudad, $idUser) {
        $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getConnection();
        }

        $sql = "UPDATE tnt_proveeedor SET pais  = '$pais', region = '$region', ciudad = '$ciudad' WHERE id_usuario = $idUser";
        $res = $instance->exec($sql);
        return $res;
    }

    public function loadProviderByUser($idUser) {
        $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getConnection();
        }

        $sql = "SELECT idtnt_proveedor  FROM tnt_proveedor WHERE id_usuario = $idUser";
        $res = $instance->get_data($sql);
        if ($res['STATUS'] == 'OK') {
            return $res['DATA'][0];
        }
    }

    /*
     * Este metodo recibe el nick que desea ser validado en la base de datos
     * hace una consulta de si existe o no el nick en la base de datos
     * retorna true cuando ya existe y false cuando no
     */

    public function validateNickname($nick_name) {
        $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getInstance();
        }
        $sql = "SELECT * FROM tnt_proveedor WHERE tnt_proveedor.nick = '$nick_name'";
        $res = $instance->get_data($sql);
        if ($res['STATUS'] == 'OK' && $res['DATA'][0]['idtnt_usuario'] != null) {
            $result['STATUS'] = 'OK';
        } else {
            $result['STATUS'] = 'ERROR';
        }
        return $result;

        return $res;
    }

}
