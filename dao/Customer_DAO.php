<?php

include_once '../common/Database.php';

/**
 * Description of Customer_DAO
 * Clase destinada para la gestión de datos de Clientes
 *
 * @author Ronald Vargas
 */
class Customer_DAO {

    /**
     * Constructor de la clase DAO
     */
    function __construct() {
        
    }

    public function updateQualification($idUser, $promedio) {
        $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getInstance();
        }
        $sqlUpd = "UPDATE tnt_cliente SET puntuacion=$promedio WHERE idtnt_cliente = $idUser";
        $instance->exec($sqlUpd);
        
    }

    /**
     * Inserta un nuevo cliente en la BD
     * @param type $data
     * @return boolean
     */
    public function AddCustomer($data) {
        $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getInstance();
        }

        $sqlUser = "INSERT INTO tnt_usuario (nick,password,tipo,aceptado) VALUES ('" . $data['nick'] . "','" . $data['password'] . "'," . 2 . "," . $data['aceptado'] . ")";

        $res = $instance->exec($sqlUser);
        if ($res['STATUS'] == 'OK') {

            $last_id = $instance->last_id('tnt_usuario') - 1;

            $codigo_referir = $this->generarCodigo(6);

            $sqlCustomer = "INSERT INTO `tnt_cliente` (`nombres`, `apellidos`, `celular`, `email`, `fecha_nacimiento`, `fumador`,
                            `notificacion_inactividad`, `id_usuario`, `puntuacion`, `pais`, `ciudad`,`region`, `codigo_referido`, `codigo_referir`, `sexo`) 
                            VALUES (
                            '" . $data['nombres']
                        . "','" . $data['apellidos']
                        . "','" . $data['celular']
                        . "','" . $data['email']
                        . "','" . $data['fecha_nacimiento']
                        . "','" . $data['fumador']
                        . "','" . $data['notificacion_inactividad']
                        . "','" . $last_id
                        . "','" . $data['puntuacion']
                        . "','" . $data['pais']
                        . "','" . $data['ciudad']
                        . "','" . $data['region']
                        . "','" . $data['codigo_referido']
                        . "','" . $codigo_referir
                        . "','" .$data['sexo']
                        . "' )";

            $res = $instance->exec($sqlCustomer);
            print $sqlCustomer;
            if ($res['STATUS'] == 'OK') {
                if($this->validarCodigoReferido($data['codigo_referido'])){
                    $last_cliente = $instance->last_id('tnt_cliente') - 1;
                    $sqlCodigo = "INSERT INTO `tnt_membresia` (`fecha_inicio`, `id_plan`, `id_cliente`) VALUES (NOW(), 1, $last_cliente)";
                    $instance->exec($sqlCodigo);
                    $this->agregarDiasMembresia($last_cliente);
                }
                $this->enviarSMS($data['celular']);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function cambiarLugar($data){
        $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getInstance();
        }
        $pais = $data['pais'];
        $ciudad = $data['ciudad'];
        $region = $data['region'];
        $id_cliente=$data['id_cliente'];
        $sqlUser = "UPDATE `tnt_cliente` SET `pais` = '$pais', `region` = '$region', `ciudad` = '$ciudad' WHERE `tnt_cliente`.`idtnt_cliente` = $id_cliente";

        $res = $instance->exec($sqlUser);
    }

    private function agregarDiasMembresia($last_cliente){
        $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getConnection();
        }
        $sqlLoad = "UPDATE tnt_membresia SET fecha_fin = ADDDATE(fecha_inicio, INTERVAL 15 DAY) WHERE id_cliente = $last_cliente";
        $res = $instance->exec($sqlLoad);
    }

    private function validarCodigoReferido($codigoReferdio){
        $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getConnection();
        }
        $sqlLoad = "SELECT * FROM tnt_cliente cli WHERE cli.codigo_referir = '$codigoReferdio'";
        $res = $instance->get_data($sqlLoad);
        if ($res['STATUS'] == 'OK' && $res['DATA'][0]['idtnt_usuario'] != null) {
            return true;
        } else {
            return false;
        }
        return false;
    }

    public function enviarSMS($numero){
        $numero='57'.$numero;
        $nexmo_url = "https://rest.nexmo.com/sms/json?api_key=75834ba6&api_secret=ffd37caf611807dd&from=NEXMO&to=".$numero."&text=Bienvenido+a+tentazone";
        $nexmo_json = file_get_contents($nexmo_url);
        $nexmo_array = json_decode($nexmo_json,true);
        return $nexmo_array;
    }



    /**
     * Metodo que genera un codigo aleatorio para los usuarios
     * @param $longitud
     * @return string
     */
    public function generarCodigo($longitud) {
        $key = '';
        $pattern = '1234567890abcdefghijklmnopqrstuvwxyz';
        $max = strlen($pattern)-1;
        for($i=0;$i < $longitud;$i++) $key .= $pattern{mt_rand(0,$max)};
        return $key;
    }


    /**
     * Carga la información de todos los clientes en el sistema.
     * @return result array asociativo multidimensional con la información de 
     * todos los clientes 
     */
    public function LoadCustomers() {

        $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getConnection();
        }
        $result = array();
        $sqlLoad = "SELECT cli.*,usr.aceptado FROM tnt_cliente cli, tnt_usuario usr WHERE cli.id_usuario = usr.idtnt_usuario";
        $res = $instance->get_data($sqlLoad);
        if ($res['STATUS'] == 'OK') {
            $result['DATA'] = $res['DATA'];
            $result['STATUS'] = 'OK';
        } else {
            $result['STATUS'] = 'ERROR';
        }
        return $result;
    }

    /**
     * Carga la información de un cliente en el sistema
     * @param type $id
     * @return result array asociativo con la información del cliente consultado
     */
    public function LoadCustomer($id) {

        $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getConnection();
        }
        $result = array();
        $sqlLoad = "SELECT cli.*, usr.nick, usr.aceptado FROM tnt_cliente cli, tnt_usuario usr where idtnt_cliente=$id AND usr.idtnt_usuario = cli.id_usuario";
        $res = $instance->get_data($sqlLoad);
        if ($res['STATUS'] == 'OK') {
            $result['DATA'] = $res['DATA'];
            $result['STATUS'] = 'OK';
            $idUsuario = $result['DATA'][0]['id_usuario'];
            $sqlLoadResources = "SELECT * FROM tnt_recursos where id_usuario = $idUsuario AND tipo <> 'FT' AND tipo <> 'VD'";
            $resResources = $instance->get_data($sqlLoadResources);
            if ($resResources['STATUS'] == 'OK') {
                $result['DATA']['RECURSOS'] = $resResources['DATA'];
            }
        } else {
            $result['STATUS'] = 'ERROR';
        }
        return $result;
    }

    /**
     * Permite actualizar la información de un cliente en la BD
     * @param type $data datos del cliente para la actualización
     * @return boolean resultado de la operación.
     */
    public function UpdateCustomer($data) {

        $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getConnection();
        }
        $sqlUpdate = "UPDATE tnt_cliente "
                . " SET nombres = '" . $data['nombres'] . "'"
                . " , apellidos = '" . $data['apellidos'] . "'"
                . " , celular = '" . $data['celular'] . "'"
                . " , email = '" . $data['email'] . "'"
                . " , url_foto_perfil = '" . $data['url_foto_perfil'] . "'"
                . " , notificacion_inactividad = '" . $data['notificacion_inactividad'] . "'"
                . " , puntuacion = " . $data['puntuacion']
                . " , fumador  = " . $data['fumador']
                . " WHERE idtnt_cliente =" . $data['id'];

        echo $sqlUpdate;

        $res = $instance->exec($sqlUpdate);
        if ($res['STATUS'] == 'OK') {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Permite actualizar la información de un cliente desde el panel administra
     * tivo
     * @param type $data
     * @return type
     */
    public function UpdateCustomerAdmin($data) {

        $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getConnection();
        }
        $sqlUpdate = "UPDATE tnt_cliente "
                . "SET nombres = '" . $data['nombres'] . "'"
                . " , apellidos = '" . $data['apellidos'] . "'"
                . " , celular = '" . $data['celular'] . "'"
                . " , email = '" . $data['email'] . "'"
                . " , puntuacion = " . $data['puntuacion']
                . " , fumador  = " . $data['fumador']
                . " WHERE idtnt_cliente =" . $data['id'];


        $res = $instance->exec($sqlUpdate);

        return $res;
    }

    /**
     * Permite actualizar el estado de aprobacion para un cliente
     * @param type $id
     * @return type
     */
    public function AceptCustomer($id) {
        $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getConnection();
        }

        $sql = "UPDATE tnt_usuario usr, tnt_cliente cli SET usr.aceptado=1 WHERE usr.idtnt_usuario = cli.id_usuario AND cli.idtnt_cliente=$id";

        $res = $instance->exec($sql);

        return $res;
    }

    /**
     * Permite eliminar la información de un cliente en la BD
     * @param type $id id del cliente a eliminar
     * @return boolean resultado de la operación
     */
    public function DeleteCustomer($id) {

        $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getConnection();
        }
        $sqlDelete = "DELETE FROM tnt_cliente "
                . " WHERE idtnt_cliente = $id";
        $res = $instance->exec($sqlDelete);
        return $res;
    }

    /**
     * Permite cargar la informacion personal de un cliente a partir de su id de
     * usuario.
     * @param type $idUser
     * @return type
     */
    public function LoadPersonalInfo($idUser) {
        $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getConnection();
        }

        $sqlLoad = "SELECT  email,nombres,apellidos,fecha_nacimiento,puntuacion, pais, region, ciudad "
                . "FROM tnt_cliente where id_usuario = $idUser";
        $res = $instance->get_data($sqlLoad);
        return $res['DATA'][0];
    }

    public function loadPerfil($id_cliente) {
        $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getConnection();
        }

        $sqlLoad = "SELECT cli.* , user.url_foto_perfil FROM tnt_cliente cli, tnt_usuario user"
                . " WHERE cli.idtnt_cliente =$id_cliente AND cli.id_usuario = user.idtnt_usuario";
        $res = $instance->get_data($sqlLoad);
        return $res['DATA'][0];
    }

    public function LocationConfiguration($pais, $region, $ciudad, $idUser) {
        $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getConnection();
        }

        $sql = "UPDATE tnt_cliente SET pais  = '$pais', region = '$region', ciudad = '$ciudad' WHERE id_usuario = $idUser";
        $res = $instance->exec($sql);
        return $res;
    }

    public function loadCustomerByUser($idUser) {
        $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getConnection();
        }

        $sql = "SELECT idtnt_cliente  FROM tnt_cliente WHERE id_usuario = $idUser";
        $res = $instance->get_data($sql);
        if ($res['STATUS'] == 'OK') {
            return $res['DATA'][0];
        }
    }

}
