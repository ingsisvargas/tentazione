<?php

include_once '../common/Database.php';

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of MembershipDAO
 *
 * @author roan0
 */
class MembershipDAO {

    function __construct() {
        
    }

    /**
     * Actualiza el estado de membresia, como una membresia valida.
     * @param type $idMembership
     * @return type
     */
    public function updateMembership($idMembership) {
        $instance = Database::getInstance();

        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getInstance();
        }

        $sqlUpdate = "UPDATE tnt_membresia SET estado=1 WHERE idtnt_membresia=$idMembership";
        $data = $instance->exec($sqlUpdate);
        return $data;
    }

    /**
     * Registra una membresia en el sistema y devuelve el codigo de la membresia.
     * @param type $fecha_inicio
     * @param type $fecha_fin
     * @param type $id_cliente
     * @param type $estado
     * @param type $idPlan
     * @return type
     */
    function registerMembership($fecha_inicio, $fecha_fin, $id_cliente, $estado, $idPlan) {
        $instance = Database::getInstance();

        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getInstance();
        }

        $sqlInsert = "INSERT INTO tnt_membresia (fecha_inicio, fecha_fin,estado,id_cliente, id_plan) "
                . " VALUES ('$fecha_inicio','$fecha_fin','$estado',$id_cliente,$idPlan)";



        $res = $instance->exec($sqlInsert);
        $idMembership = 0;
        if ($res['STATUS'] == 'OK') {
            $idMembership = $instance->last_id('tnt_membresia') - 1;
        }

        return $idMembership;
    }

    /**
     * Valida se se hah hecho pago de chat por servicio
     * @param type $idService
     * @return type
     */
    function validateStatusChatPay($idService) {
        $instance = Database::getInstance();

        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getInstance();
        }

        $sqlLoad = "SELECT count(1) cantidad FROM tnt_transferencia WHERE id_servicio = $idService"
                . " AND tipo='CHAT'";



        $data = $instance->get_data($sqlLoad);
        return $data;
    }

    /**
     * Valida si un cliente tiene membresia activa.
     * @param type $id_user
     * @return type
     */
    public function validateMemberShip($id_user) {
        $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getInstance();
        }

        $sqlLoad = "SELECT count(1) cantidad FROM tnt_membresia mem, tnt_cliente cli WHERE mem.id_cliente = cli.idtnt_cliente AND cli.id_usuario = $id_user"
                . " AND fecha_fin >= CURDATE()";

        $data = $instance->get_data($sqlLoad);
        return $data;
    }

    //put your code here
}
