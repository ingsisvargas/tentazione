<?php

include_once '../common/Database.php';

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Aspirante_DAO
 *
 * @author roan0
 */
class Candidate_DAO {

    public function registerCandidate($id_provider, $id_service) {
        $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getInstance();
        }

        $sql = "INSERT INTO tnt_aspirantes (id_proveedor,id_servicio,asignado) VALUES($id_provider,$id_service,0)";
        $result = $instance->exec($sql);
        return $result;
    }
    
    

    public function loadCandidates($idService) {
        $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getInstance();
        }

        $sql = "SELECT pro.*,asp.idtnt_aspirantes aspirante, usr.nick FROM tnt_proveedor pro, tnt_aspirantes asp, tnt_usuario usr "
                . " WHERE pro.idtnt_proveedor = asp.id_proveedor AND asp.id_servicio = $idService  AND pro.id_usuario = usr.idtnt_usuario";

        $result = $instance->get_data($sql);

        return $result;
    }
    
    
    public function loadCandidate($idService,$Provider) {
        $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getInstance();
        }
        $sql = "SELECT asp.idtnt_aspirantes aspirante ,usu.nick cliente FROM tnt_aspirantes asp, tnt_usuario usu,tnt_servicio ser ,tnt_cliente cli 
            WHERE asp.id_proveedor = $Provider AND asp.id_servicio = $idService and  ser.id_cliente = cli.idtnt_cliente  and asp.id_servicio=ser.idtnt_servicio
            and cli.id_usuario = usu.idtnt_usuario";
        $result = $instance->get_data($sql);

        return $result;
    }

}
