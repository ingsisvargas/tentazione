<?php

include_once '../common/Database.php';

/**
 * Description of Resource_DAO
 *
 * @author roan0
 */
class Resource_DAO {

    //put your code here


    function __construct() {
        
    }

    public function loadResources($idUser) {

        $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getConnection();
        }
        $sqlLoad = "SELECT idtnt_recursos, url_almacenamiento, visisbilidad, video FROM tnt_recursos WHERE id_usuario='$idUser' AND tipo='FT'";
       
        $res = $instance->get_data($sqlLoad);
        return $res['DATA'];
    }

    public function addResources($resource) {
        $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getInstance();
        }
        $sql = "INSERT INTO tnt_recursos(url_almacenamiento,tipo,id_usuario, visisbilidad, video) "
                . "VALUES ('" . $resource["url_almacenamiento"] . "',"
                . "'" . $resource["tipo"] . "',"
                . $resource["id_usuario"] . ","
                . $resource["visisbilidad"] . ",'no')";
        $res = $instance->exec($sql);

        return $res;
    }
    
     public function addResourcesVideoPublico($resource) {
        $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getInstance();
        }
        $sql = "INSERT INTO tnt_recursos(url_almacenamiento,tipo,id_usuario, visisbilidad, video) "
                . "VALUES ('" . $resource["url_almacenamiento"] . "',"
                . "'" . $resource["tipo"] . "',"
                . $resource["id_usuario"] . ","
                . $resource["visisbilidad"] . ",'si')";
        $res = $instance->exec($sql);

        return $res;
    }
    
     public function addResourcesVideoPrivado($resource) {
        $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getInstance();
        }
        $sql = "INSERT INTO tnt_recursos(url_almacenamiento,tipo,id_usuario, visisbilidad, video) "
                . "VALUES ('" . $resource["url_almacenamiento"] . "',"
                . "'" . $resource["tipo"] . "',"
                . $resource["id_usuario"] . ","
                . $resource["visisbilidad"] . ",'si')";
        $res = $instance->exec($sql);

        return $res;
    }

    public function changeStateResource($estado, $id_resource) {
        $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getInstance();
        }

        $sql = "UPDATE tnt_recursos SET visisbilidad = $estado WHERE idtnt_recursos = $id_resource";
       
        $res = $instance->exec($sql);
        return $res;
    }
    
    public function deleteResource($id_resource) {
        $instance = Database::getInstance();
        if ($instance == NULL) {
            $db = new Database();
            $instance = $db->getInstance();
        }

        $sql = "DELETE FROM tnt_recursos WHERE idtnt_recursos = $id_resource";

        $res = $instance->exec($sql);
        return $res;
    }

}
