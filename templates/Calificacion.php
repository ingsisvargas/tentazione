<html>
    <head>
        <style type="text/css">
            #calificacion table{

                background-color: #fff;


            }
            #calificacion   thead th {
                background-color: #AE181F;
                color: white;
            }
            #calificacion   tbody tr {

                height: 80px;

            }
            #calificacion tbody tr td {
                padding-top:20px;
            }
            .aouto-Scroll{
                overflow: auto;
            }
            .dialog-header-puntuacion { background-color: #AE181F; }
            .dialog-header-puntuacion h4 { color: #ffffff; }
            .bg-icon{

                font-size: 20px;
                padding: 5px;
                cursor:pointer;
            }
            .clasificacion{

                direction: rtl;/* right to left */
                unicode-bidi: bidi-override;/* bidi de bidireccional */
                text-align: left;

            }
            .clasificacion input[type="radio"] {
                display: none;
            }

            .clasificacion label:hover,
            .clasificacion label:hover ~ label {
                color: #FFD700;
            }
            .clasificacion input[type="radio"]:checked ~ label {
                color: #FFD700;
            }
        </style>
        <link href="css/jquery.dataTables.css" rel="stylesheet">
        <link href="css/jquery.dataTables_themeroller.css" rel="stylesheet">
        <script type="text/javascript" src="js/jquery.dataTables.min.js"/>       
    </head>
    <body>
        <div id="calificacion" class="row">
            <div class="col-md-12">
                <div id="tablaServices" class="table-responsive">

                </div>
            </div>
        </div>
        <div id="puntuacion" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <form>
                    <div class="modal-content">
                        <div class="modal-header dialog-header-puntuacion">
                            <a class="close" data-dismiss="modal">&times;</a>
                            <h4 class="modal-title">Puntuación</h4>
                        </div>
                        <div class="modal-body" id="modalCalificacion">
                            <div id ="alerta"></div>
                            <p>
                                <strong>Selecciona la puntación por estrellas:</strong>
                            </p>
                            <div class="clasificacion" >
                                <input id="radio1" type="radio" name="estrellas" required value="5"/>
                                <label class="bg-icon" for="radio1"><span class="glyphicon glyphicon-star gold" aria-hidden="true"></span></label>
                                <input id="radio2" type="radio" name="estrellas" value="4"/>
                                <label class="bg-icon" for="radio2"><span class="glyphicon glyphicon-star gold" aria-hidden="true"></span></label>
                                <input id="radio3" type="radio" name="estrellas" value="3"/>
                                <label class="bg-icon" for="radio3"><span class="glyphicon glyphicon-star gold" aria-hidden="true"></span></label>
                                <input id="radio4" type="radio" name="estrellas" value="2"/>
                                <label class="bg-icon" for="radio4"><span class="glyphicon glyphicon-star gold" aria-hidden="true"></span></label>
                                <input id="radio5" type="radio" name="estrellas" value="1"/>
                                <label class="bg-icon" for="radio5"><span class="glyphicon glyphicon-star gold" aria-hidden="true"></span></label>
                            </div>    
                            <p>
                                <textarea class="form-control input-sm" id="comentario" style="resize: none;" placeholder="Escriba aqui su comentario..."></textarea>
                            </p>
                        </div>
                        <div class="modal-footer">
                            <a onclick="enviar()" class="btn btn-warning" >Enviar</a>
                        </div>
                    </div>
                </form>    
            </div>
        </div>
        <script type="text/javascript">

            var idService;
            $(document).ready(function () {
               
                cargarServicios();
                 $('[data-toggle="tooltip"]').tooltip();
                 $('#serv_calificacion').DataTable();
            });
            function cargarServicios() {

                $.post("Services/LoadServicesCalification.php",
                        function (data) {
                            var tabla = '<h4 class="text-center">Servicios por calificar</h4>' +
                                    '<table id="serv_calificacion" class="table table-condensedtable-hover">' +
                                    '<thead>' +
                                    '<tr>' +
                                    '<th>Plan</th>' +
                                    '<th>Descripción</th>' +
                                    '<th>Fecha</th>' +
                                    '<th>Proveedor</th>' +
                                    '<th>Acción</th>' +
                                    '</tr>' +
                                    '</thead>' +
                                    '<tbody>';
                            if (data.STATUS === 'OK') {
                                var cant = data.DATA.length;
                                for (var i = 0; i < cant; i++) {

                                    tabla += '<tr>' +
                                            '<td>' + data.DATA[i].plan + '</td>' +
                                            '<td class="aouto-Scroll">' + data.DATA[i].descripcion + '</td>' +
                                            '<td>' + data.DATA[i].fecha_servicio + '</td>' +
                                            '<td>' + data.DATA[i].asociado + '</td>' +
                                            '<td>' +
                                            '<a data-toggle="modal" data-target="#puntuacion" data-toggle="tooltip" data-placement="right" title="Calificar" class="btn btn-warningbtn-sm" onclick="cargarId(' + data.DATA[i].idtnt_servicio + ')">' +
                                            '<span class="glyphicon glyphicon-check"></span>' +
                                            '</a>' +
                                            '</td>' +
                                            '</tr>';
                                }
                                tabla += '</tbody>' +
                                        '</table>';
                                
                                $("#tablaServices").html(tabla);
                            }
                        },
                        "json"
                        );
            }


            function cargarId(val) {

                idService = val;
            }


            function validar() {
                var bool;
                if ($('input[name="estrellas"]').is(':checked')) {
                    bool = true;
                } else {
                    bool = false;
                }
                return bool;
            }



            function enviar() {
                if (validar()) {
                    console.log('bien');
                    $("#alerta").empty();

                    var puntuacion = $('input:radio[name=estrellas]:checked').val();
                    var descripcion = $("#comentario").val();
                    var dataIn = {"id_servicio": idService, "puntuacion": puntuacion, "descripcion": descripcion};

                    $.post("Services/addQualification.php",
                            dataIn,
                            function (data) {
                                if (data.STATUS == 'OK') {
                                    alert("Calificacion Agregada");
                                    $("#puntuacion").modal('hide');
                                  
                                    cargarServicios();
                                } else {

                                    alert("Error en Calificacion");
                                  
                                }
                            },
                            "json");
                } else {
                    var alerta = "<div class=\"alert alert-warning\">" +
                            "<a class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>" +
                            "Debe seleccionar la puntación de estrellas .</div>";
                    $("#alerta").html(alerta);
                }
            }


        </script>
    </body>
</html>

