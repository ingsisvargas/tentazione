<?php
session_start();
?>
<html>
    <head>
        <style>



            .dialog-header-puntuacion { background-color: #AE181F; }
            .dialog-header-puntuacion h4 { color: #ffffff; }

            #tramitados{


                padding-top: 20px;

                background-color: #fff;


            }
            #tramitados  thead th {
                background-color: #AE181F;
                color: white;
            }

            .dialog-header-puntuacion { background-color: #AE181F; }
            .dialog-header-puntuacion h4 { color: #ffffff; }
            .bg-icon{

                font-size: 20px;
                padding: 5px;
                cursor:pointer;
            }

        </style>


    </head>    
    <body>
        <div id="tramitados" class="row">
            <div class="col-md-12">
                <h4 class="text-center">Servicios disponibles</h4>
                <div id="servicios" class="table-responsive">

                </div>
                <div id="servicio" class="modal fade " role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header dialog-header-puntuacion">
                                <a class="close" data-dismiss="modal">&times;</a>
                                <h4 class="modal-title">Información del Servicio</h4>
                            </div>
                            <div class="modal-body" id="modalCalificacion">
                                <div id ="alerta"></div>

                                <div class="clasificacion" >
                                    <p id="plan"></p>
                                    <p id="fecha"></p>
                                    <p id="cliente"></p>
                                    <p id="hora_inicio"></p>
                                    <p id="hora_fin"></p>
                                    <p id="descripcion"></p>
                                </div>    

                            </div>
                            <div class="modal-footer">
                                <a onclick="aspirar()" class="btn btn-warning" >Postularme</a>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="perfil" class="modal fade bs-example-modal-lg" role="dialog">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                            <div class="modal-header dialog-header-puntuacion">
                                <a class="close" data-dismiss="modal">&times;</a>
                                <h4 class="modal-title">Perfil cliente</h4>
                            </div>
                            <div class="modal-body" id="modalCalificacion">
                                <div id ="alerta"></div>

                                <div id="loadPerfil"  >

                                </div>    

                            </div>
                            <div class="modal-footer">
                                <a onclick="closeModal()" class="btn btn-warning" >Aceptar</a>
                            </div>
                        </div>
                    </div>
                </div> 


            </div>

            <script type="text/javascript">
                var idServicio;
                $(document).ready(function () {
                    cargarServicios();
                    $('[data-toggle="tooltip"]').tooltip();
                });
                function cargarServicios() {
                    $.post("Services/servicesAvailables.php",
                            function (data) {
                                if (data.STATUS === 'OK') {
                                    var tabla = '<table id="dataTable" class="table table-condensedtable-hover">' +
                                            '<thead>' +
                                            ' <tr>' +
                                            '<th>Fecha</th>' +
                                            '<th>Hora</th>' +
                                            '<th>Plan</th>' +
                                            '<th>Descripcion</th>' +
                                            '<th>Hora final</th>' +
                                            '<th>Acción</th>' +
                                            '</tr> ' +
                                            '</thead>' +
                                            '<tbody>';
                                    var cant = data.DATA.length;
                                    for (var i = 0; i < cant; i++) {

                                        tabla += '<tr>' +
                                                '<td>' + data.DATA[i].fecha_servicio + '</td>' +
                                                '<td>' + data.DATA[i].hora_inicio + '</td>' +
                                                '<td>' + data.DATA[i].plan + '</td>' +
                                                '<td>' + data.DATA[i].descripcion + '</td>' +
                                                '<td>' + data.DATA[i].hora_fin + '</td>' +
                                                '<td>' +
                                                '<a data-toggle="modal" data-target="#perfil" data-toggle="tooltip" data-placement="right" title = "Ver perfil"  onclick="verPerfil(' + data.DATA[i].id_cliente + ')" class = "btn btn-warning btn-sm" >' +
                                                '<span class = "glyphicon glyphicon-eye-open" > </span>' +
                                                ' </a> &nbsp; &nbsp;' +
                                                '<a data-toggle="tooltip" data-placement="right"  title="Postularse" onclick="aspirar(' + data.DATA[i].idtnt_servicio + ')" class="btn btn-warning btn-sm" >' +
                                                ' <span class="glyphicon glyphicon-ok"></span>' +
                                                ' </a>' +
                                                '</td>' +
                                                '</tr>';
                                    }

                                    tabla += ' </tbody></table>';
                                    $('#servicios').html(tabla);
                                }
                            },
                            "json");
                }


                function aspirantes(id_servicio) {
                    $("#Servicio").load("templates/aspirantes.php?id_servicio=" + id_servicio);
                }


                function verMas(id_servicio) {
                    var dataIn = {id_servicio: idServicio};
                    $.post("Services/LoadService.php",
                            dataIn,
                            function (data) {

                                if (data.STATUS === 'OK') {

                                    $("#plan").html("<label>Plan:&nbsp</label>" + data.DATA[0].plan);
                                    $("#fecha").html("<label>Fecha:&nbsp</label>" + data.DATA[0].fecha_servicio);
                                    $("#hora_inicio").html("<label>Hora inicio:&nbsp</label>" + data.DATA[0].hora_inicio);
                                    $("#hora_fin").html("<label>Hora fin:&nbsp</label>" + data.DATA[0].hora_fin);
                                    $("#descripcion").html("<label>Descripción:&nbsp</label>" + data.DATA[0].descripcion);
                                    $("#cliente").html("<label>Cliente:&nbsp</label>" + data.DATA[0].cliente);
                                }

                            },
                            "json");
                }



                function verPerfil(id_cliente) {
                    $("#loadPerfil").load("templates/perfilLoad.php?id_userLoad=" + id_cliente);
                }


                function aspirar(idServicio) {
                    var dataIn = {"id_servicio": idServicio};
                    $.post("Services/registerCandidate.php",
                            dataIn,
                            function (data) {
                                console.log('141424');
                                if (data.STATUS === 'OK') {
                                    $.post("Services/loadCandidate.php",
                                            dataIn,
                                            function (data) {
                                                var cliente = data.DATA[0].cliente;
                                                var aspirante = data.DATA[0].aspirante;
                                                var provedor = "<?php echo $_SESSION['USER']; ?>";
                                                var refRooms = connect.ref('rooms').child(aspirante);
                                                var jsonMessage = {
                                                "customer" : {
                                                "nickname" : cliente,
                                                        "connected" : false
                                                },
                                                        "messageCustomer" : false,
                                                        "messageProvider" : false,
                                                        "stateService":'AB',
                                                        "provider" : {
                                                        "nickname" : provedor,
                                                                "connected" : false                                                       
                                                    }
                                                };
                                                console.log(jsonMessage);
                                                refRooms.set(jsonMessage);
                                            }, "json");
                                            alert("Se ha postulado... Espere que el cliente le contacte.");
                                            $("#servicio").modal('hide');
                                            cargarServicios();
                                        } else {
                                            alert("Error en postulación");
                                        }
                                    },
                                    "json");
                        }


                        function closeModal() {
                            $("#perfil").modal('hide');
                        }


            </script>
    </body>


</html>
