<?php
session_start();

if (!isset($_SESSION['USER'])) {
    ?>
    <script type="text/javascript">
        window.location = "index.html";
    </script>
    <?php
}
?>
<html>
    <head>
        <style type="text/css">


            .dialog-header-puntuacion { background-color: #AE181F; }
            .dialog-header-puntuacion h4 { color: #ffffff; }
            img { height: 210px; max-width: 100%};

            .imgPer{

                width: 210px;
                height: 210px;   
            }
            div.panel {
                font-size: 16px;
                height: 210px;
                width: 100%;
            }
            div.panel p label{

                margin-right: 10px;
                font-size: 14px;
            }
            .button{
                margin-bottom: 10px;

            }
            #info{
                margin-left: 15px;
                max-width: 550px;
            }
            .button > .pull-left {
                margin-right: 10px;
            }
            .carousel-inner{text-align: center;}
            .carousel-inner > .item {
                width:100%;
                height:440px;
            }
            .carousel-inner > .item > img {
                width:auto;
                display: inline-block;
                height: auto;
                max-height:440px;
            }

            .gold{
                color:  #FFD700;
            }
        </style>

    </head>
    <body>
        <div class = "row">

            <div class="col-md-6">
                <div class="button">
                    <a onclick="loadGallery()" class="btn btn-sm btn-warning pull-left" data-toggle="modal" data-target="#changeImage" data-toggle="tooltip" data-placement="left"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
                    <div class="clearfix"></div>
                </div>   
                <div class="row">
                    <div class="col-md-5">  
                        <div class="imgPer">
                            <img id="foto-perfil" src ="" class="img-thumbnail"/>
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div  class="panel panel-default">
                            <div class="panel-heading text-center">Datos Ubicación</div>  
                            <div class="panel-body">
                                <p id="pais"></p>
                                <p id="region"></p>
                                <p id="ciudad"></p>
                                <!-- <a class="btn btn-warning pull-right">Configurar</a>  -->
                                <div class="clearfix"></div>
                            </div>
                        </div> 
                    </div>  
                </div>
                <div class="row">
                    <div id="info" class="panel pnael-default">
                        <div class="panel-heading text-center">
                            Datos Usuario
                        </div> 
                        <div class="panel-body">
                            <p id="nombre"></p>
                            <p id="apellido"></p>
                            <p id="edad"></p>
                            <p id="puntuacion">



                            </p> 
                        </div>  
                    </div>  
                </div> 

                <div class="row">
                    <div class="col-sm-6" id="videos">


                    </div> 
                </div> 




            </div>
            <div class="col-md-4" style="float:right;">


                <div class="button col-md-4" >
                    <a  class="btn btn-sm btn-warning  "   data-toggle="modal" data-target="#loadFile" data-toggle="tooltip" data-placement="right" title="Subir Fotos"><span class="glyphicon  glyphicon-upload" aria-hidden="true"> -</span><span class="glyphicon glyphicon-picture" aria-hidden="true"> </span></a>

                </div> 


                <div class="button col-md-4" id="divVideoPublico" >

                </div> 

                <div class="button col-md-4"  id="divVideoPrivado">

                </div> 

            </div> 
            <br><br>
            <br> <br>  <br><br>
            <br> <br>           <div class="col-md-6">

                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol id="list-carrusel" class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                        <li data-target="#myCarousel" data-slide-to="3"></li>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div id="img-carrusel" class="carousel-inner" role="listbox">

                    </div>

                    <!-- Left and right controls -->
                    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>

            </div>  



        </div>

        <div id="loadFile" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header dialog-header-puntuacion">
                        <a class="close" data-dismiss="modal">&times;</a>
                        <h4 class="modal-title">Subir Archivo</h4>
                    </div>
                    <div class="modal-body" id="modalLoadFile">  
                        <div id="content">
                            <input type="file" name="files[]" id="filer_input2" multiple="multiple">                                
                        </div>
                    </div>                       
                </div>              
            </div>
        </div>
        <div class="modal-footer">

        </div>


        <!--MODAL PROPIO PARA SUBIR VIDEO Publico-->
        <div id="loadFileVideoPublico" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header dialog-header-puntuacion">
                        <a class="close" data-dismiss="modal">&times;</a>
                        <h4 class="modal-title">Subir Video Público</h4>
                    </div>
                    <div class="modal-body" id="modalLoadFile">  
                        <div id="content">
                            <input type="file" name="files[]" id="filer_input3" multiple="multiple">                                
                        </div>
                    </div>                       
                </div>              
            </div>
        </div>
        <div class="modal-footer">

        </div>
        <!--MODAL PROPIO PARA SUBIR VIDEO CENSURADO-->
        <div id="loadFileVideoPrivado" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header dialog-header-puntuacion">
                        <a class="close" data-dismiss="modal">&times;</a>
                        <h4 class="modal-title">Subir Video Privado</h4>
                    </div>
                    <div class="modal-body" id="modalLoadFile">  
                        <div id="content">
                            <input type="file" name="files[]" id="filer_input4" multiple="multiple">                                
                        </div>
                    </div>                       
                </div>              
            </div>
        </div>
        <div class="modal-footer">

        </div>

        <div id="changeImage" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header dialog-header-lo">
                        <a class="close" data-dismiss="modal">&times;</a>
                        <h4 class="modal-title">Foto de perfil.</h4>

                        <div class="modal-body" id="modalCalificacion"> 
                            <div id="content">
                                <div id="myCarousel-change" class="carousel slide" data-ride="carousel">
                                    <!-- Indicators -->
                                    <ol id="list-carrusel-change" class="carousel-indicators">

                                    </ol>
                                    <!-- Wrapper for slides -->
                                    <div id="img-carrusel-change" class="carousel-inner" role="listbox">

                                    </div>

                                    <!-- Left and right controls -->
                                    <a class="left carousel-control" href="#myCarousel-change" role="button" data-slide="prev">
                                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="right carousel-control" href="#myCarousel-change" role="button" data-slide="next">
                                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>

                                </div>
                            </div>
                        </div>                        
                    </div>

                </div>
            </div>
        </div>


        <script type="text/javascript">
            $(document).ready(function () {
                $("#filer_input2").filer({
                    limit: null,
                    maxSize: null,
                    extensions: null,
                    changeInput: '<div class="jFiler-input-dragDrop"><div class="jFiler-input-inner"><div class="jFiler-input-icon"><i class="icon-jfi-cloud-up-o"></i></div><div class="jFiler-input-text"><h3>Arrastre y suelte archivos aquí</h3> <span style="display:inline-block; margin: 15px 0">or</span></div><a class="jFiler-input-choose-btn blue">gestor de archivos</a></div></div>',
                    showThumbs: true,
                    theme: "dragdropbox",
                    templates: {
                        box: '<ul class="jFiler-items-list jFiler-items-grid"></ul>',
                        item: '<li class="jFiler-item">\
                                                <div class="jFiler-item-container">\
                                                        <div class="jFiler-item-inner">\
                                                                <div class="jFiler-item-thumb">\
                                                                        <div class="jFiler-item-status"></div>\
                                                                        <div class="jFiler-item-thumb-overlay">\
                                                                                <div class="jFiler-item-info">\
                                                                                        <div style="display:table-cell;vertical-align: middle;">\
                                                                                                <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name}}</b></span>\
                                                                                                <span class="jFiler-item-others">{{fi-size2}}</span>\
                                                                                        </div>\
                                                                                </div>\
                                                                        </div>\
                                                                        {{fi-image}}\
                                                                </div>\
                                                                <div class="jFiler-item-assets jFiler-row">\
                                                                        <ul class="list-inline pull-left">\
                                                                                <li>{{fi-progressBar}}</li>\
                                                                        </ul>\
                                                                        <ul class="list-inline pull-right">\
                                                                                <li><a class="icon-jfi-trash jFiler-item-trash-action"></a></li>\
                                                                        </ul>\
                                                                </div>\
                                                        </div>\
                                                </div>\
                                        </li>',
                        itemAppend: '<li class="jFiler-item">\
                                                        <div class="jFiler-item-container">\
                                                                <div class="jFiler-item-inner">\
                                                                        <div class="jFiler-item-thumb">\
                                                                                <div class="jFiler-item-status"></div>\
                                                                                <div class="jFiler-item-thumb-overlay">\
                                                                                        <div class="jFiler-item-info">\
                                                                                                <div style="display:table-cell;vertical-align: middle;">\
                                                                                                        <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name}}</b></span>\
                                                                                                        <span class="jFiler-item-others">{{fi-size2}}</span>\
                                                                                                </div>\
                                                                                        </div>\
                                                                                </div>\
                                                                                {{fi-image}}\
                                                                        </div>\
                                                                        <div class="jFiler-item-assets jFiler-row">\
                                                                                <ul class="list-inline pull-left">\
                                                                                        <li><span class="jFiler-item-others">{{fi-icon}}</span></li>\
                                                                                </ul>\
                                                                                <ul class="list-inline pull-right">\
                                                                                        <li><a class="icon-jfi-trash jFiler-item-trash-action"></a></li>\
                                                                                </ul>\
                                                                        </div>\
                                                                </div>\
                                                        </div>\
                                                </li>',
                        progressBar: '<div class="bar"></div>',
                        itemAppendToEnd: false,
                        canvasImage: true,
                        removeConfirmation: false,
                        _selectors: {
                            list: '.jFiler-items-list',
                            item: '.jFiler-item',
                            progressBar: '.bar',
                            remove: '.jFiler-item-trash-action'
                        }
                    },
                    dragDrop: {
                        dragEnter: null,
                        dragLeave: null,
                        drop: null,
                        dragContainer: null,
                    },
                    uploadFile: {
                        url: "Services/uploadFile.php",
                        data: null,
                        type: 'POST',
                        enctype: 'multipart/form-data',
                        synchron: true,
                        beforeSend: function () {},
                        success: function (data, itemEl, listEl, boxEl, newInputEl, inputEl, id) {
                            var parent = itemEl.find(".jFiler-jProgressBar").parent(),
                                    new_file_name = JSON.parse(data),
                                    filerKit = inputEl.prop("jFiler");
                            loadDataPerfil();
                            location.reload();
                            filerKit.files_list[id].name = new_file_name;

                            itemEl.find(".jFiler-jProgressBar").fadeOut("slow", function () {
                                $("<div class=\"jFiler-item-others text-success\"><i class=\"icon-jfi-check-circle\"></i> Exito</div>").hide().appendTo(parent).fadeIn("slow");
                            });


                        },
                        error: function (el) {
                            var parent = el.find(".jFiler-jProgressBar").parent();
                            el.find(".jFiler-jProgressBar").fadeOut("slow", function () {
                                $("<div class=\"jFiler-item-others text-error\"><i class=\"icon-jfi-minus-circle\"></i> Error</div>").hide().appendTo(parent).fadeIn("slow");
                            });

                        },
                        statusCode: null,
                        onProgress: null,
                        onComplete: null

                    },
                    files: null,
                    addMore: false,
                    allowDuplicates: true,
                    clipBoardPaste: true,
                    excludeName: null,
                    beforeRender: null,
                    afterRender: null,
                    beforeShow: null,
                    beforeSelect: null,
                    onSelect: null,
                    afterShow: null,
                    onRemove: function (itemEl, file, id, listEl, boxEl, newInputEl, inputEl) {
                        var filerKit = inputEl.prop("jFiler"),
                                file_name = filerKit.files_list[id].name;

                        $.post('Services/removeFile.php', {file: file_name});
                    },
                    onEmpty: null,
                    options: null,
                    dialogs: {
                        alert: function (text) {
                            return alert(text);
                        },
                        confirm: function (text, callback) {
                            confirm(text) ? callback() : null;
                        }
                    },
                    captions: {
                        button: "Seleccionar archivos",
                        feedback: "Selecciona archivos para subir",
                        feedback2: "Los archivos fueron elegidos",
                        drop: "Descargar archivo aquí para cargar",
                        removeConfirmation: "Seguro que quieres eliminar este archivo?",
                        errors: {
                            filesLimit: "Sólo los archivos {{fi-limit}}  se pueden cargar",
                            filesType: "Sólo las imágenes se pueden cargar.",
                            filesSize: "{{fi-name}} ¡Es demasiado largo! Cargue el archivo hasta  {{fi-maxSize}} MB.",
                            filesSizeAll: "¡Los archivos que has elegido son demasiado grandes! Cargue archivos hasta {{fi-maxSize}} MB."
                        }
                    }
                });

                //CODIGO MODAL VIDEO PUBLICOOOOO////////////////////////////////////////
                $("#filer_input3").filer({
                    limit: null,
                    maxSize: null,
                    extensions: null,
                    changeInput: '<div class="jFiler-input-dragDrop"><div class="jFiler-input-inner"><div class="jFiler-input-icon"><i class="icon-jfi-cloud-up-o"></i></div><div class="jFiler-input-text"><h3>Arrastre y suelte archivos aquí</h3> <span style="display:inline-block; margin: 15px 0">or</span></div><a class="jFiler-input-choose-btn blue">gestor de archivos</a></div></div>',
                    showThumbs: true,
                    theme: "dragdropbox",
                    templates: {
                        box: '<ul class="jFiler-items-list jFiler-items-grid"></ul>',
                        item: '<li class="jFiler-item">\
                                                <div class="jFiler-item-container">\
                                                        <div class="jFiler-item-inner">\
                                                                <div class="jFiler-item-thumb">\
                                                                        <div class="jFiler-item-status"></div>\
                                                                        <div class="jFiler-item-thumb-overlay">\
                                                                                <div class="jFiler-item-info">\
                                                                                        <div style="display:table-cell;vertical-align: middle;">\
                                                                                                <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name}}</b></span>\
                                                                                                <span class="jFiler-item-others">{{fi-size2}}</span>\
                                                                                        </div>\
                                                                                </div>\
                                                                        </div>\
                                                                        {{fi-image}}\
                                                                </div>\
                                                                <div class="jFiler-item-assets jFiler-row">\
                                                                        <ul class="list-inline pull-left">\
                                                                                <li>{{fi-progressBar}}</li>\
                                                                        </ul>\
                                                                        <ul class="list-inline pull-right">\
                                                                                <li><a class="icon-jfi-trash jFiler-item-trash-action"></a></li>\
                                                                        </ul>\
                                                                </div>\
                                                        </div>\
                                                </div>\
                                        </li>',
                        itemAppend: '<li class="jFiler-item">\
                                                        <div class="jFiler-item-container">\
                                                                <div class="jFiler-item-inner">\
                                                                        <div class="jFiler-item-thumb">\
                                                                                <div class="jFiler-item-status"></div>\
                                                                                <div class="jFiler-item-thumb-overlay">\
                                                                                        <div class="jFiler-item-info">\
                                                                                                <div style="display:table-cell;vertical-align: middle;">\
                                                                                                        <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name}}</b></span>\
                                                                                                        <span class="jFiler-item-others">{{fi-size2}}</span>\
                                                                                                </div>\
                                                                                        </div>\
                                                                                </div>\
                                                                                {{fi-image}}\
                                                                        </div>\
                                                                        <div class="jFiler-item-assets jFiler-row">\
                                                                                <ul class="list-inline pull-left">\
                                                                                        <li><span class="jFiler-item-others">{{fi-icon}}</span></li>\
                                                                                </ul>\
                                                                                <ul class="list-inline pull-right">\
                                                                                        <li><a class="icon-jfi-trash jFiler-item-trash-action"></a></li>\
                                                                                </ul>\
                                                                        </div>\
                                                                </div>\
                                                        </div>\
                                                </li>',
                        progressBar: '<div class="bar"></div>',
                        itemAppendToEnd: false,
                        canvasImage: true,
                        removeConfirmation: false,
                        _selectors: {
                            list: '.jFiler-items-list',
                            item: '.jFiler-item',
                            progressBar: '.bar',
                            remove: '.jFiler-item-trash-action'
                        }
                    },
                    dragDrop: {
                        dragEnter: null,
                        dragLeave: null,
                        drop: null,
                        dragContainer: null,
                    },
                    uploadFile: {
                        url: "Services/uploadVideoPublico.php",
                        data: null,
                        type: 'POST',
                        enctype: 'multipart/form-data',
                        synchron: true,
                        beforeSend: function () {},
                        success: function (data, itemEl, listEl, boxEl, newInputEl, inputEl, id) {
                            var parent = itemEl.find(".jFiler-jProgressBar").parent(),
                                    new_file_name = JSON.parse(data),
                                    filerKit = inputEl.prop("jFiler");
                            loadDataPerfil();
                            location.reload();
                            filerKit.files_list[id].name = new_file_name;

                            itemEl.find(".jFiler-jProgressBar").fadeOut("slow", function () {
                                $("<div class=\"jFiler-item-others text-success\"><i class=\"icon-jfi-check-circle\"></i> Exito</div>").hide().appendTo(parent).fadeIn("slow");
                            });
                        },
                        error: function (el) {
                            var parent = el.find(".jFiler-jProgressBar").parent();
                            el.find(".jFiler-jProgressBar").fadeOut("slow", function () {
                                $("<div class=\"jFiler-item-others text-error\"><i class=\"icon-jfi-minus-circle\"></i> Error</div>").hide().appendTo(parent).fadeIn("slow");
                            });
                        },
                        statusCode: null,
                        onProgress: null,
                        onComplete: null
                    },
                    files: null,
                    addMore: false,
                    allowDuplicates: true,
                    clipBoardPaste: true,
                    excludeName: null,
                    beforeRender: null,
                    afterRender: null,
                    beforeShow: null,
                    beforeSelect: null,
                    onSelect: null,
                    afterShow: null,
                    onRemove: function (itemEl, file, id, listEl, boxEl, newInputEl, inputEl) {
                        var filerKit = inputEl.prop("jFiler"),
                                file_name = filerKit.files_list[id].name;

                        $.post('Services/removeFile.php', {file: file_name});
                    },
                    onEmpty: null,
                    options: null,
                    dialogs: {
                        alert: function (text) {
                            return alert(text);
                        },
                        confirm: function (text, callback) {
                            confirm(text) ? callback() : null;
                        }
                    },
                    captions: {
                        button: "Seleccionar archivos",
                        feedback: "Selecciona archivos para subir",
                        feedback2: "Los archivos fueron elegidos",
                        drop: "Descargar archivo aquí para cargar",
                        removeConfirmation: "Seguro que quieres eliminar este archivo?",
                        errors: {
                            filesLimit: "Sólo los archivos {{fi-limit}}  se pueden cargar",
                            filesType: "Sólo las imágenes se pueden cargar.",
                            filesSize: "{{fi-name}} ¡Es demasiado largo! Cargue el archivo hasta  {{fi-maxSize}} MB.",
                            filesSizeAll: "¡Los archivos que has elegido son demasiado grandes! Cargue archivos hasta {{fi-maxSize}} MB."
                        }
                    }

                });
                /////////////////////////////////////////////////////////////////////////


                //CODIGO MODAL VIDEO PRIVADOOOOO////////////////////////////////////////
                $("#filer_input4").filer({
                    limit: null,
                    maxSize: null,
                    extensions: null,
                    changeInput: '<div class="jFiler-input-dragDrop"><div class="jFiler-input-inner"><div class="jFiler-input-icon"><i class="icon-jfi-cloud-up-o"></i></div><div class="jFiler-input-text"><h3>Arrastre y suelte archivos aquí</h3> <span style="display:inline-block; margin: 15px 0">or</span></div><a class="jFiler-input-choose-btn blue">gestor de archivos</a></div></div>',
                    showThumbs: true,
                    theme: "dragdropbox",
                    templates: {
                        box: '<ul class="jFiler-items-list jFiler-items-grid"></ul>',
                        item: '<li class="jFiler-item">\
                                                <div class="jFiler-item-container">\
                                                        <div class="jFiler-item-inner">\
                                                                <div class="jFiler-item-thumb">\
                                                                        <div class="jFiler-item-status"></div>\
                                                                        <div class="jFiler-item-thumb-overlay">\
                                                                                <div class="jFiler-item-info">\
                                                                                        <div style="display:table-cell;vertical-align: middle;">\
                                                                                                <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name}}</b></span>\
                                                                                                <span class="jFiler-item-others">{{fi-size2}}</span>\
                                                                                        </div>\
                                                                                </div>\
                                                                        </div>\
                                                                        {{fi-image}}\
                                                                </div>\
                                                                <div class="jFiler-item-assets jFiler-row">\
                                                                        <ul class="list-inline pull-left">\
                                                                                <li>{{fi-progressBar}}</li>\
                                                                        </ul>\
                                                                        <ul class="list-inline pull-right">\
                                                                                <li><a class="icon-jfi-trash jFiler-item-trash-action"></a></li>\
                                                                        </ul>\
                                                                </div>\
                                                        </div>\
                                                </div>\
                                        </li>',
                        itemAppend: '<li class="jFiler-item">\
                                                        <div class="jFiler-item-container">\
                                                                <div class="jFiler-item-inner">\
                                                                        <div class="jFiler-item-thumb">\
                                                                                <div class="jFiler-item-status"></div>\
                                                                                <div class="jFiler-item-thumb-overlay">\
                                                                                        <div class="jFiler-item-info">\
                                                                                                <div style="display:table-cell;vertical-align: middle;">\
                                                                                                        <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name}}</b></span>\
                                                                                                        <span class="jFiler-item-others">{{fi-size2}}</span>\
                                                                                                </div>\
                                                                                        </div>\
                                                                                </div>\
                                                                                {{fi-image}}\
                                                                        </div>\
                                                                        <div class="jFiler-item-assets jFiler-row">\
                                                                                <ul class="list-inline pull-left">\
                                                                                        <li><span class="jFiler-item-others">{{fi-icon}}</span></li>\
                                                                                </ul>\
                                                                                <ul class="list-inline pull-right">\
                                                                                        <li><a class="icon-jfi-trash jFiler-item-trash-action"></a></li>\
                                                                                </ul>\
                                                                        </div>\
                                                                </div>\
                                                        </div>\
                                                </li>',
                        progressBar: '<div class="bar"></div>',
                        itemAppendToEnd: false,
                        canvasImage: true,
                        removeConfirmation: false,
                        _selectors: {
                            list: '.jFiler-items-list',
                            item: '.jFiler-item',
                            progressBar: '.bar',
                            remove: '.jFiler-item-trash-action'
                        }
                    },
                    dragDrop: {
                        dragEnter: null,
                        dragLeave: null,
                        drop: null,
                        dragContainer: null,
                    },
                    uploadFile: {
                        url: "Services/uploadVideoPrivado.php",
                        data: null,
                        type: 'POST',
                        enctype: 'multipart/form-data',
                        synchron: true,
                        beforeSend: function () {},
                        success: function (data, itemEl, listEl, boxEl, newInputEl, inputEl, id) {
                            var parent = itemEl.find(".jFiler-jProgressBar").parent(),
                                    new_file_name = JSON.parse(data),
                                    filerKit = inputEl.prop("jFiler");
                            loadDataPerfil();
                            location.reload();
                            filerKit.files_list[id].name = new_file_name;

                            itemEl.find(".jFiler-jProgressBar").fadeOut("slow", function () {
                                $("<div class=\"jFiler-item-others text-success\"><i class=\"icon-jfi-check-circle\"></i> Exito</div>").hide().appendTo(parent).fadeIn("slow");
                            });
                        },
                        error: function (el) {
                            var parent = el.find(".jFiler-jProgressBar").parent();
                            el.find(".jFiler-jProgressBar").fadeOut("slow", function () {
                                $("<div class=\"jFiler-item-others text-error\"><i class=\"icon-jfi-minus-circle\"></i> Error</div>").hide().appendTo(parent).fadeIn("slow");
                            });
                        },
                        statusCode: null,
                        onProgress: null,
                        onComplete: null
                    },
                    files: null,
                    addMore: false,
                    allowDuplicates: true,
                    clipBoardPaste: true,
                    excludeName: null,
                    beforeRender: null,
                    afterRender: null,
                    beforeShow: null,
                    beforeSelect: null,
                    onSelect: null,
                    afterShow: null,
                    onRemove: function (itemEl, file, id, listEl, boxEl, newInputEl, inputEl) {
                        var filerKit = inputEl.prop("jFiler"),
                                file_name = filerKit.files_list[id].name;

                        $.post('Services/removeFile.php', {file: file_name});
                    },
                    onEmpty: null,
                    options: null,
                    dialogs: {
                        alert: function (text) {
                            return alert(text);
                        },
                        confirm: function (text, callback) {
                            confirm(text) ? callback() : null;
                        }
                    },
                    captions: {
                        button: "Seleccionar archivos",
                        feedback: "Selecciona archivos para subir",
                        feedback2: "Los archivos fueron elegidos",
                        drop: "Descargar archivo aquí para cargar",
                        removeConfirmation: "Seguro que quieres eliminar este archivo?",
                        errors: {
                            filesLimit: "Sólo los archivos {{fi-limit}}  se pueden cargar",
                            filesType: "Sólo las imágenes se pueden cargar.",
                            filesSize: "{{fi-name}} ¡Es demasiado largo! Cargue el archivo hasta  {{fi-maxSize}} MB.",
                            filesSizeAll: "¡Los archivos que has elegido son demasiado grandes! Cargue archivos hasta {{fi-maxSize}} MB."
                        }
                    }
                });
                /////////////////////////////////////////////////////////////////////////

                loadDataPerfil();

            });



            /**
             * Permite cargar la información de perfil.
             * @returns {undefined}
             */
            function loadDataPerfil() {

                $("#foto-perfil").attr("src", "<?php echo $_SESSION['DATA']['url_foto_perfil']; ?>");
                $.ajax({
                    url: 'Services/loadPersonalInfo.php',
                    type: 'GET',
                    success: function (json) {
                        var Result = JSON.parse(json);
                        console.log(Result);
                        var puntuacion = "<label >Puntuación:</label>";
                        var numero = parseInt(Result.puntuacion);
                        $("#pais").html("<label>Pais:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>" + Result.pais);
                        $("#region").html("<label>Región:</label>" + Result.region);
                        $("#ciudad").html("<label>Ciudad:</label>" + Result.ciudad);
                        $("#nombre").html("<label>Nombre:</label>" + Result.nombres);
                        $("#apellido").html("<label>Apellido:</label>" + Result.apellidos);
                        $("#edad").html("<label>Edad:</label>" + Result.fecha_nacimiento);
                        for (var i = 0; i < numero; i++) {
                            puntuacion += "<span class=\"glyphicon glyphicon-star gold\" aria-hidden=\"true\"></span>";
                        }
                        $("#puntuacion").html(puntuacion);
                        if (Result.IMAGENES.length > 0) {
                            var list_carrusel = "<li data-target=\"#myCarousel\" data-slide-to=\"0\" class=\"active\"></li>";
                            var img_carrusel = "<div class=\"item active\"><img class=\"img-responsive\"  src=\"" + Result.IMAGENES[0].url_almacenamiento + "\">";
                            img_carrusel += " <div class = \"carousel-caption\">";
                            
                           
                        var video="";
                             
                        
                             var cpresionado="";
                            var cpresionado2="";
                            
                             
                            if(Result.IMAGENES[0].visisbilidad=="1"){
                                 cpresionado="chocolate";
                            }

                            if (Result.IMAGENES[0].visisbilidad == "0") {
                                cpresionado2 = "chocolate";
                            }
                           
                           
                            //console.log(Result.IMAGENES[0].idtnt_recursos);
                            img_carrusel += '<a data-toggle="tooltip" data-placement="right"   title="Mostrar publico"  style="background-color:'+cpresionado+'" onclick="cambiarEstadoImagen(1,' + Result.IMAGENES[0].idtnt_recursos + ')" class="btn btn-warning btn-sm">' +
                                    ' <span class="glyphicon glyphicon-ok-sign"></span>' +
                                    '</a>';
                            img_carrusel += "&nbsp";
                            img_carrusel += "&nbsp";
                            img_carrusel += '<a data-toggle="tooltip" data-placement="right"  title="Ocultar publico" style="background-color:' + cpresionado2 + '" onclick="cambiarEstadoImagen(0,' + Result.IMAGENES[0].idtnt_recursos + ')" class="btn btn-warning btn-sm">' +
                                    ' <span class="glyphicon glyphicon-remove-sign"></span>' +
                                    '</a>';
                                img_carrusel += "&nbsp";
                                img_carrusel += "&nbsp";
                                img_carrusel += '<a data-toggle="tooltip" data-placement="right"  title="Ocultar" style="background-color:#d33434; "  onclick="BorrarImagen(' + Result.IMAGENES[0].idtnt_recursos + ')" class="btn btn-warning btn-sm">' +
                                        ' <span class="glyphicon glyphicon-trash"></span>' +
                                        '</a>';

                            img_carrusel += "</div>";
                            img_carrusel += "</div>";
                            var hayPublico="no";
                            var hayPrivado="no";
                            
                            for (var i = 1; i < Result.IMAGENES.length; i++)
                            {
                                
                            var presionado="";
                            var presionado2="";
                            
                             console.log(Result.IMAGENES[i].visisbilidad);
                            if(Result.IMAGENES[i].visisbilidad=="1"){
                                 presionado="chocolate";
                            }
                            
                            if(Result.IMAGENES[i].visisbilidad=="0"){
                                  presionado2="chocolate";
                            }
                           
                             if(Result.IMAGENES[i] && Result.IMAGENES[i].video=="no"){  
                               console.log("veces");              
                                list_carrusel += "<li data-target=\"#myCarousel\" data-slide-to=\"" + i + "\" ></li>";
                                img_carrusel += "<div class=\"item\"><img class=\"img-responsive\" src=\"" + Result.IMAGENES[i].url_almacenamiento + "\">";
                                img_carrusel += " <div class = \"carousel-caption\">";
                                img_carrusel += '<a data-toggle="tooltip" data-placement="right"  title="Mostrar" style="background-color:'+presionado+'"  onclick="cambiarEstadoImagen(1,' + Result.IMAGENES[i].idtnt_recursos + ')" class="btn btn-warning btn-sm">' +
                                        ' <span class="glyphicon glyphicon-ok-sign"></span>' +
                                        '</a>';
                                img_carrusel += "&nbsp";
                                img_carrusel += "&nbsp";
                                img_carrusel += '<a data-toggle="tooltip" data-placement="right"  title="Ocultar" style="background-color:'+presionado2+'"  onclick="cambiarEstadoImagen(0,' + Result.IMAGENES[i].idtnt_recursos + ')" class="btn btn-warning btn-sm">' +
                                       ' <span class="glyphicon glyphicon-remove-sign"></span>' +
                                        '</a>';
                                img_carrusel += "&nbsp";
                                img_carrusel += "&nbsp";
                                img_carrusel += '<a data-toggle="tooltip" data-placement="right"  title="Eliminar" style="background-color:#d12727; "  onclick="BorrarImagen(' + Result.IMAGENES[i].idtnt_recursos + ')" class="btn btn-warning btn-sm">' +
                                        ' <span class="glyphicon glyphicon-trash"></span>' +
                                        '</a>';

                                img_carrusel += "</div>";
                                img_carrusel += "</div>";
                            }else if( Result.IMAGENES[i].video=="si"){
                                if(Result.IMAGENES[i].visisbilidad=="1"){
                             
                            video+="<div class='embed-responsive embed-responsive-16by9'>";
                                 video+="<video  controls>";
                                video+="<source src='"+Result.IMAGENES[i].url_almacenamiento+"' type='video/mp4'>";
                                video+=" </video>";
                                 video+=" </div>";
                      
                                video+="<h3>Video Público</h3>"
                                video+='<a data-toggle="tooltip" data-placement="right"  title="Eliminar" style="background-color:#d12727;  "  onclick="BorrarVideo(' + Result.IMAGENES[i].idtnt_recursos + ')" class="btn btn-warning btn-sm">' +
                                        ' <span class="glyphicon glyphicon-trash"> Eliminar Público</span>' +
                                        '</a> <br>';  
                                    console.log(Result.IMAGENES[i].url_almacenamiento);
                                    hayPublico="si";
                                }
                                
                                else if(Result.IMAGENES[i].visisbilidad=="0"){
                                
                            video+="<div class='embed-responsive embed-responsive-16by9'>";    
                                video+="<video  controls>";
                                video+="<source src='"+Result.IMAGENES[i].url_almacenamiento+"' type='video/mp4'>";
                                video+=" </video>";
                                 video+=" </div>";
                                video+="<h3>Video Privado</h3>"
                                video+='<a data-toggle="tooltip" data-placement="right"  title="Eliminar" style="background-color:#d12727;  "  onclick="BorrarVideo(' + Result.IMAGENES[i].idtnt_recursos + ')" class="btn btn-warning btn-sm">' +
                                        ' <span class="glyphicon glyphicon-trash"> Eliminar Privado</span>' +
                                        '</a>';
                                hayPrivado="si";
                                }

                                if (Result.IMAGENES[i] && Result.IMAGENES[i].video == "no") {
                                    console.log("veces");
                                    list_carrusel += "<li data-target=\"#myCarousel\" data-slide-to=\"" + i + "\" ></li>";
                                    img_carrusel += "<div class=\"item\"><img class=\"img-responsive\" src=\"" + Result.IMAGENES[i].url_almacenamiento + "\">";
                                    img_carrusel += " <div class = \"carousel-caption\">";
                                    img_carrusel += '<a data-toggle="tooltip" data-placement="right"  title="Mostrar" style="background-color:' + presionado + '"  onclick="cambiarEstadoImagen(1,' + Result.IMAGENES[i].idtnt_recursos + ')" class="btn btn-warning btn-sm">' +
                                            ' <span class="glyphicon glyphicon-ok-sign"></span>' +
                                            '</a>';
                                    img_carrusel += "&nbsp";
                                    img_carrusel += "&nbsp";
                                    img_carrusel += '<a data-toggle="tooltip" data-placement="right"  title="Ocultar" style="background-color:' + presionado2 + '"  onclick="cambiarEstadoImagen(0,' + Result.IMAGENES[i].idtnt_recursos + ')" class="btn btn-warning btn-sm">' +
                                            ' <span class="glyphicon glyphicon-remove-sign"></span>' +
                                            '</a>';
                                    img_carrusel += "&nbsp";
                                    img_carrusel += "&nbsp";
                                    img_carrusel += '<a data-toggle="tooltip" data-placement="right"  title="Eliminar" style="background-color:#d12727; "  onclick="BorrarImagen(' + Result.IMAGENES[i].idtnt_recursos + ')" class="btn btn-warning btn-sm">' +
                                            ' <span class="glyphicon glyphicon-trash"></span>' +
                                            '</a>';

                                    img_carrusel += "</div>";
                                    img_carrusel += "</div>";
                                } else if (Result.IMAGENES[i].video == "si") {
                                    if (Result.IMAGENES[i].visisbilidad == "1") {
                                        video += "<video  controls>";
                                        video += "<source src='" + Result.IMAGENES[i].url_almacenamiento + "'  type='video/mp4; codecs=\"avc1.42E01E, mp4a.40.2\"'>";
                                        video += " </video>";
                                        video += " </div>";

                                        video += "<h3>Video Público</h3>"
                                        video += '<a data-toggle="tooltip" data-placement="right"  title="Eliminar" style="background-color:#d12727;  "  onclick="BorrarVideo(' + Result.IMAGENES[i].idtnt_recursos + ')" class="btn btn-warning btn-sm">' +
                                                ' <span class="glyphicon glyphicon-trash"></span>' +
                                                '</a>';
                                        console.log(Result.IMAGENES[i].url_almacenamiento);
                                        hayPublico = "si";
                                    } else if (Result.IMAGENES[i].visisbilidad == "0") {
                                        video += "<video  controls>";
                                        video += "<source src='" + Result.IMAGENES[i].url_almacenamiento + "'  type='video/mp4; codecs=\"avc1.42E01E, mp4a.40.2\"'>";
                                        video += " </video>";
                                        video += " </div>";
                                        video += "<h3>Video Privado</h3>"
                                        video += '<a data-toggle="tooltip" data-placement="right"  title="Eliminar" style="background-color:#d12727;  "  onclick="BorrarVideo(' + Result.IMAGENES[i].idtnt_recursos + ')" class="btn btn-warning btn-sm">' +
                                                ' <span class="glyphicon glyphicon-trash"></span>' +
                                                '</a>';
                                        hayPrivado = "si";
                                    }

                                }

                            }

                            if (hayPublico == "no") {
                                var modalBtn = "<a  class='btn btn-sm btn-warning ' data-toggle='modal' data-target='#loadFileVideoPublico' data-toggle='tooltip' data-placement='right' title='Subir Video Público'><span class='glyphicon glyphicon-plus  glyphicon-film' aria-hidden='true'> -</span><span class='glyphicon glyphicon-eye-open' aria-hidden='true'> </span></a>";

                                $("#divVideoPublico").html(modalBtn);
                            }

                            if (hayPrivado == "no") {
                                var modalBtn = "<a  class='btn btn-sm btn-warning ' data-toggle='modal' data-target='#loadFileVideoPrivado' data-toggle='tooltip' data-placement='right' title='Subir Video Privado'><span class='glyphicon glyphicon-plus  glyphicon-film' aria-hidden='true'> -</span><span class='glyphicon glyphicon-eye-close' aria-hidden='true'> </span></a>";

                                $("#divVideoPrivado").html(modalBtn);
                            }



                            $("#videos").html(video);
                            $("#list-carrusel").html(list_carrusel);
                            $("#img-carrusel").html(img_carrusel);
                        }
                    },
                    error: function (xhr, status) {
                        alert('Se presentó un problema, intentelo nuevamente.');
                    }

                });
            }

            function cambiarEstadoImagen(state, id) {
                console.log("iddd" + id);
                var dataIn = {"estado": state, "id_recurso": id};
                $.post("Services/changeStateResource.php",
                        dataIn,
                        function (data) {
                            console.log(data);
                            if (data.STATUS === 'OK') {
                                if (state === 0) {
                                    alert("Foto oculta al publico");
                                }
                                if (state === 1) {
                                    alert("Foto visible al publico");
                                }

                                loadDataPerfil();
                            }

                        },
                        "json");


            }
            function BorrarImagen(id) {
                console.log(id);
                var dataIn = {"id_recurso": id};
                $.post("Services/DeleteResource.php",
                        dataIn,
                        function (data) {
                            console.log(data);
                            if (data.STATUS === 'OK') {


                                alert("Imagen eliminada correctamente");


                                loadDataPerfil();
                            } else {
                                alert("No se pudo eliminar la imagen");
                            }

                        },
                        "json");


            }

            function BorrarVideo(id) {
                console.log(id);
                var dataIn = {"id_recurso": id};
                $.post("Services/DeleteResource.php",
                        dataIn,
                        function (data) {
                            console.log(data);
                            if (data.STATUS === 'OK') {


                                alert("Video eliminado correctamente");
                                loadDataPerfil();
                                location.reload();

                                loadDataPerfil();
                            } else {
                                alert("No se pudo eliminar la imagen");
                            }

                        },
                        "json");


            }




            function loadGallery() {

                $.post("Services/loadGallery.php",
                        function (Result) {

                            if (Result.STATUS === 'OK') {
                                if (Result.DATA.length > 0) {
                                    var list_carrusel = "<li data-target=\"#myCarousel-change\" data-slide-to=\"0\" class=\"active\"></li>";
                                    var img_carrusel = "<div class=\"item active\"><img class=\"img-responsive\"  src=\"" + Result.DATA[0].url_almacenamiento + "\">";
                                    img_carrusel += " <div class = \"carousel-caption\">";
                                    img_carrusel += '<a data-toggle="tooltip" data-placement="right"  title="Mostrar"  onclick="changeImagePerfil(\'' + Result.DATA[0].url_almacenamiento + '\')" class="btn btn-warning btn-sm">' +
                                            ' <span class="glyphicon glyphicon-ok-sign"></span>' +
                                            '</a></div></div>';
                                    for (var i = 1; i < Result.DATA.length; i++) {

                                        list_carrusel += "<li data-target=\"#myCarousel-change\" data-slide-to=\"" + i + "\" ></li>";
                                        img_carrusel += "<div class=\"item\"><img class=\"img-responsive\" src=\"" + Result.DATA[i].url_almacenamiento + "\">";
                                        img_carrusel += " <div class = \"carousel-caption\">";
                                        img_carrusel += '<a data-toggle="tooltip" data-placement="right"  title="Mostrar"  onclick="changeImagePerfil(\'' + Result.DATA[i].url_almacenamiento + '\')" class="btn btn-warning btn-sm">' +
                                                ' <span class="glyphicon glyphicon-ok-sign"></span>' +
                                                '</a></div></div>';

                                    }

                                    $("#list-carrusel-change").html(list_carrusel);
                                    $("#img-carrusel-change").html(img_carrusel);
                                }
                            }
                        },
                        "json"
                        );
            }



            function changeImagePerfil(urlNew) {
                var dataIn = {"urlNewImage": urlNew};
                $.post("Services/changePerfilImage.php",
                        dataIn,
                        function (data) {

                            if (data.STATUS === 'OK') {
                                $("#foto-perfil").attr("src", "<?php echo $_SESSION['DATA']['url_foto_perfil']; ?>");
                                $("#changeImage").modal('hide');
                                window.location = "providerHome.php";
                            }
                        },
                        "json");

            }



        </script>
    </body>
</html> 


