<html>

    <head>
        <style>

            .dialog-header-puntuacion { background-color: #AE181F; }
            .dialog-header-puntuacion h4 { color: #ffffff; }

            #aspirantes{

                padding-top: 20px;

                background-color: #fff;
            }
            .gold{
                color:  #FFD700;
            }
            #aspirantes  thead th {
                background-color: #AE181F;
                color: white;
            }
            .table-responsive{
                margin-top: 30px;
            }

        </style>

    </head>    

    <body>
        <input type="hidden" id="idService" value="<?php echo $_GET['id_servicio']; ?>"/>
        <input type="hidden" id="estado" value="<?php echo $_GET['estado']; ?>"/>
        <div id="aspirantes" class="row">
            <div class="col-md-12">
                <a onclick="tramitados()"  class="btn btn-warning pull-left"><i class="fa fa-chevron-left"></i> Regresar</a>
                <h4 class="text-center">Aspirantes al servicio</h4>
                <div id="aspirantesData" class="table-responsive">

                </div>
            </div>
        </div>



        <div id="perfil" class="modal fade bs-example-modal-lg" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header dialog-header-puntuacion">
                        <a class="close" data-dismiss="modal">&times;</a>
                        <h4 class="modal-title">Perfil Proveedor</h4>
                    </div>
                    <div class="modal-body" id="modalCalificacion">
                        <div id ="alerta"></div>

                        <div id="loadPerfil"  >

                        </div>    

                    </div>
                    <div class="modal-footer">
                        <div class="col-md-6">
                            <a id="fotosOcultas" onclick="fotosOcultas()" style="display: none;"class="btn btn-warning" >Fotos Ocultas</a>  
                        </div>   
                        <div class="col-md-6">
                            <a id="aceptar" onclick="closeModal()"  style="display: none;"  class="btn btn-warning" >Aceptar</a>
                        </div>
                    </div>
                </div>
            </div>
        </div> 

        <div id="modalPay" class="modal fade bs-example-modal-lg" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header dialog-header-puntuacion">
                        <a class="close" data-dismiss="modal">&times;</a>
                        <h4 class="modal-title">Sección de pagos y privilegios!</h4>
                    </div>
                    <div class="modal-body" id="modalCalificacion">
                        <div id ="alerta"></div>

                        <div id="loadPagoChat"  >

                        </div>    

                    </div>
                    <div class="modal-footer">
                        <div class="col-md-6">

                        </div>   
                        <div class="col-md-6">
                            <a id="efectuarPago"  class="btn btn-warning" onclick="closePayModal();">Cancelar</a>
                        </div>
                    </div>
                </div>
            </div>
        </div> 

        <script type="text/javascript">
            var memberState;
            var payChat;
            var idProveedor;
            $(document).ready(function () {
                cargarAspirantes();
                loadStatusMembership();
                loadStatusChatPay();
                $('[data-toggle="tooltip"]').tooltip();

            });


            function  loadStatusChatPay() {
                var dataIn = {"idService": $("#idService").val()};

                $.post("Services/validateStatusChatPay.php",
                        dataIn,
                        function (data) {
                            payChat = 'NO'
                            if (data.STATUSPAY == 'OK') {
                                payChat = data.STATUSPAY;
                            }
                        },
                        "json");
            }

            function loadStatusMembership() {

                $.post("Services/validatemembership.php",
                        function (data) {
                            memberState = data.MEMBER;
                            if (memberState === 'INVALIDATE') {
                                $("#fotosOcultas").css("display", "block");
                                $("#aceptar").css("display", "block");
                            }
                            $("#aceptar").css("display", "block");
                        },
                        "json");
            }



            function cargarAspirantes() {

                var dataIn = {"id_servicio": $("#idService").val(), "estado": $("#estado").val()};
                $.post("Services/loadCandidates.php",
                        dataIn,
                        function (data) {

                            if (data.STATUS == 'OK') {

                                var tabla = '<table id="dataTable" class="table table-condensedtable-hover">' +
                                        '<thead>' +
                                        ' <tr>' +
                                        '<th>Nombres</th>' +
                                        '<th>Apellidos</th>' +
                                        '<th>Edad</th>' +
                                        '<th>Categoria</th>' +
                                        '<th>Puntuación</th>' +
                                        '<th>Accion</th>' +
                                        '</tr> ' +
                                        '</thead>' +
                                        '<tbody>';

                                if ($("#estado").val() === 'AT') {


                                    tabla += '<tr>' +
                                            '<td>' + data.DATA[0].nombres + '</td>' +
                                            '<td>' + data.DATA[0].apellidos + '</td>' +
                                            '<td>' + data.DATA[0].fecha_nacimiento + '</td>' +
                                            '<td>' + data.DATA[0].categoria + '</td>' +
                                            '<td>' + data.DATA[0].puntuacion + '</td>' +
                                            '<td>' +
                                            '<a data-toggle="modal" data-target="#perfil" data-toggle="tooltip" data-placement="right" title = "Pefil" onclick = "verPerfil(\'' + data.DATA[0].idtnt_proveedor + '\')" class = "btn btn-warning btn-sm" >' +
                                            '<span class = "glyphicon glyphicon-eye-open" > </span>' +
                                            ' </a>' +
                                            ' <a data - toggle = "tooltip" data - placement = "right"  title = "Chat" onclick = "chat(\'' + data.DATA[0].nick + '\',' + data.DATA[0].aspirante + ')" class = "btn btn-warning btn-sm" >' +
                                            '<span class = "fa fa-comment-o" > </span>' +
                                            ' </a>' +
                                            '</td>' +
                                            '</tr>';
                                } else {
                                    var cant = data.DATA.length;

                                    for (var i = 0; i < cant; i++) {
                                        tabla += '<tr>' +
                                                '<td>' + data.DATA[i].nombres + '</td>' +
                                                '<td>' + data.DATA[i].apellidos + '</td>' +
                                                '<td>' + data.DATA[i].fecha_nacimiento + '</td>' +
                                                '<td>' + data.DATA[i].categoria + '</td>' +
                                                '<td>' + data.DATA[i].puntuacion + '</td>' +
                                                '<td>' +
                                                '<a data-toggle="modal" data-target="#perfil" data-toggle="tooltip" data-placement="right" title = "Pefil" onclick = "verPerfil(\'' + data.DATA[i].idtnt_proveedor + '\')" class = "btn btn-warning btn-sm" >' +
                                                '<span class = "glyphicon glyphicon-eye-open" > </span>' +
                                                ' </a>' +
                                                ' <a data - toggle = "tooltip" data - placement = "right"  title = "Chat" onclick = "chat(\'' + data.DATA[i].nick + '\',' + data.DATA[i].aspirante + ')" class = "btn btn-warning btn-sm" >' +
                                                '<span class = "fa fa-comment-o" > </span>' +
                                                ' </a>' +
                                                '</td>' +
                                                '</tr>';
                                    }
                                }

                                tabla += ' </tbody></table>';
                                $('#aspirantesData').html(tabla);
                            }
                        },
                        "json");
            }
            
            
            function closePayModal() {
             $("#modalPay").modal('hide');
            }


            function fotosOcultas() {
                $("#modalPay").modal('show');
                $("#loadPagoChat").load("templates/payMembershipOrChat.php?id_service=" + $("#idService").val() + "&type_tran=VERPER&idProvider="+idProveedor);

            }


            function chat(nick, aspirante) {
                if (memberState === 'VALIDATE') {
                    $("#load").load("templates/chat.php?nick=" + nick + "&id_servicio=" + $("#idService").val() + "&estado=" + $("#estado").val() + "&aspirante=" + aspirante);
                } else {

                    

                    if (payChat === 'OK') {
                        $("#load").load("templates/chat.php?nick=" + nick + "&id_servicio=" + $("#idService").val() + "&estado=" + $("#estado").val() + "&aspirante=" + aspirante);
                    } else {
                      
                        $("#modalPay").modal('show');
                        $("#loadPagoChat").load("templates/payMembershipOrChat.php?id_service=" + $("#idService").val() + "&chat=SI&type_tran=SERTRAMEM");
                    }





                }
            }

            function tramitados() {
                $("#load").load("templates/tramitados.php");
            }

            function verPerfil(id_proveedor) {
                idProveedor = id_proveedor;
                $("#loadPerfil").load("templates/perfilLoad.php?id_userLoad=" + id_proveedor+"&memberShip="+memberState);


            }

            function closeModal() {
                $("#perfil").modal('hide');
            }

        </script>
    </body>
</html>
