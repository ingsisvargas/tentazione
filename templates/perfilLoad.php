<?php
session_start();

if (!isset($_SESSION['USER'])) {
    ?>
    <script type="text/javascript">
        window.location = "index.html";
    </script>
    <?php
}
?>
<html>
    <head>
        <style type="text/css">

            img.img-thumbnail{

                width: 210px;
                height: 210px;   
            }
            div.panel {
                font-size: 16px;
                height: 210px;
                width: 100%;
            }
            div.panel p label{

                margin-right: 10px;
                font-size: 14px;
            }
            .button{
                margin-bottom: 10px;

            }
            #info{
                margin-left: 15px;
                max-width: 550px;
            }
            .button > .pull-left {
                margin-right: 10px;
            }
            .carousel-inner{text-align: center;}
            .carousel-inner > .item {
                width:100%;
                height:440px;
            }
            .carousel-inner > .item > img {
                width:auto;
                display: inline-block;
                height: auto;
                max-height:440px;
            }

            .gold{
                color:  #FFD700;
            }
        </style>

    </head>
    <body>
        <input type="hidden" value="<?php echo $_GET['id_userLoad']; ?>" id="id_load"/>
        <div class = "row">

            <div class="col-md-12">

                <div class="row">
                    <div class="col-md-12"><center>
                            <img id="foto-perfil" src ="" class="img-thumbnail"/></center>
                    </div>
                </div>
                <div class="row">

                    <div  class="panel panel-default">
                        <div class="panel-heading text-center">Datos Ubicación</div>  
                        <div class="panel-body">
                            <p id="pais"></p>
                            <p id="region"></p>
                            <p id="ciudad"></p>

                            <div class="clearfix"></div>
                        </div>
                    </div> 

                </div>
                <div class="row">
                    <div class="panel panel-default">
                        <div class="panel-heading text-center">
                            Datos Usuario
                        </div> 
                        <div class="panel-body">
                            <p id="nombre"></p>
                            <p id="apellido"></p>
                            <p id="edad"></p>
                            <p id="puntuacion"> </p> 
                            <div class="clearfix"></div>
                        </div>  
                    </div>  
                </div> 


                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol id="list-carrusel" class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                        <li data-target="#myCarousel" data-slide-to="3"></li>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div id="img-carrusel" class="carousel-inner" role="listbox">

                    </div>

                    <!-- Left and right controls -->
                    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
                <div class="col-sm-12" id="videos">


                </div> 
            </div>  
        </div>

        <input type="hidden" id="tipo" value="<?php echo $_SESSION['DATA']['tipo']; ?>"/>
        <?php if (isset($_GET['memberShip'])) { ?>
            <input type="hidden" id="memberShip" value="<?php echo $_GET['memberShip']; ?>"/>
        <?php } ?>

        <script type="text/javascript">
            $(document).ready(function () {
                var dataIn = {"id_load": $("#id_load").val()};
                $.post('Services/loadPerfil.php',
                        dataIn,
                        function (json) {

                            var Result = json;
                            var tipo = $("#tipo").val();

                            var puntuacion = "<label >Puntuación:</label>";
                            var numero = parseInt(Result.puntuacion);
                            $("#foto-perfil").attr("src", Result.url_foto_perfil);
                            $("#pais").html("<label>Pais:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>" + Result.pais);
                            $("#region").html("<label>Región:</label>" + Result.region);
                            $("#ciudad").html("<label>Ciudad:</label>" + Result.ciudad);
                            $("#nombre").html("<label>Nombre:</label>" + Result.nombres);
                            $("#apellido").html("<label>Apellido:</label>" + Result.apellidos);
                            $("#edad").html("<label>Edad:</label>" + Result.fecha_nacimiento);

                            for (var i = 0; i < numero; i++) {
                                puntuacion += "<span class=\"glyphicon glyphicon-star gold\" aria-hidden=\"true\"></span>";
                            }
                            $("#puntuacion").html(puntuacion);

                            alert($("#memberShip").val());

                            if (Result.IMAGENES.length > 0) {
                                console.log("paso");
                                console.log(tipo);
                                if (tipo === '2') {

                                    var list_carrusel = "";
                                    var img_carrusel = "";
                                    var activo = false;
                                    var video = "";
                                    video += "<div class='embed-responsive embed-responsive-16by9'>";
                                    for (var i = 0; i < Result.IMAGENES.length; i++)
                                    {
                                        if ($("#memberShip").val() === 'VALIDATE') {

                                            if (Result.IMAGENES[i].video == "si") {

                                                video += "<video  controls>";
                                                video += "<source src='" + Result.IMAGENES[i].url_almacenamiento + "' type='video/mp4'>";
                                                video += " </video>";
                                                video += " </div>";

                                                $("#videos").html(video);
                                                console.log(Result.IMAGENES[i].url_almacenamiento);


                                            } else {
                                                list_carrusel += "<li data-target=\"#myCarousel\" data-slide-to=\"" + i + "\" ></li>";
                                                if (!activo) {
                                                    activo = true;
                                                    img_carrusel += "<div class=\"item active\"><img class=\"img-responsive\" src=\"" + Result.IMAGENES[i].url_almacenamiento
                                                            + "\"></div>";
                                                } else {
                                                    img_carrusel += "<div class=\"item\"><img class=\"img-responsive\" src=\"" + Result.IMAGENES[i].url_almacenamiento
                                                            + "\"></div>";
                                                }
                                            }
                                        } else {

                                            if (Result.IMAGENES[i].visisbilidad === "1" && Result.IMAGENES[i].video == "no") {

                                                list_carrusel += "<li data-target=\"#myCarousel\" data-slide-to=\"" + i + "\" ></li>";
                                                if (!activo) {
                                                    activo = true;
                                                    img_carrusel += "<div class=\"item active\"><img class=\"img-responsive\" src=\"" + Result.IMAGENES[i].url_almacenamiento
                                                            + "\"></div>";
                                                } else {
                                                    img_carrusel += "<div class=\"item\"><img class=\"img-responsive\" src=\"" + Result.IMAGENES[i].url_almacenamiento
                                                            + "\"></div>";
                                                }
                                            } else if (Result.IMAGENES[i].video == "si") {
                                                if (Result.IMAGENES[i].visisbilidad == "1") {
                                                    video += "<video  controls>";
                                                    video += "<source src='" + Result.IMAGENES[i].url_almacenamiento + "' type='video/mp4'>";
                                                    video += " </video>";
                                                    video += " </div>";

                                                    $("#videos").html(video);
                                                    console.log(Result.IMAGENES[i].url_almacenamiento);

                                                }
                                            }


                                        }
                                    }

                                    $("#list-carrusel").html(list_carrusel);
                                    $("#img-carrusel").html(img_carrusel);
                                }
                                if (tipo === '3') {
                                    var list_carrusel = "<li data-target=\"#myCarousel\" data-slide-to=\"0\" class=\"active\"></li>";
                                    var img_carrusel = "<div class=\"item active\"><img class=\"img-responsive\"  src=\"" + Result.IMAGENES[0].url_almacenamiento
                                            + "\"></div>";
                                    for (var i = 1; i < Result.IMAGENES.length; i++)
                                    {
                                        list_carrusel += "<li data-target=\"#myCarousel\" data-slide-to=\"" + i + "\" ></li>";
                                        img_carrusel += "<div class=\"item\"><img class=\"img-responsive\" src=\"" + Result.IMAGENES[i].url_almacenamiento
                                                + "\"></div>";
                                    }
                                    $("#list-carrusel").html(list_carrusel);
                                    $("#img-carrusel").html(img_carrusel);

                                }
                            }
                        },
                        "json");

            });

        </script>
    </body>
</html>
