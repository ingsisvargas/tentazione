<?php
session_start();
?>
<html>
    <head>
        <style type="text/css">

            .dialog-header-puntuacion { background-color: #AE181F; }
            .dialog-header-puntuacion h4 { color: #ffffff; }

            img { height: 210px; max-width: 100%};
            .button-service {
                margin-top: 10px;
                margin-left: 20px;
                margin-right: 20px;
            }
            .video{
                margin-right: 20px;
                margin-left: 20px;
                margin-top: 10px;

            }
            .video>img{
                width: 90%;
                height: 360px;

            }

            .video>video{
                width: 90%;
                height: 360px;

            }
            .bg-btn{
                padding: 5px 20px;
                font-size: 20px;
                border-radius: 6px;
                -moz-border-radius: 6px;
                -webkit-border-radius: 6px;
            }
            #panel-chat{

                margin-top: 54px;
            }
            #btn-input{
                resize: none;
            }
        </style>
    </head>
    <body>
        <div class="row">
            <div class="col-md-5">
                <div class="row">
                    <?php IF ($_SESSION['DATA']['tipo'] == 2) { ?>
                        <a class="btn btn-warning button-service" onclick="aspirantes()">
                            <i class="fa fa-chevron-left">
                            </i>
                            Regresar
                        </a>
                        <?php IF ($_GET['estado'] != 'AT') { ?>
                            <a class="btn btn-warning button-service" onclick="confirmar()">
                                <i class="fa fa-chevron-right">
                                </i>
                                Tomar Propuesta
                            </a>
                        <?php } ?>
                    <?php } ?>
                    <div class="video" id="video-box">

                    </div>
                    <div class="row button-service">
                        <div class="col-md-5">
                            <a class="btn btn-success bg-btn pull-right" id="makeDial" disabled="true" onclick="makeCall();" >
                                <i class="glyphicon glyphicon-earphone">
                                </i>
                            </a>
                            <div class="clearfix">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <a class="btn btn-danger bg-btn pull-left" id="endDial" disabled="true" onclick="endCall();">
                                <i class="fa fa-ban">
                                </i>
                            </a>
                            <div class="clearfix">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="panel panel-default" id="panel-chat">
                        <div class="panel-heading">
                            <i class="fa fa-comment">
                            </i>
                            Chat
                        </div>
                        <div class="panel-body">
                            <div id="chatScroll">
                                <ul class="chat" id="chat-body">
                                </ul>                               
                            </div>
                        </div>
                        <div class="panel-footer">
                            <textarea class="form-control input-sm"  id="tta-message" placeholder="Escriba aqui"/>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-5">
                        <button class="btn btn-warning pull-right " id="btn-Send">
                            Enviar
                        </button>
                        <div class="clearfix">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <button class="btn btn-warning pull-left" id="btn-clean">
                            Limpiar
                        </button>
                        <div class="clearfix">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <input id="user_need" type="hidden" value="<?php echo $_GET['nick']; ?>"/>        
        <input id="estado" type="hidden" value="<?php echo $_GET['estado']; ?>"/>
        <input id="idaspirante" type="hidden" value="<?php echo $_GET['aspirante']; ?>"/>
        <input id="idService" type="hidden" value="<?php echo $_GET['id_servicio']; ?>"/>


        <div id="modalPay" class="modal fade bs-example-modal-lg" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header dialog-header-puntuacion">
                        <a class="close" data-dismiss="modal">&times;</a>
                        <h4 class="modal-title">Pago por el servicio!</h4>
                    </div>
                    <div class="modal-body" id="modalCalificacion">
                        <div id ="alerta"></div>

                        <div id="loadPagoChat"  >

                        </div>    

                    </div>
                    <div class="modal-footer">
                        <div class="col-md-6">

                        </div>   
                        <div class="col-md-6">
                            <a id="efectuarPago"  class="btn btn-warning" onclick="closePayModal();">Cancelar</a>
                        </div>
                    </div>
                </div>
            </div>
        </div> 

        <script>

            var statusMember;
            $(document).ready(function () {
                killerSession();
                loadPerfilImages();
                loadStatusMembership();
            });


            function loadStatusMembership() {

                $.post("Services/validatemembership.php",
                        function (data) {
                            statusMember = data.MEMBER;
                        },
                        "json");
            }

            function closePayModal() {
                $("#modalPay").modal('hide');
            }



            function loadPerfilImages() {

                var dataIn = {"user_need": $("#user_need").val()};
                $.post("Services/loadImagesPerfil.php",
                        dataIn,
                        function (data) {
                            if (data.STATUS === 'OK') {
                                $("#video-box").html("<img  src='" + data.img_user_need + "' /><br/><br/>");
                            }

                        }, "json");
            }


            function killerSession() {
                setTimeout("window.open('../Services/sendMessages.php','_top');", 60000);
            }
        </script>
        <script type="text/javascript">
            var idRoom = $("#idaspirante").val();
            var conexion;
            var state = $("#estado").val();
            var refRoom = connect.ref("rooms").child(idRoom);
            var refMessages = refRoom.child("messages");
            refRoom.child('stateService').set(state);
            function stateButtonCall(data) {
                var user = data.val();
                if (user.status !== true) {
                    $('#makeDial').attr("disabled");
                    $('#endDial').attr("disabled");
                } else {
                    $('#makeDial').removeAttr("disabled");
                    $('#endDial').removeAttr("disabled");
                }
            }
            function sendSignal() {
                user_chat = "<?php echo $_SESSION['USER'] ?>";
                user_need = document.getElementById('user_need').value;
                startSocketConection();
            }
            function stateConnection() {
                if (user.typeUser === '2') {
                    refRoom.child('customer').on('value', stateButtonCall);
                } else {
                    refRoom.child('Provider').on('value', stateButtonCall);
                }
            }



            function loadMessages(data) {
                var message = data.val();
                var burbler =
                        '<li class = "rigth clearfix">' +
                        '<div class = "chat-body clearfix">' +
                        '<div class = "header">' +
                        '<strong class = "primary-font">' + message.nickname + '</strong>' +
                        '<small class = "pull-right text-muted"> <i class ="fa fa-clock-o"> </i>' + message.date + '</small>' +
                        '</div>' +
                        '<p>' + message.message +
                        '</p>' +
                        '</div>  </li>';
                console.log(user.typeUser === 3);
                console.log(user.typeUser === '3');
                console.log(user.typeUser == "3");
                console.log(user.typeUser === "3");
                if (user.typeUser === '3') {
                    refRoom.child('messageCustomer').set(false);
                } else {
                    refRoom.child('messageProvider').set(false);
                }
                $("#chat-body").append(burbler);
                $('#chatScroll').slimScroll({
                    start: "bottom",
                    height: '220px'
                });
            }
            function sendMessage() {
                var message = $("#tta-message").val();
                var dateMessage = moment().format('LTS');
                refMessages.push({
                    nickname: user.nickname,
                    message: message,
                    date: dateMessage
                });
                cleanMessage();
                $('#chatScroll').slimScroll({
                    start: "bottom",
                    height: '220px'
                });
                if (user.typeUser === '2') {
                    refRoom.child('messageProvider').set(true);
                } else {
                    refRoom.child('messageCustomer').set(true);
                }
            }

            function connectedService() {
                if (user.typeUser === '3') {
                    refRoom.child('provider').child('connected').set(true);
                    refRoom.child('customer').on('value', function (data) {
                        var customer = data.val();
                        console.log(customer);
                        if (customer.connected)
                        {
                            $('#makeDial').removeAttr("disabled");
                            $('#endDial').removeAttr("disabled");
                        } else
                        {
                            $('#makeDial').attr("disabled");
                            $('#endDial').attr("disabled");
                        }
                    }
                    );
                } else
                {
                    refRoom.child('customer').child('connected').set(true);
                    refRoom.child('provider').on('value', function (data) {
                        var provider = data.val();
                        console.log(provider);
                        if (provider.connected)
                        {

                            $('#makeDial').removeAttr("disabled");
                            $('#endDial').removeAttr("disabled");
                        } else
                        {

                            $('#makeDial').attr("disabled");
                            $('#endDial').attr("disabled");
                        }
                    }
                    );
                }
            }

            function loginPubNub(user) {
                if (phone == null) {
                    phone = window.phone = PHONE({
                        number: user || "Anonymous", // listen on username line else Anonymous
                        publish_key: 'pub-c-37d12d2d-9c14-4eaf-b5ff-4b8e3ffc0c3a', // Your Pub Key
                        subscribe_key: 'sub-c-94c3ce42-8e89-11e6-8409-0619f8945a4f', // Your Sub Key
                        media: {audio: true, video: true},
                        ssl: true,
                        uuid: user
                    });
                }

                phone.ready(function () {
                    console.log("Conectado");
                });
                phone.receive(function (session) {
                    var video_out = document.getElementById("video-box");
                    session.connected(function (session) {
                        conexion = session;
                        if (video_out == null) {
                            return;
                        }
                        video_out.innerHTML = '';
                        video_out.appendChild(session.video);
                    });
                    session.ended(function (session) {
                        conexion = session;
                        video_out.innerHTML = '';
                        loadPerfilImages();
                    });
                });
                return false;
            }

            function makeCall(form) {


                if (!window.phone)
                    alert("Login First!");
                else {


                    conexion = phone.dial($("#user_need").val());
                }
                return false;
            }


            function errWrap(fxn, form) {
                try {
                    return fxn(form);
                } catch (err) {
                    alert("WebRTC is currently only supported by Chrome, Opera, and Firefox");
                    return false;
                }
            }

            function endCall() {
                conexion.hangup();
                loadPerfilImages();
            }

            function confirmar() {
               
                var dataIn = {"nick": $("#user_need").val(), "id_service": $("#idService").val()};
                $.post("Services/confirmServices.php",
                        dataIn,
                        function (data) {
                            if (data.STATUS === 'OK') {
                                alert("Tienes una cita confirmada... ¡Procede a cancelar el servicio!");
                                $("#modalPay").modal('show');
                                $("#loadPagoChat").load("templates/payService.php?id_service=" + $("#idService").val() + "&type_tran=ASPSER");
                            } else {
                                alert("Algo salio mal...");
                            }
                        },
                        "json"
                        );
            }



            function aspirantes() {
                if (phone != null) {
                    phone.hangup();
                    if (phone.mystream != null) {
                        phone.mystream.getVideoTracks()[0].stop();
                    }
                    if (phone.mystream != null) {
                        phone.mystream.getAudioTracks()[0].stop();
                    }
                    phone.pubnub.unsubscribe({
                        channel: "<?php echo $_SESSION['USER']; ?>"
                    });
                    phone = null;
                }

                $("#load").load("templates/aspirantes.php?id_servicio=" + $("#idService").val() + "&estado=" + $("#estado").val());
            }

            function  cleanMessage() {
                $("#tta-message").val('');
            }
            function  initializeChat() {
                refMessages.on('child_added', loadMessages);
                connectedService();
                $("#btn-clean").click(cleanMessage);
                $("#btn-Send").click(sendMessage);
            }


            $(document).ready(function () {
                loginPubNub("<?php echo $_SESSION['USER'] ?>");
                initializeChat();
            });


        </script>

       <?php/*
//iniciamos la sesión
        session_name("loginUsuario");

//antes de hacer los cálculos, compruebo que el usuario está logueado
//utilizamos el mismo script que antes
        if ($_SESSION["autentificado"] != "SI") {
            //si no está logueado lo envío a la página de autentificación
            header("Location: index.php");
        } else {
            //sino, calculamos el tiempo transcurrido
            $fechaGuardada = $_SESSION["ultimoAcceso"];
            $ahora = time();
            $tiempo_transcurrido = $ahora - $fechaGuardada;

            //comparamos el tiempo transcurrido
            if ($tiempo_transcurrido >= 600000) {
                //si pasaron 10 minutos o más
            } else {
                $_SESSION["ultimoAcceso"] = $ahora;
            }
        }*/
        ?>

    </body>
</html>
