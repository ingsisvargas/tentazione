<html>

    <head>
        <!-- Special version of Bootstrap that is isolated to content wrapped in .bootstrap-iso 
        <link rel="stylesheet" href="https://formden.com/static/cdn/bootstrap-iso.css" />-->

        <!--Font Awesome (added because you use icons in your prepend/append)
        <link rel="stylesheet" href="https://formden.com/static/cdn/font-awesome/4.4.0/css/font-awesome.min.css" />-->

        <!-- Inline CSS based on choices in "Settings" tab -->
        <style>.bootstrap-iso .formden_header h2, .bootstrap-iso .formden_header p, .bootstrap-iso form{font-family: Arial, Helvetica, sans-serif; color: black}.bootstrap-iso form button, .bootstrap-iso form button:hover{color: white !important;} .asteriskField{color: red;}</style>


        <!--Para el calendario

        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>-->


        <style>


            .stepwizard-step p {
                margin-top: 10px;
            }
            .stepwizard-row {
                display: table-row;
            }
            .stepwizard {
                display: table;
                width: 50%;
                position: relative;
            }
            .stepwizard-step button[disabled] {
                opacity: 1 !important;
                filter: alpha(opacity=100) !important;
            }
            .stepwizard-row:before {
                top: 14px;
                bottom: 0;
                position: absolute;
                content: " ";
                width: 100%;
                height: 1px;
                background-color: #ccc;
                z-order: 0;
            }
            .stepwizard-step {
                display: table-cell;
                text-align: center;
                position: relative;
            }
            .btn-circle {
                width: 30px;
                height: 30px;
                text-align: center;
                padding: 6px 0;
                font-size: 12px;
                line-height: 1.428571429;
                border-radius: 15px;
            }

            #nuevoService{

                padding-top: 20px;

                background-color: #fff;
            }
            .gold{
                color:  #FFD700;
            }

            .table-responsive{
                margin-top: 30px;
            }

        </style>

    </head>    

    <body>

        <div id="nuevoService">
            <div  class="row">
                <div class="col-md-12">

                    <h4 class="text-center">Planeando Cita</h4>
                </div>
            </div>

            <div class="container">

                <div class="stepwizard col-md-offset-3">
                    <div class="stepwizard-row setup-panel">
                        <div class="stepwizard-step">
                            <a href="#step-1" type="button" class="btn btn-primary btn-circle">1</a>
                            <p>Plan</p>
                        </div>
                        <div class="stepwizard-step">
                            <a href="#step-2" type="button" class="btn btn-default btn-circle" disabled="disabled">2</a>
                            <p>Dia</p>
                        </div>
                        <div class="stepwizard-step">
                            <a href="#step-3" type="button" class="btn btn-default btn-circle" disabled="disabled">3</a>
                            <p>Tiempos</p>
                        </div>
                        <div class="stepwizard-step">
                            <a href="#step-4" type="button" class="btn btn-default btn-circle" disabled="disabled">4</a>
                            <p>Detalle Final</p>
                        </div>
                    </div>
                </div>

                <form role="form" action="" method="post">
                    <div class="row setup-content" id="step-1">
                        <div class="col-xs-6 col-md-offset-3">
                            <div class="col-md-12">

                                <div class="form-group">
                                    <label class="control-label">Plan: </label>
                                    <input id="plan" required="required" type="text" class="form-control input-sm" placeholder="¿Que Propones?">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Descripción: </label>
                                    <textarea id="descripcion" required="required"  class="form-control input-lg" placeholder="Descripción del plan"></textarea>
                                </div>
                                <button class="btn  btn-primary nextBtn btn-lg pull-right" type="button" >2</button>
                            </div>
                        </div>
                    </div>
                    <div class="row setup-content" id="step-2">
                        <div class="col-xs-6 col-md-offset-3">
                            <div class="col-md-12">


                                <div class="bootstrap-iso">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6 col-xs-12">

                                                <div class="form-group ">
                                                    <label class="control-label col-sm-2 requiredField" for="fecha_servicio">
                                                        Fecha: 
                                                    </label> &nbsp;
                                                    <div class="col-sm-10">
                                                        <div class="input-group">
                                                            <div class="input-group-addon">
                                                                <i class="fa fa-calendar">
                                                                </i>
                                                            </div>
                                                            <input class="form-control" id="fecha_servicio" name="date" placeholder="MM/DD/YYYY" type="text"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-sm-10 col-sm-offset-2">
                                                        <input name="_honey" style="display:none" type="text"/>

                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <button class="btn  btn-primary nextBtn btn-lg pull-right" type="button" >3</button>
                            </div>
                        </div>
                    </div>
                    <div class="row setup-content" id="step-3">
                        <div class="col-xs-6 col-md-offset-3">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="sr-only">Hora inicial: </label>
                                    <input id="hora_inicio" required="required" type="text"    class="form-control input-sm" placeholder="inicia">
                                </div> 
                                <div class="form-group">
                                    <label class="sr-only">Hora final: </label>
                                    <input id="hora_fin" required="required"  type="text"    class="form-control input-sm" placeholder="Termina">
                                </div> 

                                <button class="btn  btn-primary nextBtn btn-lg pull-right" type="button" >4</button>
                            </div>
                        </div>
                    </div>
                    <div class="row setup-content" id="step-4">
                        <div class="col-xs-6 col-md-offset-3">
                            <div class="col-md-12">

                                <div class="form-group">
                                    <label class="sr-only">Catégoria: </label>
                                    <select required="required" id="categoria" class="form-control input-sm"> 
                                        <option value="">-- Seleccione --</option>
                                        <option value="A">A</option>
                                        <option value="AA">AA</option>
                                        <option value="AAA">AAA</option>
                                    </select>
                                </div> 
                                <button type="reset" onclick="loadServices();" class="btn btn-sm btn-primary"> cancelar</button>
                                <button type="button" onclick="registrarServicio();" class="btn btn-sm btn-primary"> Registrar</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>



        <script type="text/javascript">



            $(document).ready(function () {

                var navListItems = $('div.setup-panel div a'),
                        allWells = $('.setup-content'),
                        allNextBtn = $('.nextBtn');

                allWells.hide();

                navListItems.click(function (e) {
                    e.preventDefault();
                    var $target = $($(this).attr('href')),
                            $item = $(this);

                    if (!$item.hasClass('disabled')) {
                        navListItems.removeClass('btn-primary').addClass('btn-default');
                        $item.addClass('btn-primary');
                        allWells.hide();
                        $target.show();
                        $target.find('input:eq(0)').focus();
                    }
                });

                allNextBtn.click(function () {
                    var curStep = $(this).closest(".setup-content"),
                            curStepBtn = curStep.attr("id"),
                            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
                            curInputs = curStep.find("input[type='text'],input[type='url']"),
                            isValid = true;

                    $(".form-group").removeClass("has-error");
                    for (var i = 0; i < curInputs.length; i++) {
                        if (!curInputs[i].validity.valid) {
                            isValid = false;
                            $(curInputs[i]).closest(".form-group").addClass("has-error");
                        }
                    }

                    if (isValid)
                        nextStepWizard.removeAttr('disabled').trigger('click');
                });

                $('div.setup-panel div a.btn-primary').trigger('click');
                readyDate();

            });

            function readyDate() {


                var date_input = $('input[name="date"]'); //our date input has the name "date"
                var container = $('.bootstrap-iso form').length > 0 ? $('.bootstrap-iso form').parent() : "body";
                date_input.datepicker({
                    format: 'mm/dd/yyyy',
                    container: container,
                    todayHighlight: true,
                    autoclose: true
                });

            }
            function registrarServicio() {

                var dataIn = {"fecha_servicio": $("#fecha_servicio").val(),
                    "plan": $("#plan").val(),
                    "descripcion": $("#descripcion").val(),
                    "hora_inicio": ($("#hora_inicio").val().replace(" am", "")).replace(" pm", ""),
                    "hora_fin": ($("#hora_fin").val().replace(" am", "")).replace(" pm", ""),
                    "categoria": $("#categoria").val()
                };

                $.post("Services/registerService.php",
                        dataIn,
                        function (data) {
                            if (data.STATUS === 'OK') {
                                alert("Servicio registrado... Revise su seguimiento.");
                                document.getElementById("formRegisterService").reset();

                                $("#load").load("templates/tramitados.php");
                                $('#tittleLoad').html('<h3>Servicios</h3>');

                            } else {
                                alert("Hubo un error.");
                            }
                        },
                        "json"
                        );
            }

            $("#hora_inicio").timeDropper({
                // custom time format
                format: 'h:mm a',
                // auto changes hour-minute or minute-hour on mouseup/touchend.
                autoswitch: false,
                // sets time in 12-hour clock in which the 24 hours of the day are divided into two periods. 
                meridians: false,
                // enable mouse wheel
                mousewheel: false,
                // auto set current time
                setCurrentTime: true,
                // fadeIn(default), dropDown
                init_animation: "fadein",
                // custom CSS styles
                primaryColor: "#1977CC",
                borderColor: "#1977CC",
                backgroundColor: "#FFF",
                textColor: '#555'

            });

            $("#hora_fin").timeDropper({
                // custom time format
                format: 'h:mm a',
                // auto changes hour-minute or minute-hour on mouseup/touchend.
                autoswitch: false,
                // sets time in 12-hour clock in which the 24 hours of the day are divided into two periods. 
                meridians: false,
                // enable mouse wheel
                mousewheel: false,
                // auto set current time
                setCurrentTime: true,
                // fadeIn(default), dropDown
                init_animation: "fadein",
                // custom CSS styles
                primaryColor: "#1977CC",
                borderColor: "#1977CC",
                backgroundColor: "#FFF",
                textColor: '#555'

            });





        </script>
    </body>
</html>
