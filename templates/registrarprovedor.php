<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Registro de cliente</title>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <!-- fontawaesome -->
    <script src="https://use.fontawesome.com/835980aa1c.js"></script>

    <!--Estilos CSS-->
    <link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500">

</head>
<body>

<div class="container">
    <div class="row">

        <div class="wrapper">
            <div class="container">
                <div class="row">
                    <?php
                    if(isset($_GET['create'])){
                        if($_GET['create'] == "bad"){
                            echo "<div class=\"alert alert-danger\">
                                <strong>Error!</strong> Verifica que todos tus campos esten bien 
                                </div>";
                        }
                    }
                    ?>
                    <!--
                    Para poder mostrar o solicitar información debo llamar
                    a la clase panel que es la que me permite visualizar
                    las etiquetas HTML que necesite. Esta es una de las clases de bootstrap
                    -->
                    <div class="col-md-5">
                        <div class="panel panel-danger">

                            <div class="panel-heading">
                                <h3 class="panel-title">Datos usuario</h3>
                            </div>

                            <div class="panel-body">
                                <form action="../Services/registrarProveedor.php" method="post">
                                    <div class="form-group">
                                        <label for="nick">Nick</label>
                                        <div class="input-group margin-bottom-sm">
                                            <span class="input-group-addon"><i class="fa fa-user-o" aria-hidden="true"></i></span>
                                            <input class="form-control" type="text" id="nick" name="nick" placeholder="digita tu nick" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="contraseña">Contraseña</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-key fa-fw"></i></span>
                                            <input class="form-control" type="password" id="contraseña" name="contraseña" placeholder="minimo 4 caracteres" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="rcontraseña">Confirma tu contraseña</label>
                                        <div class="input-group">
                                            <span class="input-group-addon"><i class="fa fa-key fa-fw"></i></span>
                                            <input class="form-control" type="password" id="rcontraseña" name="rcontraseña" placeholder="minimo 4 caracteres" required>
                                        </div>
                                    </div>

                            </div>

                        </div>

                    </div>

                    <div class="col-md-7">

                        <div class="panel panel-danger">
                            <div class="panel-heading clearfix">
                                <h3 class="panel-title pull-left">Informacion del proveedor</h3>
                            </div>
                            <div class="panel-body">

                                <div class="form-group">
                                    <label for="nombre">Nombres</label>
                                    <div class="input-group margin-bottom-sm">
                                        <span class="input-group-addon"><i class="fa fa-user-o" aria-hidden="true"></i></span>
                                        <input class="form-control" type="text" id="nombre" name="nombre" placeholder="digita tu nombre" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="apellido">apellido</label>
                                    <div class="input-group margin-bottom-sm">
                                        <span class="input-group-addon"><i class="fa fa-user-o" aria-hidden="true"></i></span>
                                        <input class="form-control" type="text" id="apellido" name="apellido" placeholder="digita tu apellido" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="nro_celular">Numero de celular</label>
                                    <div class="input-group margin-bottom-sm">
                                        <span class="input-group-addon"><i class="fa fa-mobile" aria-hidden="true"></i></span>
                                        <input class="form-control" type="text" id="nro_celular" name="nro_celular" placeholder="digita tu numero de celular" maxlength="11" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="fecha_nacimiento">Fecha de nacimiento</label>
                                    <div class="input-group margin-bottom-sm">
                                        <span class="input-group-addon"><i class="fa fa-mobile" aria-hidden="true"></i></span>
                                        <input class="form-control" type="date" id="fecha_nacimiento" name="fecha_nacimiento" placeholder="dd/mm/aaaa" maxlength="11" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="ubicacion">Ciudad en donde estas </label>
                                    <div class="input-group margin-bottom-sm" id="locationField">
                                        <span class="input-group-addon"><i class="fa fa-map-o" aria-hidden="true"></i></span>
                                        <input class="form-control" type="text" id="autocomplete" placeholder="Ejemplo :bogota" onFocus="geolocate()" type="text" required>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="email">E-mail</label>
                                    <div class="input-group margin-bottom-sm">
                                        <span class="input-group-addon"><i class="fa fa-envelope-o fa-fw"></i></span>
                                        <input class="form-control" type="email" id="email" name="email" placeholder="Ejemplo : tuemail@gmail.com" required>
                                    </div>
                                </div>

                            </div>
                        </div>

                        <button type="submit" class="btn btn-lg btn-warning" ><i class="fa fa-plus" aria-hidden="true"></i> Crear cuenta</button>
                        <button type="reset" class="btn btn-lg btn-warning"><i class="fa fa-trash-o" aria-hidden="true"></i> Limpiar</button>
                        <a href="../index.php"><button type="button"class="btn btn-lg btn-warning"><i class="fa fa-undo" aria-hidden="true"></i> Regresar</button></a>

                    </div>

                    <div id="datos" style="display: none;">
                        <table id="address">
                            <tr>
                                <td class="label">Street address</td>
                                <td class="slimField"><input class="field" id="street_number" disabled="true"></td>
                                <td class="wideField" colspan="2"><input class="field" id="route"  disabled="true"></td>
                            </tr>
                            <tr>
                                <td class="label">City</td>
                                <td class="wideField" colspan="3"><input class="field" id="locality" name="ciudad"  disabled="true"></td>
                            </tr>
                            <tr>
                                <td class="label">State</td>
                                <td class="slimField"><input class="field" id="administrative_area_level_1" name="region" disabled="true"></td>
                                <td class="label">Zip code</td>
                                <td class="wideField"><input class="field" id="postal_code" disabled="true"></td>
                            </tr>
                            <tr>
                                <td class="label">Country</td>
                                <td class="wideField" colspan="3"><input class="field" id="country" name="pais" disabled="true"></td>
                            </tr>
                        </table>

                        <script>
                            // This example displays an address form, using the autocomplete feature
                            // of the Google Places API to help users fill in the information.

                            var placeSearch, autocomplete;
                            var componentForm = {
                                street_number: 'short_name',
                                route: 'long_name',
                                locality: 'long_name',
                                administrative_area_level_1: 'short_name',
                                country: 'long_name',
                                postal_code: 'short_name'
                            };

                            function initAutocomplete() {
                                // Create the autocomplete object, restricting the search to geographical
                                // location types.
                                autocomplete = new google.maps.places.Autocomplete(
                                    /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
                                    {types: ['geocode']});

                                // When the user selects an address from the dropdown, populate the address
                                // fields in the form.
                                autocomplete.addListener('place_changed', fillInAddress);
                            }

                            // [START region_fillform]
                            function fillInAddress() {
                                // Get the place details from the autocomplete object.
                                var place = autocomplete.getPlace();

                                for (var component in componentForm) {
                                    document.getElementById(component).value = '';
                                    document.getElementById(component).disabled = false;
                                }

                                // Get each component of the address from the place details
                                // and fill the corresponding field on the form.
                                for (var i = 0; i < place.address_components.length; i++) {
                                    var addressType = place.address_components[i].types[0];
                                    if (componentForm[addressType]) {
                                        var val = place.address_components[i][componentForm[addressType]];
                                        document.getElementById(addressType).value = val;
                                    }
                                }
                            }
                            // [END region_fillform]

                            // [START region_geolocation]
                            // Bias the autocomplete object to the user's geographical location,
                            // as supplied by the browser's 'navigator.geolocation' object.
                            function geolocate() {
                                if (navigator.geolocation) {
                                    navigator.geolocation.getCurrentPosition(function(position) {
                                        var geolocation = {
                                            lat: position.coords.latitude,
                                            lng: position.coords.longitude
                                        };
                                        var circle = new google.maps.Circle({
                                            center: geolocation,
                                            radius: position.coords.accuracy
                                        });
                                        autocomplete.setBounds(circle.getBounds());
                                    });
                                }
                            }
                            // [END region_geolocation]

                        </script>
                        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCSMxQ62XqiSmhe5vr9XoYKKUMk2IMkOjw&libraries=places&callback=initAutocomplete"
                                async defer></script>
                    </div>

                    </form>
                </div>
            </div>
        </div>

    </div>
</div>
</body>
</html>