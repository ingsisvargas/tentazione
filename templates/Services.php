<html>
    <head>


    </head>

    <body>
        <div class="panel-tab">	
            <ul class="nav nav-tabs nav-justified">
                <li class="active" ><a onclick="newService()" data-toggle="tab" href="#NuevoServicio">Nuevo servicio</a></li>
                <li><a onclick="Show()" data-toggle="tab" href="#Servicio">Seguimiento</a></li>
            </ul>
        </div>                 

        <div class="tab-content">
            <div id="NuevoServicio" class="tab-pane fade in active">
                
            </div>
            <div id="Servicio" class="tab-pane fade">

            </div>
        </div>
        <script type="text/javascript">

            $(document).ready(function(){
                newService();
            });

            function Show() {
                $('#Servicio').show();
                $("#NuevoServicio").html("");
                $("#Servicio").load("templates/tramitados.php");
            }
            
            function newService(){
                $("#NuevoServicio").show();
                $('#Servicio').html("");
                $("#NuevoServicio").load("templates/nuevoServicio.php");
                
            }

        </script>

    </body>

</html>