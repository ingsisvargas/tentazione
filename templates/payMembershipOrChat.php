<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <style>

        form {
            margin: 40px 0;
        }




        .labelB {
            width: 200px;
            border-radius: 3px;
            border: 1px solid #D1D3D4
        }

        /* hide input */
        input.radio:empty {
            margin-left: -999px;
        }

        /* style label */
        input.radio:empty ~ label {
            position: relative;
            float: left;
            line-height: 2.5em;
            text-indent: 3.25em;
            margin-top: 2em;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        input.radio:empty ~ label:before {
            position: absolute;
            display: block;
            top: 0;
            bottom: 0;
            left: 0;
            content: '';
            width: 2.5em;
            background: #D1D3D4;
            border-radius: 3px 0 0 3px;
        }

        /* toggle hover */
        input.radio:hover:not(:checked) ~ label:before {
            content:'\2714';
            text-indent: .9em;
            color: #C2C2C2;
        }

        input.radio:hover:not(:checked) ~ label {
            color: #888;
        }

        /* toggle on */
        input.radio:checked ~ label:before {
            content:'\2714';
            text-indent: .9em;
            color: #9CE2AE;
            background-color: #4DCB6D;
        }

        input.radio:checked ~ label {
            color: #777;
        }

        /* radio focus */
        input.radio:focus ~ label:before {
            box-shadow: 0 0 0 3px #999;
        }
    </style>
    <body>

        <h3>Seleccion de Pago</h3>



        <?php if (isset($_GET['chat'])) { ?>
            <div>
                <input type="radio" name="plan" id="radio1" value="-1" class="radio" />
                <label class="labelB" for="radio1">Pago  chat de servicio (5 USD)</label>
            </div>
        <?php } ?>



        <div>
            <input type="radio" name="plan" id="radio2" value="2" class="radio"/>
            <label  class="labelB" for="radio2">Membresia 1 Mes, (20 USD/mes)</label>
        </div>

        <div>	
            <input type="radio" name="plan" id="radio3" value="3" class="radio"/>
            <label class="labelB" for="radio3">Membresia 6 Meses, (16 USD/mes - 96 USD Total)</label>
        </div>

        <div>	
            <input type="radio" name="plan" id="radio4" value="4" class="radio"/>
            <label class="labelB" for="radio4">Membresia 1 a�o, (12 USD/mes - 144 USD Total)</label>
        </div>

        <br/><br/>
        
        <?php if(isset($_GET['idProvider'])) { ?>
        <input id="idProvider"    type="hidden"  value="<?php echo $_GET['idProvider']; ?>" >
        
        <?php } ?>

        <input id="id_service"    type="hidden"  value="<?php echo $_GET['id_service']; ?>" >
        <input id="type_tran"    type="hidden"  value="<?php echo $_GET['type_tran']; ?>" >

        <form id="formPay" method="post" action="https://sandbox.gateway.payulatam.com/ppp-web-gateway">
            <input name="merchantId"    type="hidden"  value="508029"   >
            <input name="accountId"     type="hidden"  value="512321" >
            <input name="description"   type="hidden"  value=""  >
            <input name="referenceCode" type="hidden"  value="" >
            <input name="amount"        type="hidden"  value=""   >
            <input name="tax"           type="hidden"  value="0"  >
            <input name="taxReturnBase" type="hidden"  value="0" >
            <input name="currency"      type="hidden"  value="USD" >
            <input name="signature"     type="hidden"  value="ba9ffa71559580175585e45ce70b6c37"  >
            <input name="test"          type="hidden"  value="1" >
            <input name="buyerFullName" type="hidden"  value="Ronald Antonny vargas Mora" >
            <input name="buyerEmail"    type="hidden"  value="ingsisvargas@gmail.com" >
            <input name="shippingAddress" type="hidden"  value="No Aplica" >
            <input name="shippingCity" type="hidden"  value="No Aplica" >
            <input name="shippingCountry" type="hidden"  value="No Aplica" >
            <input name="telephone" type="hidden"  value="No Aplica" >
            <input name="extra1" type="hidden"  value="" >
            <input name="responseUrl"    type="hidden"  value="http://www.test.com/response" >
            <input name="confirmationUrl"    type="hidden"  value="http://www.test.com/confirmation" >
            <br/><br/>
            <input type="button" id="efectuarPago"  class="btn btn-warning" value="Efectuar Transaccion" >
        </form>




        <script type="text/javascript">

            $("#efectuarPago").click(function () {
                var dataIn = {
                    "id_service": $("#id_service").val(),
                    "id_plan": $('input:radio[name=plan]:checked').val(),
                    "type": $("#type_tran").val(),
                    "idProvider":$("#idProvider").val() === 'undefined'?'':$("#idProvider").val()
                };
                $.post("Services/applyTransfer.php",
                        dataIn,
                        function (data) {
                            
                            if (data.STATUS === 'OK') {

                                document.getElementsByName("description").value = data.DATA.description;
                                document.getElementsByName("referenceCode").value = data.DATA.id_transferencia;
                                document.getElementsByName("amount").value = data.DATA.valor;
                                document.getElementsByName("buyerFullName").value = data.DATA.nombreFull;
                                document.getElementsByName("buyerEmail").value = data.DATA.emailFull;
                                document.getElementsByName("extra1").value = data.DATA.extra1;
                                document.getElementsByName("signature").value = data.DATA.firma;
                                $("#formPay").submit();

                            } else {
                                alert("Ha ocurrdio un error, contacte al administrador del Sistema.");

                            }
                        },
                        "json");

            });

            


        </script>
    </body>
</html>
