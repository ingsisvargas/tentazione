<?php
session_start();

if (!isset($_SESSION['USER'])) {
    ?>
    <script type="text/javascript">
        window.location = "index.php";</script>
    <?php
}
?>
<html>
    <head>
        <style type="text/css">


            img { height: 210px; max-width: 100%};

            .imgPer{

                width: 210px;
                height: 210px;   
            }

            .dialog-header-puntuacion { background-color: #AE181F; }
            .dialog-header-puntuacion h4 { color: #ffffff; }

            #loadFile table{

                background-color: #fff;


            }
            #loadFile   thead th {
                background-color: #AE181F;
                color: white;
            }
            #loadFile   tbody tr {

                height: 80px;

            }
            #loadFile tbody tr td {
                padding-top:20px;
            }
            .aouto-Scroll{
                overflow: auto;
            }
            .dialog-header-loadFile { background-color: #AE181F; }
            .dialog-header-loadFile h4 { color: #ffffff; }
            .bg-icon{

                font-size: 20px;
                padding: 5px;
                cursor:pointer;
            }
            .loadFile{

                direction: rtl;/* right to left */
                unicode-bidi: bidi-override;/* bidi de bidireccional */
                text-align: left;

            }


            div.panel {
                font-size: 16px;
                height: 210px;
                width: 100%;
            }
            div.panel p label{

                margin-right: 10px;
                font-size: 14px;
            }
            .button{
                margin-bottom: 10px;

            }
            #info{
                margin-left: 15px;
                max-width: 550px;
            }
            .button > .pull-left {
                margin-right: 10px;
            }
            .carousel-inner{text-align: center;}
            .carousel-inner > .item {
                width:100%;
                height:440px;
            }
            .carousel-inner > .item > img {
                width:auto;
                display: inline-block;
                height: auto;
                max-height:440px;
            }

            .gold{
                color:  #FFD700;
            }
        </style>

    </head>
    <body>
        <div class = "row">

            <div class="col-md-6">
                <div class="button">
                    <a onclick="loadGallery()" class="btn btn-sm btn-warning pull-left" data-toggle="modal" data-target="#changeImage" data-toggle="tooltip" data-placement="left"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a>
                    <div class="clearfix"></div>
                </div>   
                <div class="row">
                    <div class="col-md-5">
                        <div class="imgPer">
                            <img id="foto-perfil" src ="" />
                        </div>
                    </div>
                    <div class="col-md-7">
                        <div  class="panel panel-default">
                            <div class="panel-heading text-center">Datos Ubicación</div>  
                            <div class="panel-body">
                                <p id="pais"></p>
                                <p id="region"></p>
                                <p id="ciudad"></p>
                                <button type="button" class="btn btn-danger btn-sm" value="configurar" id="cambioL">Configurar</button>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div id="cambioLugar" style="display: none;">
                            <form action="../../tentazione/Services/configLocation.php" method="post">
                                <div class="form-group">
                                    <label for="ubicacion">Ciudad en donde estas </label>
                                    <div class="input-group margin-bottom-sm" id="locationField">
                                        <span class="input-group-addon"><i class="fa fa-map-o" aria-hidden="true"></i></span>
                                        <input class="form-control" type="text" id="autocomplete" placeholder="Ejemplo :bogota" onFocus="geolocate()" required>
                                    </div>
                                </div>

                                <div id="datos" style="display: none;">
                                    <table id="address">
                                        <tr>
                                            <td class="label">Street address</td>
                                            <td class="slimField"><input class="field" id="street_number" disabled="true"></td>
                                            <td class="wideField" colspan="2"><input class="field" id="route"  disabled="true"></td>
                                        </tr>
                                        <tr>
                                            <td class="label">City</td>
                                            <td class="wideField" colspan="3"><input class="field" id="locality" name="ciudad"  disabled="true"></td>
                                        </tr>
                                        <tr>
                                            <td class="label">State</td>
                                            <td class="slimField"><input class="field" id="administrative_area_level_1" name="region" disabled="true"></td>
                                            <td class="label">Zip code</td>
                                            <td class="wideField"><input class="field" id="postal_code" disabled="true"></td>
                                        </tr>
                                        <tr>
                                            <td class="label">Country</td>
                                            <td class="wideField" colspan="3"><input class="field" id="country" name="pais" disabled="true"></td>
                                        </tr>
                                    </table>

                                    <script>
                                        // This example displays an address form, using the autocomplete feature
                                        // of the Google Places API to help users fill in the information.

                                        var placeSearch, autocomplete;
                                        var componentForm = {
                                            street_number: 'short_name',
                                            route: 'long_name',
                                            locality: 'long_name',
                                            administrative_area_level_1: 'short_name',
                                            country: 'long_name',
                                            postal_code: 'short_name'
                                        };

                                        function initAutocomplete() {
                                            // Create the autocomplete object, restricting the search to geographical
                                            // location types.
                                            autocomplete = new google.maps.places.Autocomplete(
                                                /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
                                                {types: ['geocode']});

                                            // When the user selects an address from the dropdown, populate the address
                                            // fields in the form.
                                            autocomplete.addListener('place_changed', fillInAddress);
                                        }

                                        // [START region_fillform]
                                        function fillInAddress() {
                                            // Get the place details from the autocomplete object.
                                            var place = autocomplete.getPlace();

                                            for (var component in componentForm) {
                                                document.getElementById(component).value = '';
                                                document.getElementById(component).disabled = false;
                                            }

                                            // Get each component of the address from the place details
                                            // and fill the corresponding field on the form.
                                            for (var i = 0; i < place.address_components.length; i++) {
                                                var addressType = place.address_components[i].types[0];
                                                if (componentForm[addressType]) {
                                                    var val = place.address_components[i][componentForm[addressType]];
                                                    document.getElementById(addressType).value = val;
                                                }
                                            }
                                        }
                                        // [END region_fillform]

                                        // [START region_geolocation]
                                        // Bias the autocomplete object to the user's geographical location,
                                        // as supplied by the browser's 'navigator.geolocation' object.
                                        function geolocate() {
                                            if (navigator.geolocation) {
                                                navigator.geolocation.getCurrentPosition(function(position) {
                                                    var geolocation = {
                                                        lat: position.coords.latitude,
                                                        lng: position.coords.longitude
                                                    };
                                                    var circle = new google.maps.Circle({
                                                        center: geolocation,
                                                        radius: position.coords.accuracy
                                                    });
                                                    autocomplete.setBounds(circle.getBounds());
                                                });
                                            }
                                        }
                                        // [END region_geolocation]

                                    </script>
                                    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCSMxQ62XqiSmhe5vr9XoYKKUMk2IMkOjw&libraries=places&callback=initAutocomplete"
                                            async defer></script>
                                </div>

                                <script type='text/javascript' src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>







                                <button type="submit" class="btn btn-danger btn-sm"">cambiar</button>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div id="info" class="panel pnael-default">
                        <div class="panel-heading text-center">
                            Datos Usuario
                        </div> 
                        <div class="panel-body">
                            <p id="nombre"></p>
                            <p id="apellido"></p>
                            <p id="edad"></p>
                            <p id="puntuacion"></p>
                        </div>  
                    </div>  
                </div>

            </div>





            <div class="col-md-6">
                <div class="button">
                    <a class="btn btn-sm btn-warning pull-right" data-toggle="modal" data-target="#loadFile" data-toggle="tooltip" data-placement="right" title="Subir Fotos"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></a>
                    <div class="clearfix"></div>
                </div> 
                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ol id="list-carrusel" class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                        <li data-target="#myCarousel" data-slide-to="3"></li>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div id="img-carrusel" class="carousel-inner" role="listbox">

                    </div>

                    <!-- Left and right controls -->
                    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>

            </div>  
        </div>

        <div id="loadFile" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <form>
                    <div class="modal-content">
                        <div class="modal-header dialog-header-loadFile">
                            <a class="close" data-dismiss="modal">&times;</a>
                            <h4 class="modal-title">Cargar Fotos</h4>
                        </div>
                        <div class="modal-body" id="modalLoadFile"> 
                            <div id="content">
                                <input type="file" name="files[]" id="filer_input2" multiple="multiple">                                
                            </div>
                        </div>                        
                    </div>
                </form>    
            </div>
        </div>

        <div id="changeImage" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header dialog-header-lo">
                        <a class="close" data-dismiss="modal">&times;</a>
                        <h4 class="modal-title">Foto de perfil.</h4>

                        <div class="modal-body" id="modalCalificacion"> 
                            <div id="content">
                                <div id="myCarousel-change" class="carousel slide" data-ride="carousel">
                                    <!-- Indicators -->
                                    <ol id="list-carrusel-change" class="carousel-indicators">

                                    </ol>
                                    <!-- Wrapper for slides -->
                                    <div id="img-carrusel-change" class="carousel-inner" role="listbox">

                                    </div>

                                    <!-- Left and right controls -->
                                    <a class="left carousel-control" href="#myCarousel-change" role="button" data-slide="prev">
                                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="right carousel-control" href="#myCarousel-change" role="button" data-slide="next">
                                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                        <span class="sr-only">Next</span>
                                    </a>

                                </div>
                            </div>
                        </div>                        
                    </div>

                </div>
            </div>
        </div>






        <div id="datos" style="display: none;">
            <table id="address">
                <tr>
                    <td class="label">Street address</td>
                    <td class="slimField"><input class="field" id="street_number" disabled="true"></td>
                    <td class="wideField" colspan="2"><input class="field" id="route"  disabled="true"></td>
                </tr>
                <tr>
                    <td class="label">City</td>
                    <td class="wideField" colspan="3"><input class="field" id="locality" name="ciudad"  disabled="true"></td>
                </tr>
                <tr>
                    <td class="label">State</td>
                    <td class="slimField"><input class="field" id="administrative_area_level_1" name="region" disabled="true"></td>
                    <td class="label">Zip code</td>
                    <td class="wideField"><input class="field" id="postal_code" disabled="true"></td>
                </tr>
                <tr>
                    <td class="label">Country</td>
                    <td class="wideField" colspan="3"><input class="field" id="country" name="pais" disabled="true"></td>
                </tr>
            </table>




        </div>


        <script type="text/javascript">
            $(document).ready(function () {
<<<<<<< HEAD
=======

                $("#cambioL").on("click", function () {
                    $('#cambioLugar').toggle();
                });


>>>>>>> adb4ac7b1478f22ae7633316c19f2936ab96809e
                $("#filer_input2").filer({
                    limit: null,
                    maxSize: null,
                    extensions: null,
                    changeInput: '<div class="jFiler-input-dragDrop"><div class="jFiler-input-inner"><div class="jFiler-input-icon"><i class="icon-jfi-cloud-up-o"></i></div><div class="jFiler-input-text"><h3>Arrastre y suelte archivos aquí</h3> <span style="display:inline-block; margin: 15px 0">or</span></div><a class="jFiler-input-choose-btn blue">gestor de archivos</a></div></div>',
                    showThumbs: true,
                    theme: "dragdropbox",
                    templates: {
                        box: '<ul class="jFiler-items-list jFiler-items-grid"></ul>',
                        item: '<li class="jFiler-item">\
                                                <div class="jFiler-item-container">\
                                                        <div class="jFiler-item-inner">\
                                                                <div class="jFiler-item-thumb">\
                                                                        <div class="jFiler-item-status"></div>\
                                                                        <div class="jFiler-item-thumb-overlay">\
                                                                                <div class="jFiler-item-info">\
                                                                                        <div style="display:table-cell;vertical-align: middle;">\
                                                                                                <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name}}</b></span>\
                                                                                                <span class="jFiler-item-others">{{fi-size2}}</span>\
                                                                                        </div>\
                                                                                </div>\
                                                                        </div>\
                                                                        {{fi-image}}\
                                                                </div>\
                                                                <div class="jFiler-item-assets jFiler-row">\
                                                                        <ul class="list-inline pull-left">\
                                                                                <li>{{fi-progressBar}}</li>\
                                                                        </ul>\
                                                                        <ul class="list-inline pull-right">\
                                                                                <li><a class="icon-jfi-trash jFiler-item-trash-action"></a></li>\
                                                                        </ul>\
                                                                </div>\
                                                        </div>\
                                                </div>\
                                        </li>',
                        itemAppend: '<li class="jFiler-item">\
                                                        <div class="jFiler-item-container">\
                                                                <div class="jFiler-item-inner">\
                                                                        <div class="jFiler-item-thumb">\
                                                                                <div class="jFiler-item-status"></div>\
                                                                                <div class="jFiler-item-thumb-overlay">\
                                                                                        <div class="jFiler-item-info">\
                                                                                                <div style="display:table-cell;vertical-align: middle;">\
                                                                                                        <span class="jFiler-item-title"><b title="{{fi-name}}">{{fi-name}}</b></span>\
                                                                                                        <span class="jFiler-item-others">{{fi-size2}}</span>\
                                                                                                </div>\
                                                                                        </div>\
                                                                                </div>\
                                                                                {{fi-image}}\
                                                                        </div>\
                                                                        <div class="jFiler-item-assets jFiler-row">\
                                                                                <ul class="list-inline pull-left">\
                                                                                        <li><span class="jFiler-item-others">{{fi-icon}}</span></li>\
                                                                                </ul>\
                                                                                <ul class="list-inline pull-right">\
                                                                                        <li><a class="icon-jfi-trash jFiler-item-trash-action"></a></li>\
                                                                                </ul>\
                                                                        </div>\
                                                                </div>\
                                                        </div>\
                                                </li>',
                        progressBar: '<div class="bar"></div>',
                        itemAppendToEnd: false,
                        canvasImage: true,
                        removeConfirmation: false,
                        _selectors: {
                            list: '.jFiler-items-list',
                            item: '.jFiler-item',
                            progressBar: '.bar',
                            remove: '.jFiler-item-trash-action'
                        }
                    },
                    dragDrop: {
                        dragEnter: null,
                        dragLeave: null,
                        drop: null,
                        dragContainer: null,
                    },
                    uploadFile: {
                        url: "Services/uploadFile.php",
                        data: null,
                        type: 'POST',
                        enctype: 'multipart/form-data',
                        synchron: true,
                        beforeSend: function () {},
                        success: function (data, itemEl, listEl, boxEl, newInputEl, inputEl, id) {
                            var parent = itemEl.find(".jFiler-jProgressBar").parent(),
                                    new_file_name = JSON.parse(data),
                                    filerKit = inputEl.prop("jFiler");
                            filerKit.files_list[id].name = new_file_name;
                            itemEl.find(".jFiler-jProgressBar").fadeOut("slow", function () {
                                $("<div class=\"jFiler-item-others text-success\"><i class=\"icon-jfi-check-circle\"></i> Exito</div>").hide().appendTo(parent).fadeIn("slow");
                            });
                        },
                        error: function (el) {
                            var parent = el.find(".jFiler-jProgressBar").parent();
                            el.find(".jFiler-jProgressBar").fadeOut("slow", function () {
                                $("<div class=\"jFiler-item-others text-error\"><i class=\"icon-jfi-minus-circle\"></i> Error</div>").hide().appendTo(parent).fadeIn("slow");
                            });
                        },
                        statusCode: null,
                        onProgress: null,
                        onComplete: null
                    },
                    files: null,
                    addMore: false,
                    allowDuplicates: true,
                    clipBoardPaste: true,
                    excludeName: null,
                    beforeRender: null,
                    afterRender: null,
                    beforeShow: null,
                    beforeSelect: null,
                    onSelect: null,
                    afterShow: null,
                    onRemove: function (itemEl, file, id, listEl, boxEl, newInputEl, inputEl) {
                        var filerKit = inputEl.prop("jFiler"),
                                file_name = filerKit.files_list[id].name;
                        $.post('Services/removeFile.php', {file: file_name});
                    },
                    onEmpty: null,
                    options: null,
                    dialogs: {
                        alert: function (text) {
                            return alert(text);
                        },
                        confirm: function (text, callback) {
                            confirm(text) ? callback() : null;
                        }
                    },
                    captions: {
                        button: "Seleccionar archivos",
                        feedback: "Selecciona archivos para subir",
                        feedback2: "Los archivos fueron elegidos",
                        drop: "Descargar archivo aquí para cargar",
                        removeConfirmation: "Seguro que quieres eliminar este archivo?",
                        errors: {
                            filesLimit: "Sólo los archivos {{fi-limit}}  se pueden cargar",
                            filesType: "Sólo las imágenes se pueden cargar.",
                            filesSize: "{{fi-name}} ¡Es demasiado largo! Cargue el archivo hasta  {{fi-maxSize}} MB.",
                            filesSizeAll: "¡Los archivos que has elegido son demasiado grandes! Cargue archivos hasta {{fi-maxSize}} MB."
                        }
                    }
                });

                loadPerfilCustomer();
            });

            function cambiarLugar() {
                alert('entro en cambio de lugar');
                window.locationf="../Services/configLocation.php";

            }



            function loadPerfilCustomer() {
                $("#foto-perfil").attr("src", "<?php echo $_SESSION['DATA']['url_foto_perfil']; ?>");
                console.log("<?php echo $_SESSION['DATA']['url_foto_perfil']; ?>");
                $.ajax({
                    url: 'Services/loadPersonalInfo.php',
                    type: 'GET',
                    success: function (json) {
                        var Result = JSON.parse(json);
                        console.log(Result);
                        var puntuacion = "<label >Puntuación:</label>";
                        var numero = parseInt(Result.puntuacion);
                        $("#pais").html("<label>Pais:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>" + Result.pais);
                        $("#region").html("<label>Región:</label>" + Result.region);
                        $("#ciudad").html("<label>Ciudad:</label>" + Result.ciudad);
                        $("#nombre").html("<label>Nombre:</label>" + Result.nombres);
                        $("#apellido").html("<label>Apellido:</label>" + Result.apellidos);
                        $("#edad").html("<label>Edad:</label>" + Result.fecha_nacimiento);
                        for (var i = 0; i < numero; i++) {
                            puntuacion += "<span class=\"glyphicon glyphicon-star gold\" aria-hidden=\"true\"></span>";
                        }
                        $("#puntuacion").html(puntuacion);
                        if (Result.IMAGENES.length > 0) {
                            var list_carrusel = "<li data-target=\"#myCarousel\" data-slide-to=\"0\" class=\"active\"></li>";
                            var img_carrusel = "<div class=\"item active\"><img class=\"img-responsive\"  src=\"" + Result.IMAGENES[0].url_almacenamiento
                                    + "\"></div>";
                            for (var i = 1; i < Result.IMAGENES.length; i++)
                            {
                                list_carrusel += "<li data-target=\"#myCarousel\" data-slide-to=\"" + i + "\" ></li>";
                                img_carrusel += "<div class=\"item\"><img class=\"img-responsive\" src=\"" + Result.IMAGENES[i].url_almacenamiento
                                        + "\"></div>";
                            }
                            $("#list-carrusel").html(list_carrusel);
                            $("#img-carrusel").html(img_carrusel);
                        }
                    },
                    error: function (xhr, status) {
                        alert('Se presentó un problema,intentelo nuevmanete.');
                    }

                });
            }


            function loadGallery() {

                $.post("Services/loadGallery.php",
                        function (Result) {

                            if (Result.STATUS === 'OK') {
                                if (Result.DATA.length > 0) {
                                    var list_carrusel = "<li data-target=\"#myCarousel-change\" data-slide-to=\"0\" class=\"active\"></li>";
                                    var img_carrusel = "<div class=\"item active\"><img class=\"img-responsive\"  src=\"" + Result.DATA[0].url_almacenamiento + "\">";
                                    img_carrusel += " <div class = \"carousel-caption\">";
                                    img_carrusel += '<a data-toggle="tooltip" data-placement="right"  title="Mostrar"  onclick="changeImagePerfil(\'' + Result.DATA[0].url_almacenamiento + '\')" class="btn btn-warning btn-sm">' +
                                            ' <span class="glyphicon glyphicon-ok-sign"></span>' +
                                            '</a></div></div>';
                                    for (var i = 1; i < Result.DATA.length; i++) {
                                        list_carrusel += "<li data-target=\"#myCarousel-change\" data-slide-to=\"" + i + "\" ></li>";
                                        img_carrusel += "<div class=\"item\"><img class=\"img-responsive\" src=\"" + Result.DATA[i].url_almacenamiento + "\">";
                                        img_carrusel += " <div class = \"carousel-caption\">";
                                        img_carrusel += '<a data-toggle="tooltip" data-placement="right"  title="Mostrar"  onclick="changeImagePerfil(\'' + Result.DATA[i].url_almacenamiento + '\')" class="btn btn-warning btn-sm">' +
                                                ' <span class="glyphicon glyphicon-ok-sign"></span>' +
                                                '</a></div></div>';
                                    }

                                    $("#list-carrusel-change").html(list_carrusel);
                                    $("#img-carrusel-change").html(img_carrusel);
                                }
                            }
                        },
                        "json"
                        );
            }



            function changeImagePerfil(urlNew) {
                var dataIn = {"urlNewImage": urlNew};
                $.post("Services/changePerfilImage.php",
                        dataIn,
                        function (data) {

                            if (data.STATUS === 'OK') {
                                $("#foto-perfil").attr("src", "<?php echo $_SESSION['DATA']['url_foto_perfil']; ?>");
                                $("#changeImage").modal('hide');
                                window.location = "customerHome.php";
                            }
                        },
                        "json");

            }



            // This example displays an address form, using the autocomplete feature
            // of the Google Places API to help users fill in the information.

<<<<<<< HEAD
        </script>

        <!-- Libreria jQuery -->




        <script type="text/javascript">
            $(document).ready(function()
            {
                $("#cambioL").on( "click", function() {
                    $('#cambioLugar').toggle();
                });
            });
        </script>

=======
            var placeSearch, autocomplete;
            var componentForm = {
                street_number: 'short_name',
                route: 'long_name',
                locality: 'long_name',
                administrative_area_level_1: 'short_name',
                country: 'long_name',
                postal_code: 'short_name'
            };

            function initAutocomplete() {
                // Create the autocomplete object, restricting the search to geographical
                // location types.
                autocomplete = new google.maps.places.Autocomplete(
                        /** @type {!HTMLInputElement} */(document.getElementById('autocomplete')),
                        {types: ['geocode']});

                // When the user selects an address from the dropdown, populate the address
                // fields in the form.
                autocomplete.addListener('place_changed', fillInAddress);
            }

            // [START region_fillform]
            function fillInAddress() {
                // Get the place details from the autocomplete object.
                var place = autocomplete.getPlace();

                for (var component in componentForm) {
                    document.getElementById(component).value = '';
                    document.getElementById(component).disabled = false;
                }

                // Get each component of the address from the place details
                // and fill the corresponding field on the form.
                for (var i = 0; i < place.address_components.length; i++) {
                    var addressType = place.address_components[i].types[0];
                    if (componentForm[addressType]) {
                        var val = place.address_components[i][componentForm[addressType]];
                        document.getElementById(addressType).value = val;
                    }
                }
            }
            // [END region_fillform]

            // [START region_geolocation]
            // Bias the autocomplete object to the user's geographical location,
            // as supplied by the browser's 'navigator.geolocation' object.
            function geolocate() {
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(function (position) {
                        var geolocation = {
                            lat: position.coords.latitude,
                            lng: position.coords.longitude
                        };
                        var circle = new google.maps.Circle({
                            center: geolocation,
                            radius: position.coords.accuracy
                        });
                        autocomplete.setBounds(circle.getBounds());
                    });
                }
            }
            // [END region_geolocation]



        </script>







>>>>>>> adb4ac7b1478f22ae7633316c19f2936ab96809e


    </body>
</html>