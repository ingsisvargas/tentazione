<html>
    <head>
        <style>


            .dialog-header-puntuacion { background-color: #AE181F; }
            .dialog-header-puntuacion h4 { color: #ffffff; }

            #tramitados{


                padding-top: 20px;

                background-color: #fff;


            }
            #tramitados  thead th {
                background-color: #AE181F;
                color: white;
            }

        </style>


    </head>    
    <body>
        <div id="tramitados" class="row">

            <div class="col-md-12">
                <h4 class="text-center">Servicios tramitados</h4>
                <div id="servicios" class="table-responsive">

                </div>
            </div>
        </div>



        <div id="servicio" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header dialog-header-puntuacion">
                        <a class="close" data-dismiss="modal">&times;</a>
                        <h4 class="modal-title">Información del Servicio</h4>
                    </div>
                    <div class="modal-body" id="modalCalificacion">
                        <div id ="alerta"></div>

                        <div class="clasificacion" >
                            <p id="plan"></p>
                            <p id="fecha"></p>
                            <p id="cliente"></p>
                            <p id="hora_inicio"></p>
                            <p id="hora_fin"></p>
                            <p id="estado"></p>
                            <p id="descripcion"></p>
                        </div>    

                    </div>
                    <div class="modal-footer">
                        <a onclick="closeModal()" class="btn btn-warning" >Aceptar</a>
                    </div>
                </div>
            </div>
        </div> 



      




        <script type="text/javascript">
            $(document).ready(function () {
                cargarServicios();
                $('[data-toggle="tooltip"]').tooltip();
            });

            function cargarServicios() {

                var dataIn = {"estado": ""};

                $.post("Services/getServices.php",
                        dataIn,
                        function (data) {

                            if (data.STATUS === 'OK') {
                                var tabla = '<a onclick="loadNew()" class="btn btn-sm btn-warning pull-left" data-toggle="tooltip" >'
                                        + '<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>'
                                        + '</a>';


                                tabla += '   <br/> <br/> <table id="dataTable" class="table table-condensedtable-hover">' +
                                        '<thead>' +
                                        ' <tr>' +
                                        '<th>Fecha</th>' +
                                        '<th>Hora</th>' +
                                        '<th>Plan</th>' +
                                        '<th>Hora final</th>' +
                                        '<th>Estado</th>' +
                                        '<th>Acción</th>' +
                                        '</tr> ' +
                                        '</thead>' +
                                        '<tbody>';
                                var cant = data.DATA.length;
                                for (var i = 0; i < cant; i++) {

                                    tabla += '<tr>' +
                                            '<td>' + data.DATA[i].fecha_servicio + '</td>' +
                                            '<td>' + data.DATA[i].hora_inicio + '</td>' +
                                            '<td>' + data.DATA[i].plan + '</td>' +
                                            '<td>' + data.DATA[i].hora_fin + '</td>' +
                                            '<td>' + (data.DATA[i].estado === "AB" ? 'Abierto' : 'Atendido') + '</td>' +
                                            '<td>' +
                                            '<a data-toggle="modal" data-target="#servicio" data-toggle="tooltip" data-placement="right" title="Ver mas..." onclick="verMas(' + data.DATA[i].idtnt_servicio + ')" class="btn btn-warning btn-sm" >' +
                                            ' <span class="glyphicon glyphicon-info-sign"></span>' +
                                            ' </a>&nbsp; &nbsp;' +
                                            '<a data-toggle="tooltip" data-placement="right"  title="Siguiente" onclick="aspirantes(' + data.DATA[i].idtnt_servicio + ',' + '\'' + data.DATA[i].estado + '\'' + ')" class="btn btn-warning btn-sm">' +
                                            ' <span class="glyphicon glyphicon-share-alt"></span>' +
                                            '</a>' +
                                            '</td>' +
                                            '</tr>';
                                }

                                tabla += ' </tbody></table>';

                                $('#servicios').html(tabla);


                            }
                        },
                        "json");


            }

            function loadNew() {
                $("#load").load("templates/nuevoServicio.php");
            }

            function verMas(id_servicio) {

                var dataIn = {"id_servicio": id_servicio};

                $.post("Services/LoadService.php",
                        dataIn,
                        function (data) {

                            if (data.STATUS === 'OK') {

                                $("#plan").html("<label>Plan:&nbsp</label>" + data.DATA.plan);
                                $("#fecha").html("<label>Fecha:&nbsp</label>" + data.DATA.fecha_servicio);
                                $("#hora_inicio").html("<label>Hora inicio:&nbsp</label>" + data.DATA.hora_inicio);
                                $("#hora_fin").html("<label>Hora fin:&nbsp</label>" + data.DATA.hora_fin);
                                $("#descripcion").html("<label>Descripción:&nbsp</label>" + data.DATA.descripcion);
                                $("#cliente").html("<label>Cliente:&nbsp</label>" + data.DATA.cliente);

                            }

                        },
                        "json");


            }

            function closeModal() {
                $("#servicio").modal('hide');
            }

            function closeModal2() {
                $("#newService").modal('hide');
            }

            function aspirantes(id_servicio, estado) {
                $("#load").load("templates/aspirantes.php?id_servicio=" + id_servicio + "&estado=" + estado);

            }

          


        </script>
    </body>


</html>
