<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <div>
            <h4>El valor a pagar por el servicio que esta tramitando es correspondiente a : </h4> <p id="valor"></p>   
        </div>

        <input id="id_service"    type="hidden"  value="<?php echo $_GET['id_service']; ?>" >
        <input id="type_tran"    type="hidden"  value="<?php echo $_GET['type_tran']; ?>" >
        <input id="valorH"    type="hidden"  value="" >

         <form id="formPay" method="post" action="https://sandbox.gateway.payulatam.com/ppp-web-gateway">
            <input name="merchantId"    type="hidden"  value="508029"   >
            <input name="accountId"     type="hidden"  value="512321" >
            <input name="description"   type="hidden"  value=""  >
            <input name="referenceCode" type="hidden"  value="" >
            <input name="amount"        type="hidden"  value=""   >
            <input name="tax"           type="hidden"  value="0"  >
            <input name="taxReturnBase" type="hidden"  value="0" >
            <input name="currency"      type="hidden"  value="USD" >
            <input name="signature"     type="hidden"  value="ba9ffa71559580175585e45ce70b6c37"  >
            <input name="test"          type="hidden"  value="1" >
            <input name="buyerFullName" type="hidden"  value="Ronald Antonny vargas Mora" >
            <input name="buyerEmail"    type="hidden"  value="ingsisvargas@gmail.com" >
            <input name="shippingAddress" type="hidden"  value="No Aplica" >
            <input name="shippingCity" type="hidden"  value="No Aplica" >
            <input name="shippingCountry" type="hidden"  value="No Aplica" >
            <input name="telephone" type="hidden"  value="No Aplica" >
            <input name="extra1" type="hidden"  value="" >
            <input name="responseUrl"    type="hidden"  value="http://www.test.com/response" >
            <input name="confirmationUrl"    type="hidden"  value="http://www.test.com/confirmation" >
            <br/><br/>
            <input type="button" id="efectuarPago"  class="btn btn-warning" value="Efectuar Transaccion" >
        </form>




        <script type="text/javascript">


            $(document).ready(function () {
                cargarValor();
            });


            function cargarValor() {

                var dataIn = {"id_service": $("#id_service").val()};
                $.post("Services/loadServicePrice.php",
                        dataIn,
                        function (data) {
                            if (data.STATUS === 'OK') {
                                document.getElementById("valor").innerHTML = "<h3>" + data.VALOR + " USD</h3>";
                                document.getElementById("valorH").value = data.VALOR;

                            }

                        },
                        "json");

            }

            $("#efectuarPago").click(function () {
                var dataIn = {
                    "id_service": $("#id_service").val(),
                    "type": $("#type_tran").val(),
                    "valor": $("#valorH").val()
                };
                $.post("Services/applyTransferService.php",
                        dataIn,
                        function (data) {

                            if (data.STATUS === 'OK') {

                                document.getElementsByName("description").value = data.DATA.description;
                                document.getElementsByName("referenceCode").value = data.DATA.id_transferencia;
                                document.getElementsByName("amount").value = data.DATA.valor;
                                document.getElementsByName("buyerFullName").value = data.DATA.nombreFull;
                                document.getElementsByName("buyerEmail").value = data.DATA.emailFull;
                                document.getElementsByName("extra1").value = data.DATA.extra1;
                                document.getElementsByName("signature").value = data.DATA.firma;
                                $("#formPay").submit();

                            } else {
                                alert("Ha ocurrdio un error, contacte al administrador del Sistema.");

                            }
                        },
                        "json");

            });
            
            
 





        </script>
    </body>
</html>
